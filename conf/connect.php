<?php
	function getServer(){
		return "http://sportsci.dpe.go.th";
	}
	function DbConnect(){
		if($_SESSION['TYPE_CONN'] == 2){
			try
			{
				// $ip = $_SESSION['IP'];
				$db = "mysql:host=localhost;dbname=pfit_db";
			  $user = "root";
			  $pass = "";
				$options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8');

		    return new PDO($db,$user,$pass, $options);
		  } catch (Exception $e) {
		    return $e->getMessage();
		  }
		}else{
			try
			{
					// $link = 'http://mbsolutions.';
					$connOracle = new PDO('oci:dbname=192.168.2.19:1521/osrddb1;charset=utf8', 'SPORTSCIDEV', 'SPORTSCIDEV');
					$connOracle->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
					$connOracle->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
					return $connOracle;
			} catch(PDOException $e) {
					echo 'ERROR: ' . $e->getMessage();
			}
		}
	}


	function DbQuery($sql,$ParamData){

		try {
			if(!isset($_SESSION['TYPE_CONN']))
			{
				exit("<script>window.location='login.php';</script>");
			}else{
				$obj = DbConnect();
				$typeQuery = "1";
				if($_SESSION['TYPE_CONN'] != 2){
					if( strpos(strtolower($sql), "select") !== false ) {
		      	$obj->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_EMPTY_STRING);
			    } else {
						$obj->setAttribute(PDO::ATTR_ORACLE_NULLS, PDO::NULL_TO_STRING);
						$typeQuery = "2";
			    }
				}
				// echo  $sql;
				$stm = $obj->prepare($sql);

				if($ParamData != null){
					for($x=1; $x<=count($ParamData); $x++)
					{
					  $stm->bindParam($x,$ParamData[$x-1]);
					}
				}

			  $stm->execute();

				$arr = $stm->errorInfo();
				$data['errorInfo'] = $arr;

				$num = 0;

				//$data = array("status"=>"status","dataCount"=>0);
				if($typeQuery == "1"){
					while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
			      $data['data'][] = $row;
						$num++;
			    }
				}
				//echo "<br>";
				$data['dataCount'] = $num;
				$data['status'] = 'success';
				if($num == 0){
					$data['data'][] = "";
				}
				//echo ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>------->".json_encode($data)."<br>";
				//print_r($data);
				//echo "<br>";
				return json_encode($data);
			}
		} catch (Exception $e) {
			$data['dataCount'] = 0;
			$data['status'] = 'Fail';
			//print_r($data);
		  return  json_encode($data);
			$e->getTraceAsString();
		}
	}
?>
