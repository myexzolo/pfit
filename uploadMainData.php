<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php
  include("inc/header.php");
  include("inc/sidebar.php");
  $display = "none";
  if($_SESSION['TYPE_CONN'] == 1){
    $display = "block";
  }
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">

    <!-- Content Header (Page header) -->
    <!-- Main content -->
    <section class="content">
      <form action="ajax/upload/importData.php" method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <div class="box boxBlack">
                    <div class="box-header with-border">
                      <h3 class="box-title"></h3>
                      <button type="button" class="btn" style="width:80px;display:<?= $display; ?>" onclick="exportData();">ส่งออก</button>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                      <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            <div class="form-group col-md-12">
                              <label class="col-sm-4 control-label">นำเข้าข้อมูลหลัก</label>
                              <div class="col-sm-8">
                                <input type="file" name="filepath" accept=".txt" required>
                              </div>
                            </div>
                            <div class="text-center col-md-12">
                              <button type="submit" class="btn btn-success" style="width:80px;">นำเข้า</button>
                            </div>
                        </div>
                      </div>
                    </div>
              </div>
        </div>
      </form>

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>

</body>
</html>

<script>
function exportData(){
  var url = "exportData.php";
  gotoPage(url);
}
</script>
