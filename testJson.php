<!DOCTYPE html>

<head>
    <?php include("inc/head.php"); ?>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
</head>
<html>
<body>

  <div ng-repeat="dataTest in dataObj">
      <div class="jumbotron">
        <div>{{dataObj.date}}</div>
      </div>
  </div>
<table border="0" align="center" style="width: 100%">
</table>
<?php
include("inc/footer.php");
?>
</body>
<html>
<script type="text/javascript">
  var data= [
      {
          "itemName": "fentanyl patch  25 mcg/hr",
          "fillBy": "P",
          "drugUseId": 41,
          "quantity": 2,
          "afterTake": "",
          "painLv": 2,
          "feqUse": "D",
          "itemCode": "FEN25",
          "hashVersion": "2a5a3cecf92436b23a72cff075503294",
          "usedDate": "23/08/2561 04:50",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      },
      {
          "itemName": "fentanyl patch  25 mcg/hr",
          "fillBy": "P",
          "drugUseId": 42,
          "quantity": 2,
          "afterTake": "",
          "painLv": 2,
          "feqUse": "D",
          "itemCode": "FEN25",
          "hashVersion": "480160dcd73d2d3b799dfcc6483e1a5e",
          "usedDate": "23/08/2561 08:48",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      },
      {
          "itemName": "fentanyl patch  25 mcg/hr",
          "fillBy": "P",
          "drugUseId": 43,
          "quantity": 2,
          "afterTake": "",
          "painLv": 4,
          "feqUse": "D",
          "itemCode": "FEN25",
          "hashVersion": "42b4063ad3087d322fb1d051b2c4090c",
          "usedDate": "25/08/2561 07:57",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      },
      {
          "itemName": "fentanyl patch  100 mcg/hr",
          "fillBy": "P",
          "drugUseId": 37,
          "quantity": 10,
          "afterTake": "",
          "painLv": 1,
          "feqUse": "D",
          "itemCode": "FEN100",
          "hashVersion": "6224548a684c7e2091594a0b32ea85d0",
          "usedDate": "25/08/2561 08:00",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      },
      {
          "itemName": "fentanyl patch  100 mcg/hr",
          "fillBy": "P",
          "drugUseId": 38,
          "quantity": 10,
          "afterTake": "",
          "painLv": 1,
          "feqUse": "D",
          "itemCode": "FEN100",
          "hashVersion": "6224548a684c7e2091594a0b32ea85d0",
          "usedDate": "25/08/2561 08:00",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      },
      {
          "itemName": "fentanyl patch  100 mcg/hr",
          "fillBy": "P",
          "drugUseId": 39,
          "quantity": 10,
          "afterTake": "",
          "painLv": 1,
          "feqUse": "D",
          "itemCode": "FEN100",
          "hashVersion": "6224548a684c7e2091594a0b32ea85d0",
          "usedDate": "25/08/2561 08:00",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      },
      {
          "itemName": "Morphine syrup 10 mg/5ml",
          "fillBy": "P",
          "drugUseId": 40,
          "quantity": 20,
          "afterTake": "ไม่มี",
          "painLv": 1,
          "feqUse": "S",
          "itemCode": "MSS10",
          "hashVersion": "018ac74a6e846726aaec6ed29c63b2db",
          "usedDate": "25/08/2561 08:00",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้เมื่อมีอาการมากขึ้น"
      },
      {
          "itemName": "fentanyl patch  25 mcg/hr",
          "fillBy": "P",
          "drugUseId": 44,
          "quantity": 2,
          "afterTake": "",
          "painLv": 4,
          "feqUse": "D",
          "itemCode": "FEN25",
          "hashVersion": "42b4063ad3087d322fb1d051b2c4090c",
          "usedDate": "25/08/2561 09:57",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      },
      {
          "itemName": "fentanyl patch  25 mcg/hr",
          "fillBy": "P",
          "drugUseId": 45,
          "quantity": 2,
          "afterTake": "",
          "painLv": 4,
          "feqUse": "D",
          "itemCode": "FEN25",
          "hashVersion": "42b4063ad3087d322fb1d051b2c4090c",
          "usedDate": "25/08/2561 10:57",
          "username": "nut1",
          "feqUseText": "ยาที่ใช้ทุกวัน"
      }
  ];
  //console.log(dataTest);

  groupJson(data);

  function groupJson(jsonList){
    var newObj = [];
    for (var i in jsonList) {
      var obj = jsonList[i];
      var newItem = {};
      var usedDate  = obj.usedDate.substr(0,10);
      var usedTime  = obj.usedDate.substr(11,obj.usedDate.length);
      var painLv   = obj.painLv;
      var foundItem = false;
      obj.usedDate = usedTime;
      for (var j in newObj) {
        var existingItem = newObj[j];
        if (newObj[j]['date'] == usedDate && newObj[j]['painLv'] == painLv)
        {
          foundItem = j;
        }
      }
      if (!foundItem) {
        newItem['date'] = usedDate;
        newItem['painLv'] = painLv;
        newItem['data'] = [obj];
        newObj.push(newItem);
        foundItem = true;
      } else {
        newObj[foundItem]['data'].push(obj);
      }
    }
    console.log(newObj);
  }
</script>
