<!DOCTYPE html>
<html>

<style>
  th {
    text-align: center !important;
  }
</style>
<?php
  include("inc/head.php");
  include("inc/utils.php");
?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php");
    $MN = isset($_GET['MN'])?base64_decode($_GET['MN']):"หน้าหลัก";
    $PN = isset($_GET['PN'])?base64_decode($_GET['PN']):"";
    $IC = isset($_GET['IC'])?base64_decode($_GET['IC']):"fa fa-home";

    $month  = date("m");
    $year   = date("Y");
    $year2  = date('Y', strtotime('-1 year'));
    $year3  = date('Y', strtotime('-2 year'));

    $dateStart  = DateThai(date("Y/m/01"));
    $dateEnd    = DateThai(date("Y/m/t"));
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        รายงานตามกลุ่มประเภทผู้ทดสอบ
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-clipboard"></i> รายงาน</a></li>
        <li class="active">รายงานตามกลุ่มประเภทผู้ทดสอบ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">

      <!-- Main row -->
      <div class="col-md-12">
        <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">รายชื่อกิจกรรม</h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6" >
                <div class="form-group">
                  <label class="col-md-2 control-label text-right" style="line-height: 34px;">ตั้งแต่วันที่</label>
                  <div class="col-md-4">
                    <input class="form-control datepicker" value="<?= $dateStart ?>" id="dateStart" type="text" data-provide="datepicker" data-date-language="th-th" >
                  </div>
                  <label class="col-md-2 control-label text-right" style="line-height: 34px;" >ถึง</label>
                  <div class="col-md-4">
                    <input class="form-control datepicker" value="<?= $dateEnd ?>" id="dateEnd" type="text" data-provide="datepicker" data-date-language="th-th" >
                  </div>
                </div>
              </div>
              <button class="btn btn-primary" onclick="searchProject()" style="width:80px;">ค้นหา</button>
            </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div id="show-form"></div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>

<script src="js/report01.js"></script>


</body>
</html>
