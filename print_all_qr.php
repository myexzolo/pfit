<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:16pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:14pt;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:14pt;
    font-weight: bold;
    padding: 5px;
  }

  .text-left{
    text-align: left;
  }
  .text-center{
    text-align: center;
  }
  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:14pt;

  }

  @page {
    size: A4 portrait;
    margin: 0mm;
  }
  @media print {
      html, body {
        width: 200mm;
        height: auto;
      }
      .break{
        page-break-after: always;
      }
      .half{
        margin: 5mm;
        margin-left: 10px;
        height: 143mm;
      }
  }
</style>
<html>
<body>

<div id="print" style="display:none">
  <?php
    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcode/temp'.DIRECTORY_SEPARATOR;
    //html PNG location prefix
    $PNG_WEB_DIR = 'qrcode/temp/';
    include "qrcode/qrlib.php";

    if (!file_exists($PNG_TEMP_DIR)){
      mkdir($PNG_TEMP_DIR);
    }
    $CorrectionLevel = 'L';
    $matrixPointSize = 4;


    $project_name = $_POST['project_name'];
    $project_code = $_POST['project_code'];

    $sql = "SELECT p.project_name,ps.person_number,ps.project_code,ps.person_name,ps.person_lname ,
           ".getQueryDate('date_of_birth').",ps.wiegth,ps.height,ps.person_gender
            FROM pfit_t_project p
            INNER JOIN pfit_t_person ps ON p.project_code = ps.project_code
            WHERE ps.project_code = '$project_code'";

    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $j = 1;
    foreach ($json['data'] as $value) {
    // for ($i=0; $i <2 ; $i++) {
    if(!isset($value['project_name'])){
      break;
    }
    $project_name = $value['project_name'];
    $person_number = $value['person_number'];
    $projectCode = $value['project_code'];
    $person_name = $value['person_name'];
    $person_lname = $value['person_lname'];
    $date_of_birth = $value['date_of_birth'];
    $wiegth = $value['wiegth'];
    $height = $value['height'];
    $person_gender = $value['person_gender']=='M'?"ชาย":"หญิง";


    $filename = $PNG_TEMP_DIR.$person_number.'.png';
    $textQr = "$person_number|$projectCode";
    QRcode::png($textQr, $filename, $CorrectionLevel, $matrixPointSize, 2);


    // $sql = "SELECT * FROM pfit_t_project where project_code = '$projectCode'";
    // $query = DbQuery($sql,null);
    // $row  = json_decode($query, true);

  ?>
  <div class="row half">
  <div class="col-xs-12" align="center" style="font-size:18pt"><b>แบบบันทึกผลการทดสอบ <?php echo $project_name; ?></b></div>
  <div style="padding:30px;"></div>
  <div class="col-xs-6" style="padding:0px;">
    <table border="0" cellspacing="0" style="width:100%" >
      <tbody>
    		<tr>
    			<td width="20%" valign="top" rowspan="5"><img style="padding-right: 15px;" src="<?php echo $PNG_WEB_DIR.basename($filename); ?>" /></td>
    			<td width="50%" valign="top" colspan="2" rowspan="1">ชื่อ-สกุล: <?php echo $person_name; ?> <?php echo $person_lname; ?><br />
    			เลขประจำตัว: <?php echo $person_number; ?><br />
    			วันเดือนปีเกิด: <?php echo convDatetoThai($date_of_birth); ?>

    			</td>
    			<td width="30%" valign="top" rowspan="1">
          น้ำหนัก: <?php echo $wiegth; ?><br />
    			ส่วนสูง: <?php echo $height; ?><br />
          เพศ: <?php echo $person_gender; ?>
        </td>
    		</tr>
    	</tbody>
    </table>
    </p>
    <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
        <tr>
          <td class="text-center content"><strong>ลำดับ</strong></td>
          <td class="text-center content"><strong>รายการ</strong></td>
          <td class="text-center content"><strong>ผลการวัด</strong></td>
        </tr>

        <?php
          $sqlp = "SELECT * FROM pfit_t_project_test
                  INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
                  WHERE pfit_t_project_test.project_code = '$projectCode'
                  ORDER BY pfit_t_project_test.test_seq ASC";
          $queryp = DbQuery($sqlp,null);
          $rowp  = json_decode($queryp, true);
          $x= 0;
          foreach ($rowp['data'] as $key => $value) {
            $x++;
            if($x > 8){
              break;
            }
        ?>
        <tr>
          <td width="10%" class="text-center content"><?php echo $key+1; ?></td>
          <td width="73%" class="text-left content"><?php echo $value['test_name']." (".$value['test_unit'].")"; ?></td>
          <td width="17%" class="text-center content"></td>
        </tr>
        <?php }

          $q1 = "ข้อที่ 1 มีกิจกรรมทางกายและการออกกำลังกาย กี่วัน/สัปดาห์ (นอกจากชั่วโมงพลศึกษา)";
          $q2 = "ข้อที่ 2 รูปแบบกิจกรรมทางกาย (นอกจากชั่วโมงพลศึกษา)";
          $q3 = "ข้อที่ 3 ระยะเวลาในการทำกิจกรรมต่อวัน";

          $a[0] = "ไม่เคยเลย";
          $a[1] = "1-2 วันต่อสัปดาห์";
          $a[2] = "3-4 วันต่อสัปดาห์";
          $a[3] = "5-7 วันต่อสัปดาห์";

          $b[0] = "กีฬาชนิดต่างๆ เช่น ฟุตบอล, เทควันโด, ปั่นจักรยาน เป็นต้น";
          $b[1] = "กิจกรรมนันทนาการ เช่น เต้นรำ, เต้นแอโรบิก, ลีดเดอร์";
          $b[2] = "การละเล่นพื้นบ้าน เช่น หมากเก็บ, กระโดดยาง, ดี่จับ";
          $b[3] = "งานบ้าน เช่น ล้างจาน, ถูบ้าน, รดน้ำต้นไม้";

          $c[0] = "น้อยกว่า 15 นาที";
          $c[1] = "15-30 นาที";
          $c[2] = "31-60 นาที";
          $c[3] = "61 นาทีขึ้นไป";

        ?>

    </table>
  </div>
    <div class="col-xs-6" style="padding:0px 0px 0px 20px;">
        <div class="form-group">
          <b><?= $q1 ?></b>
          <div class="row">
            <?php
            for ($i=0; $i < count($a); $i++) { ?>
            <div class="col-xs-6">
                <input type="radio" id="points_1_<?= $i ?>" name="points_1">
                <label for="points_1_<?= $i ?>" style="font-weight:400;"><?= $a[$i]; ?></label>
            </div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group">
          <b><?= $q2 ?></b>
          <div class="row">
            <?php
            for ($i=0; $i < count($b); $i++) { ?>
            <div class="col-md-6">
                <input type="radio" id="points_2_<?= $i ?>" name="points_2">
                <label for="points_2_<?= $i ?>" style="font-weight:400;"><?= $b[$i]; ?></label>
            </div>
            <?php } ?>
          </div>
        </div>
        <div class="form-group">
          <b><?= $q3 ?></b>
          <div class="row">
            <?php
            for ($i=0; $i < count($c); $i++) { ?>
            <div class="col-xs-6">
                <input type="radio" id="points_3_<?= $i ?>" name="points_3">
                <label for="points_3_<?= $i ?>" style="font-weight:400;"><?= $c[$i]; ?></label>
            </div>
            <?php } ?>
          </div>
        </div>
    </div>
  </div>
  <?php
    if($j%2 ==0){
      echo '<div class="break"></div>';
    }
    $j++
  ?>
  <?php } ?>

</div>
</body>
</html>
<?php
include("inc/footer.php");
?>
<script type="text/javascript">

  $(document).ready(function(){
    setTimeout(function(){
      $('#print').show();
      window.print();
      window.close();
    }, 500);
  });
</script>
