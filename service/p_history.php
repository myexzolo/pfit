<?php
session_start();
include('../conf/connect.php');
include('function.php');
$_SESSION['TYPE_CONN'] = 1;

FIX_PHP_CORSS_ORIGIN();

// $projectCode   = 'M611101';
// $person_number = 'M611101_0003';

$data = file_get_contents("php://input");
$dataJsonDecode = json_decode($data);
$projectCode   = @$dataJsonDecode->project_code;
$person_number  = @$dataJsonDecode->person_number;


if($_SESSION['TYPE_CONN'] == 1){
  $sqlv = "SELECT *
           FROM  (SELECT to_char(date_create, 'dd-mm-yyyy')  as date_create, time
                  FROM pfit_t_result_history
                  WHERE person_number = '$person_number' AND project_code = '$projectCode'
                  GROUP BY time,  to_char(date_create, 'dd-mm-yyyy')
                  ORDER BY time DESC)
           WHERE ROWNUM <= 2  ORDER BY time ASC";
    //echo $sqlv;
    $queryv = DbQuery($sqlv,null);
    $json  = json_decode($queryv, true);
    $num   = $json['dataCount'];
    $row  = $json['data'];

    if($num > 0){
      for ($i=0; $i < $num ; $i++) {
        $date_create    = $row[$i]['date_create'];
        $time           = $row[$i]['time'];

        $date[$i] = array('time' => $date_create);


        $sqlv = "SELECT t.test_name,r.result,r.resultHis_cal,r.test_criteria_code,r.test_code
                 FROM  pfit_t_result_history r, pfit_t_test t
                 where r.test_code = t.test_code and r.time = '$time'
                 and r.person_number = '$person_number' AND r.project_code = '$projectCode'
                 order by r.test_seq";
        //ECHO $sqlv;
        $queryv   = DbQuery($sqlv,null);
        $jsonv    = json_decode($queryv, true);
        $rowv     = $jsonv['data'];
        $numv     = $jsonv['dataCount'];

        for ($x=0; $x < $numv ; $x++) {

          $test_name              = $rowv[$x]['test_name'];
          $result                 = $rowv[$x]['result'];
          $resultHis_cal          = $rowv[$x]['resulthis_cal'];
          $test_criteria_code     = $rowv[$x]['test_criteria_code'];
          $test_code              = $rowv[$x]['test_code'];


          $test_criteria_name = "-";
          //echo ">>>>>".$test_criteria_code;
          if($test_criteria_code != ""){
            $sqlc = "SELECT category_criteria_detail_name
                    FROM pfit_t_cat_criteria_detail c,pfit_t_test_criteria t
                    where c.category_criteria_detail_code = t.category_criteria_detail_code
                    and t.test_criteria_code = '$test_criteria_code'";

            $queryc   = DbQuery($sqlc,null);
            $jsonc    = json_decode($queryc, true);
            $rowc     = $jsonc['data'];

            $test_criteria_name = $rowc[0]['category_criteria_detail_name'];
          }

          if($i == 0){
            $arr[$x] = array(
                'test_code' => $test_code,
                'test_name' => $test_name,
                'result1' => $resultHis_cal,
                'result2' => '0',
                'criteria1' => $test_criteria_name,
                'criteria2' => '-'
                );
          }else if($i == 1){
              $flag = true;
              foreach($arr as $key => $value)
              {
                if($arr[$key]['test_code'] == $test_code){
                   $arr[$key]['result2'] = $resultHis_cal;
                   $arr[$key]['criteria2'] = $test_criteria_name;
                   $flag = false;
                   break;
                }
              }

              if($flag){
                $xx  = (count($arr));
                $arr[$xx] = array(
                    'test_code' => $test_code,
                    'test_name' => $test_name,
                    'result1' => '-',
                    'result2' => $resultHis_cal,
                    'criteria1' => '-',
                    'criteria2' => $test_criteria_name
                    );
              }
          }

        }



        //echo "<br>";
        //print_r($rowv);
        //echo "<br>";
      }
    }
    if($num == 1){
      $date[$i] = array('time' => '');
    }
}

// $array[$key] = array();
//$date[0] = array('time' => '28/11/2561');
//$date[1] = array('time' => '29/11/2561');
//echo "<br>";
//print_r($date);
//echo "<br>";
// $arr[0] = array(
//                 'test_name' => 'นั้งงอตัวไปข้างหน้า',
//                 'result1' => '0',
//                 'result2' => '0',
//                 'criteria1' => '-',
//                 'criteria2' => '-'
//                 );
// $arr[1] = array(
//                 'test_name' => 'ดันพื้นประยุกต์ 30 วินาที',
//                 'result1' => '0',
//                 'result2' => '0',
//                 'criteria1' => '-',
//                 'criteria2' => '-'
//                 );
// $arr[2] = array(
//                 'test_name' => 'ลุก-นั่ง 60 วินาทีี',
//                 'result1' => '0',
//                 'result2' => '0',
//                 'criteria1' => '-',
//                 'criteria2' => '-'
//                 );
// $arr[3] = array(
//                 'test_name' => 'ยืนยกเข่าขึ้นลง 3 นาที',
//                 'result1' => '0',
//                 'result2' => '0',
//                 'criteria1' => '-',
//                 'criteria2' => '-'
//                 );
//echo "<br> >>>>> ";
//echo "<br>";
// test_name
// $sql_time = "SELECT  ptt.test_name  FROM pfit_t_project_test ptpt, pfit_t_test ptt
//         WHERE ptpt.test_code = ptt.test_code
//         AND ptpt.project_code = 'M611101'
//         ORDER BY ptpt.test_seq ASC";
// $query_time = DbQuery($sql_time,null);
// $row_time  = json_decode($query_time, true);
// $data_time = $row_time['data'];

// value
// $sqlv = "SELECT * FROM pfit_t_result_history
//         WHERE person_number = '$person_number'
//         AND project_code = '$projectCode'
//         ";
// $queryv = DbQuery($sqlv,null);
// $rowv  = json_decode($queryv, true);
// $numv   = $rowv['dataCount'];
// $datav = $rowv['data'];

echo(json_encode(array('status' => '200','message' => 'สำเร็จ', 'dataList' => $arr , 'dataTime' => $date)));


?>
