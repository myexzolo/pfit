function operateOpen(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.category_criteria_code+'\',\'VIEW\',\''+row.category_criteria_name+'\')" ',
        '<span class="fa fa-search"></span>',
        '</a>'
    ].join('');
}
function operateEdit(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.category_criteria_code+'\',\'\',\''+row.category_criteria_name+'\')"',
        '<span class="fa fa-file-text-o"></span>',
        '</a>'
    ].join('');
}

function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}

function runNo(value, row, index) {
  return [index+1].join('');
}

function operateDelete(value, row, index) {
    return [
        '<a onclick="formDel(\''+row.category_criteria_code+'\',\''+row.category_criteria_name+'\')" ',
        '<span class="glyphicon glyphicon-trash"></span>',
        '</a>'
    ].join('');
}
function searchData(no,status ,name){
  $('#myModal').modal({backdrop:'static'});
  formShows(no,status,name);
}
function formShows(code,status,name){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  $.get("ajax/PFIT0200/form.php?code="+code+"&status="+status+"&nameCategory="+name)
  .done(function( data ) {
      $("#formShow").html(data);
  });

}
function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT0200/delete.php",{code:code})
        .done(function( data ) {
          if(data.status == "success"){
            $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          }else{
            $.smkAlert({text: data.message,type: data.status});
          }
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

$('#formData').on('submit', function(event) {
  event.preventDefault();
  if ($('#formData').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT0200/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $('#myModal').modal('hide');
      $.smkProgressBar();
      setTimeout(function(){
        $('#formData').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
        $.smkProgressBar({status:'end'});
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});


function add(){
  var wrapper         = $("#input_fields_wrap"); //Fields wrapper
  var add_button      = $(".add_field_button"); //Add button ID
  var rowData         = $('.rowData').length;
  var max_fields      = 5; //maximum input boxes allowed
  rowData++;
  var arrNameCategory  = $("input[name='category_criteria_detail_code[]']");
  var num = arrNameCategory.length - 1 ;
  var value = arrNameCategory[num].value;
  console.log(value);
  var codeCategory        = value.substr(0, 5);
  var codeCriteriaDetail  = value.substr(5, 1);
  var code  = codeCategory+(parseInt(codeCriteriaDetail) + 1);
  console.log(code);
  if(rowData <= max_fields){ //max input box allowed
          var id = 'row_'+ code;
          $(wrapper).append('<div id="'+id+'" class="rowData">'+
          '<div class="col-md-1">'+
             '<div class="form-group">'+
               '<button class="btn btn-danger" type="button" onclick="remove(\''+id+'\')"><span class="glyphicon glyphicon-trash"></span></button>'+
             '</div>'+
           '</div>'+
          '<div class="col-md-2">'+
             '<div class="form-group">'+
               '<input type="text"  name="category_criteria_detail_code[]" autocomplete="off" value="'+code+'" class="form-control" placeholder="รหัส" readonly required>'+
             '</div>'+
           '</div>'+
           '<div class="col-md-3">'+
            '<div class="form-group">'+
              '<input type="text" name="category_criteria_detail_name[]" autocomplete="off" class="form-control" placeholder="รายการผลการทดสอบ" required>'+
            '</div>'+
          '</div>'+
          '<div class="col-md-3">'+
            '<div class="form-group">'+
              '<input type="number" name="category_criteria_detail_value[]" autocomplete="off" class="form-control" placeholder="คะแนนการทดสอบ" required>'+
            '</div>'+
          '</div>'+
          '<div class="col-md-3">'+
            '<div class="form-group">'+
              '<select name="statusCD[]"class="form-control">'+
                '<option value="Y" selected>ใช้งาน</option>'+
                '<option value="N">ไม่ใช้งาน</option>'+
              '</select>'+
            '</div>'+
          '</div>'+
          '</div>'); //add input box
      }else{
        //alert('max_fields 5');
      }
}
function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}
