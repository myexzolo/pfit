
  $(function () {
    $('.datepicker').datepicker();
    searchProject();
  })


function searchProject(){
  //var date = $('#s_year').val()+"/"+$('#s_month').val() + "/01";
  var dateStart = dateThToEn($('#dateStart').val(),"dd/mm/yyyy","/");
  var dateEnd   = dateThToEn($('#dateEnd').val(),"dd/mm/yyyy","/");

  $.post("ajax/report01/showProjectList.php",{dateStart:dateStart,dateEnd:dateEnd})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}
