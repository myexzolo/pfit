function randomPass(){
  var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz!&#@";
  var pass = "";
  var maxs = 10;
  for(var x=0; x<=maxs; x++){
    var i = Math.floor(Math.random()*chars.length);
    pass += chars.charAt(i);
  }
  return pass;
}

function showForm(value){
  $.post("ajax/role/formRole.php",{value:value})
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
  });
}

function showPage(){
  $.post("ajax/role/showRole.php")
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function generatePass(){
  $('.pass').val(randomPass());
}

function removeRole(value){
  $.smkConfirm({
    text:'Are You Sure?',
    accept:'Yes',
    cancel:'Cancel'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/role/delRole.php",{value:value})
        .done(function( data ) {
          $.smkProgressBar();
          setTimeout(function(){
            $.smkProgressBar({status:'end'});
            showPage();
            $.smkAlert({text: data.message,type: data.status});
          }, 1000);
      });
    }
  });
}

$("#panel2").smkPanel({
  hide: 'full,remove'
});
showPage();

$('#formAddRole').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAddRole').smkValidate()) {
    // alert(1);
    $.ajax({
        url: 'ajax/role/manageRole.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formAddRole').smkClear();
        showPage();
        $.smkAlert({text: data.message,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    });
  }
});
