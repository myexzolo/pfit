function showForm(member_id,invoice_code,data,receive_id){
  $.post("ajax/invoice/formTax.php",{member_id:member_id,invoice_code:invoice_code,data:data,receive_id:receive_id})
    .done(function( data ) {
      $('#myModal').modal('toggle');
      $('#show-form').html(data);
  });
}

function show(date){
  $.post("ajax/report/report2.php",{startDate:date})
    .done(function( data ) {
      $('#show-page').html(data);
  });
}

function reset(){
  $('#dateStart').val($('#dateStartTmp').val());
  search();
}

function search(){
  var date = $('#dateStart').val().replace(/-/g , "/");
  var dateTH = toDate(date);
  $('#dateTh').html(dateTH);
  show(date);
}

function printDiv(divName) {
  var originalContents = document.body.innerHTML;
  document.getElementById("tableDisplay").style.fontSize = "11px";
  var printContents = document.getElementById(divName).innerHTML;
  document.body.innerHTML = printContents;
  window.print();
  document.body.innerHTML = originalContents;
}

function printInvice(){
  var receive_id = $('#receive_id').val();
  var data = $('#data').val();
  var name = $('#name').val();
  var invoice_code = $('#invoiceCode').val();
  var taxId = $('#taxId').val();
  var address = $('#address').html();

  //var pam = "";
  var pam = "?receive_id="+receive_id+"&&name="+name+"&&data="+data+"&&invoice_code="+invoice_code+"&&taxId="+taxId+"&&address="+address;
  //alert(pam);
  //var myWindow = window.open("pos_process.php"+pam, "", "width=5,height=5");
  var myWindow = window.open("print_invoice.php"+pam,'', 'toolbar=no,status=no,menubar=no,scrollbars=no,resizable=no,visible=none', '');
  $('#myModal').modal('hide');
  $('#formAdd').smkClear();
}

$('#formAdd').on('submit', function(event) {
  event.preventDefault();
  if ($('#formAdd').smkValidate()) {
    $.ajax({
        url: 'ajax/invoice/print_invoice.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      //alert(data.message);
      $('#myModal').modal('toggle');
      // $.smkProgressBar();
      // setTimeout(function(){
      //   $.smkProgressBar({status:'end'});
      //   $('#formAdd').smkClear();
      //   showPage();
      //   $.smkAlert({text: data.message,type: data.status});
      //   $('#myModal').modal('toggle');
      // }, 1000);
    }).fail(function (jqXHR, exception) {
        // setTimeout(function(){
        //   $.smkProgressBar({status:'end'});
        //   $('#formAdd').smkClear();
        //   showPage();
        //   $.smkAlert({text: jqXHR.responseText ,type: 'danger'});
        //   $('#myModal').modal('toggle');
        // }, 1000);
    });
  }
});


$("#panel2").smkPanel({
  hide: 'full,remove'
});

function toDate(dateStr) {
    var parts = dateStr.split("/");
    return parts[2]+"/"+parts[1]+"/"+parts[0];
}

$(document).ready(function() {
  search();
});
