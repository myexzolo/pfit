function operateOpen(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.test_code+'\',\'VIEW\',\''+row.test_name+'\')" ',
        '<span class="fa fa-search"></span>',
        '</a>'
    ].join('');
}
function operateEdit(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.test_code+'\',\'\',\''+row.test_name+'\')"',
        '<span class="fa fa-file-text-o"></span>',
        '</a>'
    ].join('');
}

function operatePDF(value, row, index) {
  var pram = '?test_code='+row.test_code;
    return [
        '<a onclick="postURL_blank(\'pdf.php'+pram+'\')"',
        '<span class="fa fa-qrcode"></span>',
        '</a>'
    ].join('');
}

function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function getObjectStr(value, row, index){
  return [getTestOpjective(row.test_opjective)].join('');
}

function getAgeRange(value, row, index){
  var range = "-";
  if(row.test_age_min > 0 && row.test_age_max > 0){
     range = row.test_age_min + " - " + row.test_age_max
  }
  return [range].join('');;
}


function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}

function runNo(value, row, index) {
  return [index+1].join('');
}

function operateDelete(value, row, index) {
    return [
        '<a onclick="formDel(\''+row.test_code+'\',\''+row.test_name+'\')" ',
        '<span class="glyphicon glyphicon-trash"></span>',
        '</a>'
    ].join('');
}
function searchData(no ,status ,name){
  $('#myModal').modal({backdrop:'static'});
  formShows(no,status,name);
}
function formShows(code,status,name){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  $.get("ajax/PFIT0201/form.php?code="+code+"&status="+status+"&nameCategory="+name)
  .done(function( data ) {
      $("#formShow").html(data);
      CKEDITOR.replace( 'content_data',{
        height: '200px'
      } );
  });

}
function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ' + name + ' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT0201/delete.php",{code:code})
        .done(function( data ) {
          if(data.status == "success"){
            $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          }else{
            $.smkAlert({text: data.message,type: data.status});
          }
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

$('#formData').on('submit', function(event) {
  event.preventDefault();
  // if ($('#formData').smkValidate()) {
  //
  //   $.post("ajax/PFIT0201/manage.php", {
  //     content_data : CKEDITOR.instances['content_data'].getData(),
  //     content_id : $('#content_id').val()
  //   })
  //   .done(function( data ) {
  //     $.smkProgressBar();
  //     setTimeout(function(){
  //       $.smkProgressBar({status:'end'});
  //       $.smkAlert({text: data.message,type: data.status});
  //     }, 1000);
  //   });
  // }

  if ($('#formData').smkValidate()) {
    var id = $('#content_data').val(CKEDITOR.instances['content_data'].getData());
    console.log(id);
    $.ajax({
        url: 'ajax/PFIT0201/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $('#myModal').modal('toggle');
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});

function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}
