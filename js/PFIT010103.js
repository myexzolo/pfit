function operateAdd(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'EDIT\',\''+row.person_number+'\')" >',
        '<span class="glyphicon glyphicon-plus-sign"></span>',
        '</a>'
    ].join('');
}
function operateEdit(value, row, index) {
    return [
        '<a onclick="searchData(\''+row.project_code+'\',\'EDIT\',\''+row.person_number+'\')" >',
        '<span class="glyphicon glyphicon-list-alt"></span>',
        '</a>'
    ].join('');
}

function operateExport(value, row, index) {
    return [
        '<a alert("พิมพ์") >',
        '<span class="glyphicon glyphicon-print"></span>&nbsp;',
        '</a>'
    ].join('');
}

function getDateToTH(value, row, index){
  var date =  row.date_of_birth.split("-");
  var dob  = date[2] + '/' + date[1] + '/' +  (parseInt(date[0]) + 543);
  return [dob].join('');
}

function getStatus(value, row, index){
  var status = row.status;
  var str = "ไม่ใช้งาน";
  if(status == 'Y'){
    str = "ใช้งาน";
  }
  return [str].join('');
}

function getDateToTH(value, row, index){
  var date =  row.date_of_birth.split("-");
  var dob  = date[2] + '/' + date[1] + '/' +  (parseInt(date[0]) + 543);
  return [dob].join('');
}

function getGender(value, row, index){
  var status = row.person_gender;
  var str = "ชาย";
  if(status == 'F'){
    str = "หญิง";
  }
  return [str].join('');
}

function runNo(value, row, index) {
  return [index+1].join('');
}

function openModal(projectCode,projectName){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,'ADD','');
}


function searchData(projectCode ,status ,code){
  $('#myModal').modal({backdrop:'static'});
  formShows(projectCode,status,code);
}

function formShows(projectCode,status,code){
  if(status == "VIEW"){
    $('#submitFormData').hide();
  }else{
    $('#submitFormData').show();
  }
  $.get("ajax/PFIT010103/form.php?code="+code+"&status="+status+"&projectCode="+projectCode)
  .done(function( data ) {
      $("#formShow").html(data);
  });
}

function formShowPerson(){
  $('#loading').removeClass( "none" );
  $('#loading').addClass('loading');
  var projectCode = $("#projectCode").val();
  var personId = $("#personId").val();
  var personName = $("#personName").val();
  var personLname = $("#personLname").val();

  $.post("ajax/PFIT010103/list.php",{projectCode:projectCode,personId:personId,personName:personName,personLname:personLname})
  .done(function( data ) {
      $("#formShow").html(data);
      $('#loading').removeClass('loading');
      $('#loading').addClass('none');
       var num = $('#numPerson').val();
       if(num == 0){
         $('.btnReport').attr('disabled','disabled');
       }
  });
}
function formDel(code,name){
  $.smkConfirm({
    text:'ต้องการลบข้อมูล ' + name + ' ?',
    accept:'ตกลง',
    cancel:'ยกเลิก'
  },function(res){
    // Code here
    if (res) {
      $.post("ajax/PFIT010103/delete.php",{code:code})
        .done(function( data ) {
          $.smkAlert({text: 'ลบข้อมูล '+name + ' เรียบร้อย!! ', type:'success'});
          $('#table').bootstrapTable('refresh');
      });
    }
  });
}

$('#formData').on('submit', function(event) {
  event.preventDefault();

  if ($('#formData').smkValidate()) {
    $.ajax({
        url: 'ajax/PFIT010103/manage.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        $.smkAlert({text: data.message,type: data.status});
        $('#table').bootstrapTable('refresh');
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
    });;
  }
});

$('#formSearch').on('submit', function(event) {
  event.preventDefault();
  if ($('#formSearch').smkValidate()) {
    formShowPerson();
  }
});

function remove(rowid){
  var rowData  = $('.rowData').length;
  if(rowData<=1){
    //alert("Not Remove");
  }else{
    $('#'+rowid).remove();
  }
}

function postURL(url, multipart) {
 var form = document.createElement("FORM");
 form.method = "POST";
 if(multipart) {
   form.enctype = "multipart/form-data";
 }
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}

function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 form.submit();
}
