<?php

function getServerName(){
  return "http://sportsci.dpe.go.th";
}
function is_connected()
  {
    $connected = @fsockopen("www.google.com", 80);//website, port  (try 80 or 443)
    if ($connected){
        $is_conn = 'C'; //action when connected
        fclose($connected);
    }else{
        $is_conn = 'D'; //Dist connection
    }
    return $is_conn;

  }

  function queryDateOracle($formatDate){
    if($formatDate != ""){
         $formatDate = "TO_DATE('$formatDate','YYYY-MM-DD')";
    }
    return $formatDate;
  }

function queryDate($formatDate){
  if($formatDate != ""){
    if($_SESSION['TYPE_CONN'] == '1'){
       $formatDate = "TO_DATE('$formatDate','YYYY-MM-DD')";
    }else{
      $formatDate = "'$formatDate'";
    }
  }
  return $formatDate;
}


function DateTimeThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));
		$strHour= date("H",strtotime($strDate));
		$strMinute= date("i",strtotime($strDate));
		$strSeconds= date("s",strtotime($strDate));
		return "$strDay/$strMonth/$strYear $strHour:$strMinute:$strSeconds";
}


function DateThai($strDate){
		$strYear = date("Y",strtotime($strDate))+543;
		$strMonth= date("m",strtotime($strDate));
		$strDay= date("d",strtotime($strDate));

		return "$strDay/$strMonth/$strYear";
}


function dateThToEn($date,$format,$delimiter)
{
    $formatLowerCase  = strtolower($format);//var formatLowerCase=format.toLowerCase();
    $formatItems      = explode($delimiter,$formatLowerCase);//var formatItems=formatLowerCase.split(delimiter);
    $dateItems        = explode($delimiter,$date);//var dateItems=date.split(delimiter);
    $monthIndex       = array_search("mm",$formatItems);//var monthIndex=formatItems.indexOf("mm");
    $dayIndex         = array_search("dd",$formatItems);//var dayIndex=formatItems.indexOf("dd");
    $yearIndex        = array_search("yyyy",$formatItems);//var yearIndex=formatItems.indexOf("yyyy");
    $month            = $dateItems[$monthIndex];//var month=parseInt(dateItems[monthIndex]);

    $yearth = $dateItems[$yearIndex];
    if( $yearth > 2450){
      $yearth -= 543;
    }
    $dateEN = $yearth."-".sprintf("%02d", $month)."-".sprintf("%02d", $dateItems[$dayIndex]);
    return $dateEN;
}

function calTestFit($total,$score,$per){
  $txt = "ข้อมูลไม่ครบ";
  $calper = ($score/$total) * 100;
  //echo $calper."<br>";
  if($calper >= $per)
  {
    $txt = "ผ่าน";
  }
  else if($calper <$per)
  {
    $txt = "ไม่ผ่าน";
  }
  return $txt;
}


function queryDateTime($formatDate){
  if($formatDate != ""){
    if($_SESSION['TYPE_CONN'] == '1'){
       $formatDate = "TO_DATE('$formatDate','yyyy-mm-dd hh24:mi:ss')";
    }else{
      $formatDate = "'$formatDate'";
    }
  }
  return $formatDate;
}


function getQueryDate($str){
  if($str != ""){
     if($_SESSION['TYPE_CONN'] == '1'){
        $str = " TO_CHAR($str,'YYYY-MM-DD') as $str ";
     }
  }
  return $str;
}

function getQueryDateWere($str){
  if($str != ""){
     if($_SESSION['TYPE_CONN'] == '1'){
        $str = " TO_CHAR($str,'YYYY-MM-DD') ";
     }
  }
  return $str;
}

function getQueryDateV2($str, $format){
  if($str != ""){
     if($_SESSION['TYPE_CONN'] == '1'){
        $str = " TO_CHAR($str,'$format') as $str ";
     }else{
        $str = " DATE_FORMAT($str,'$format') as $str ";
     }
  }
  return $str;
}

function formatDateThtoEn($date){
  $Datestr = "";
  if(!empty($date) && strlen($date) >= 8){
    $date = str_replace ('//','/',$date);
    $date = str_replace ('-','/',$date);
    $dt = explode("/", $date);
    $d  = $dt[0];
    $m  = $dt[1];
    $y  = $dt[2];

    if($y > 2400){
      $y -= 543;
    }
    if(strlen($d) == 1){
      $d  = "0".$d;
    }

    if(strlen($m) == 1){
      $m  = "0".$m;
    }

    $date = $y."/".$m."/".$d;
    //echo $date."<br>";
    if(checkdate($m,$d,$y)){
      $Datestr = date("Y-m-d", strtotime($date));
    }
  }
  //echo $Datestr."<br>";
  return $Datestr;
}

function getTestOpjective($id){
  $str = "";
  if($id =="1" ){ $str = "ความแข็งแรงและความอดทนของกล้ามเนื้อแขน";}
  else if($id =="2" ){$str = "ความแข็งแรงและความอดทนของกล้ามเนื้อขา";}
  else if($id =="3" ){$str = "ความแข็งแรงและความอดทนของกล้ามเนื้อหน้าท้อง";}
  else if($id =="4" ){$str = "ความอ่อนตัวของกล้ามเนื้อต้นขาด้านหลัง/หลังล่าง";}
  else if($id =="5" ){$str = "ความอ่อนตัวของกล้ามเนื้อไหล่/แขน";}
  else if($id =="6" ){$str = "ความอ่อนตัวของกล้ามเนื้อลำตัว";}
  else if($id =="7" ){$str = "ความอดทนของระบบหัวใจและไหลเวียนเลือด";}
  else if($id =="8" ){$str = "ความแคล่วคล่องว่องไว";}
  else if($id =="9" ){$str = "ความเร็ว";}
  else if($id =="10"){$str = "ปฏิกิริยาตอบสนอง";}
  else if($id =="11"){$str = "การทรงตัว";}
  else if($id =="12"){$str = "กำลัง/พลังกล้ามเนื้อขา";}
  else if($id =="13"){$str = "องค์ประกอบของร่างกาย";}
  else if($id =="14"){$str = "น้ำหนักของกระดูก Bone Mass (Kg)";}
  else if($id =="15"){$str = "ระดับไขมันช่องท้อง Visceral Fat (Ratio)";}
  else if($id =="16"){$str = "การเผาพลาญพลังงานของร่างกาย BMR (Kcal)";}
  else if($id =="17"){$str = "เปรียบเทียบอายุการเผาพลาญ Matabolic Age (Year)";}
  else if($id =="18"){$str = "ดัชนีความหนาแน่นในร่างกาย BMI (Kg/m<sup>2</sup>)";}
  else if($id =="19"){$str = "การวัดสัดส่วนรอบเอว/รอบสะโพก WHR";}
  else{$str = "";}
  return $str;
}


function convert($number){
  $txtnum1 = array('ศูนย์','หนึ่ง','สอง','สาม','สี่','ห้า','หก','เจ็ด','แปด','เก้า','สิบ');
  $txtnum2 = array('','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน','สิบ','ร้อย','พัน','หมื่น','แสน','ล้าน');
  $number = str_replace(",","",$number);
  $number = str_replace(" ","",$number);
  $number = str_replace("บาท","",$number);
  $number = explode(".",$number);
  if(sizeof($number)>2){
    $number = number_format($number, 2);
  }

  $strlen = strlen($number[0]);
  $convert = '';
  for($i=0;$i<$strlen;$i++){
  	$n = substr($number[0], $i,1);
  	if($n!=0){
  		if($i==($strlen-1) AND $n==1){ $convert .= 'เอ็ด'; }
  		elseif($i==($strlen-2) AND $n==2){  $convert .= 'ยี่'; }
  		elseif($i==($strlen-2) AND $n==1){ $convert .= ''; }
  		else{ $convert .= $txtnum1[$n]; }
  		$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'บาท';
  if($number[1]=='0' || $number[1]=='00' || $number[1]==''){
  $convert .= 'ถ้วน';
  }else{
  $strlen = strlen($number[1]);
  for($i=0;$i<$strlen;$i++){
  $n = substr($number[1], $i,1);
  	if($n!=0){
  	if($i==($strlen-1) AND $n==1){$convert
  	.= 'เอ็ด';}
  	elseif($i==($strlen-2) AND
  	$n==2){$convert .= 'ยี่';}
  	elseif($i==($strlen-2) AND
  	$n==1){$convert .= '';}
  	else{ $convert .= $txtnum1[$n];}
  	$convert .= $txtnum2[$strlen-$i-1];
  	}
  }
  $convert .= 'สตางค์';
  }
  return $convert;
}

function convDatetoThai($date){
  $year  = date('Y',strtotime($date)) < 2500?date('Y',strtotime($date))+543:date('Y',strtotime($date));
  $dm = date('d/m/',strtotime($date));
  return "$dm$year";
}

function monthThai($month){
  $arr = array('','ม.ค.','ก.พ.','มี.ค.','เม.ย.','พ.ค.','มิ.ย.','ก.ค.','ส.ค.','ก.ย.','ต.ค.','พ.ย.','ธ.ค.');
  return $arr[$month];
}

function monthThaiFull($month){
  $arr = array('','มกราคม','กุมภาพันธ์','มีนาคม','เมษายน','พฤษภาคม','มิถุนายน','กรกฎาคม','สิงหาคม','กันยายน','ตุลาคม','พฤศจิกายน','ธันวาคม');
  return $arr[$month];
}

function convDatetoThaiMonth($date){
  $day   = date('d',strtotime($date));
  $month = monthThai(date('n',strtotime($date)));
  $year  = substr(date("Y", strtotime($date))+543,2,2);
  return "$day $month $year";
}

function resizeImageToBase64($obj,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($obj) && !empty($obj["name"]))
  {
    if(getimagesize($obj['tmp_name']))
    {
      $ext = pathinfo($obj["name"], PATHINFO_EXTENSION);
      if($ext == 'gif' || $ext !== 'png' || $ext !== 'jpg' )
      {
        if(!empty($h) || !empty($w))
        {
          $filePath       = $obj['tmp_name'];
          $image          = addslashes($filePath);
          $name           = addslashes($obj['name']);
          $new_images     = "thumbnails_".$user_id_update;

          $x  = 0;
          $y  = 0;

          list($width_orig, $height_orig) = getimagesize($filePath);

          if(empty($h)){
              if($width_orig > $w){
                $new_height  = $height_orig*($w/$width_orig);
                $new_width   = $w;
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else if(empty($w))
          {
              if($height_orig > $h){
                $new_height  = $h;
                $new_width   = $width_orig*($h/$height_orig);
              }else{
                $new_height  = $height_orig;
                $new_width   = $width_orig;
              }
          }
          else
          {
            if($height_orig > $width_orig)
            {
              $new_height  = $height_orig*($w/$width_orig);
              $new_width   = $w;
            }else{
              $new_height  = $h;
              $new_width   = $width_orig*($h/$height_orig);
            }
          }

          $create_width   =  $new_width;
          $create_height  =  $new_height;


          if(!empty($h) && $new_height > $h){
             $create_height = $h;
          }

          if(!empty($w) && $new_width > $w){
            $create_width = $w;
          }


          $imageOrig;
          $imageResize    = imagecreatetruecolor($create_width, $create_height);
          $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
          imagecolortransparent($imageResize, $background);
          imagealphablending($imageResize, false);
          imagesavealpha($imageResize, true);

          if($ext == 'png'){
            $imageOrig      = imagecreatefrompng($filePath);
            $new_images     = $new_images.".png";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagepng($imageResize,$path.$new_images);
          }else if ($ext == 'jpg'){
            $imageOrig      = imagecreatefromjpeg($filePath);
            $new_images     = $new_images.".jpg";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagejpeg($imageResize,$path.$new_images);
          }else if ($ext == 'gif'){
            $imageOrig      = imagecreatefromgif($filePath);
            $new_images     = $new_images.".gif";
            imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
            imagegif($imageResize,"$path".$new_images);
          }

          imagedestroy($imageOrig);
          imagedestroy($imageResize);

          $image   = file_get_contents($path.$new_images);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }else{
          $image   = file_get_contents($path.$name);
          $img     = 'data:image/png;base64,'.base64_encode($image);
        }

      }
    }
  }
  return $img;
}

function getTypeActive($typeActive){
  $unitName = "";
  if($typeActive == 1){
      $unitName = "วันที่ชำระเงิน";
  }else if($typeActive == 2){
      $unitName = "วันที่ Check-In ครั้งแรก";
  }else if($typeActive == 3){
      $unitName = "กำหนดเอง";
  }else{
      $unitName = "";
  }
  return $unitName;
}

function getUnitName($package_unit){
  $unitName = "";
  if($package_unit == 1){
      $unitName = "วัน";
  }else if($package_unit == 2){
      $unitName = "เดือน";
  }else if($package_unit == 3){
      $unitName = "ปี";
  }else if($package_unit == 4){
      $unitName = "ครั้ง";
  }else if($package_unit == 5){
      $unitName = "ตลอดชีพ";
  }else{
      $unitName = "";
  }
  return $unitName;
}

function getGender($value){
  $str = "ชาย";
  if($value == 'F'){
      $str = "หญิง";
  }
  return $str;
}

function resizeImageBase64($imgBase,$h,$w,$quality,$user_id_update,$path) {
  $img = "";
  if(isset($imgBase) && !empty($imgBase))
  {
    if(!empty($h) || !empty($w))
    {
      $imageOrig      = imagecreatefromstring(base64_decode($imgBase));
      $new_images     = "thumbnails_".$user_id_update;

      $x  = 0;
      $y  = 0;

      $width_orig   = 400;
      $height_orig  = 300;

      if(empty($h)){
          if($width_orig > $w){
            $new_height  = $height_orig*($w/$width_orig);
            $new_width   = $w;
          }else{
            $new_height  = $height_orig;
            $new_width   = $width_orig;
          }
      }
      else if(empty($w))
      {
          if($height_orig > $h){
            $new_height  = $h;
            $new_width   = $width_orig*($h/$height_orig);
          }else{
            $new_height  = $height_orig;
            $new_width   = $width_orig;
          }
      }
      else
      {
        if($height_orig > $width_orig)
        {
          $new_height  = $height_orig*($w/$width_orig);
          $new_width   = $w;
        }else{
          $new_height  = $h;
          $new_width   = $width_orig*($h/$height_orig);
        }
      }

      $create_width   =  $new_width;
      $create_height  =  $new_height;


      if(!empty($h) && $new_height > $h){
         $create_height = $h;
      }

      if(!empty($w) && $new_width > $w){
        $create_width = $w;
      }


      $imageOrig;
      $imageResize    = imagecreatetruecolor($create_width, $create_height);
      $background     = imagecolorallocatealpha($imageResize, 255, 255, 255, 127);
      imagecolortransparent($imageResize, $background);
      imagealphablending($imageResize, false);
      imagesavealpha($imageResize, true);

      $new_images     = $new_images.".png";
      imagecopyresampled($imageResize, $imageOrig, 0, 0, 0, 0, $new_width, $new_height, $width_orig, $height_orig);
      imagepng($imageResize,$path.$new_images);


      imagedestroy($imageOrig);
      imagedestroy($imageResize);

      $image   = file_get_contents($path.$new_images);
      $img     = 'data:image/png;base64,'.base64_encode($image);
    }
  }
  return $img;
}

function formatDateTh($date){
  $Datestr = "";
  if(!empty($date)){
    $d = strtotime($date);
    $Datestr = date("d/m/Y", $d);
  }
  return $Datestr;
}


function formatDateThV2($date){
  $Datestr = "";
  if(!empty($date)){
    $date = str_replace ('-','/',$date);
    $dt = explode("/", $date);
    $d  = $dt[2];
    $m  = $dt[1];
    $y  = $dt[0];

    if($y < 2500){
      $y += 543;
    }
    $Datestr = $d."/".$m."/".$y;
  }
  return $Datestr;
}

function chkNull($obj){
  if(!isset($obj) || empty($obj)){
    $obj = "";
  }
  return $obj;
}

function chkNullZero($obj){
  if(!isset($obj) || empty($obj)){
    $obj = "0";
  }
  return $obj;
}

function yearBirth($date){
  $d1 = new DateTime(date('Y-m-d'));
  $d2 = new DateTime($date);
  $diff = $d2->diff($d1);
  return $diff->y;
}

function yearBirth2($date,$date2){
  // $d1 = new DateTime($date2);
  // $d2 = new DateTime($date);
  // $diff = $d2->diff($d1);
  // return $diff->y;

  ///คำนวนอายุ = ปี พ.ศ. ของวันที่ทำการทดสอบ(อิงตามวันสิ้นสุดขอกิจกรรม)  - ปีพ.ศ.ของวันที่เกิด
  $dob = date('Y', strtotime($date));
  $d2  = date('Y', strtotime($date2));
  $diff = intval($d2) - intval($dob);
  return $diff;
}

function dateDifference($date_1 , $date_2 , $differenceFormat)
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);

    $interval = date_diff($datetime1, $datetime2);

    return $interval->format($differenceFormat);

}
// Vule //
function bmi($wiegth,$height){
  $str = '';
  if($height > 0 ){
    $bmi = number_format($wiegth/(pow($height/100,2)),2);
  }else{
    $bmi = 0;
  }
  return $bmi;
}

function ValuePerWiegth($value,$wiegth){

  return empty($value)?"":number_format($value/$wiegth,2);

}

function ValuePerHeight($value,$height){
  return empty($value)?"":number_format($value/$height,2);
}

function ValuePerTwo($value){
  return empty($value)?"":ceil($value/2);
}

function getResultCalculator($test_calculator,$result,$wiegth,$height,$yearBirth,$person_gender,$projectCode,$person_number){
  $res = 0;
  if($test_calculator == 1){
    $res = $result;
  }else if($test_calculator == 2){
    $res = ValuePerWiegth($result,$wiegth);
  }else if($test_calculator == 3){
    $res = ValuePerHeight($result,$height);
  }else if($test_calculator == 4){
    $value  = explode("|",$result);
    $wiegth = $value[0];
    $height = $value[1];

    $sql = "UPDATE PFIT_T_PERSON SET
              WIEGTH = '$wiegth',
              HEIGHT = '$height'
              WHERE  project_code = '$projectCode' and person_number = '$person_number'";
    //echo $sqlr;
     DbQuery($sql,null);

    $res = bmi($wiegth,$height);
  }else if($test_calculator == 5){
    $res = VoToMax($yearBirth,$wiegth,$result,$person_gender);
  }else if($test_calculator == 6){
    $res = BloodPressureValue($result);
  }else if($test_calculator == 7){
    $res = ValuePerTwo($result);
  }
  return $res;
}


function BloodPressureValue($value){
  $str = '';
  if(!empty($value)){
    $value  = explode("|",$value);
    $str    = $value[0];
  }
  return $str;
}

function BloodPressure($value){
  $str = '';
  if($value >= 141){
    $str = 'ความดันโลหิตสูง';
  }else if($value >= 100){
    $str = 'ความดันโลหิตปกติ';
  }else{
    $str = 'ความดันโลหิตต่ำ';
  }
  return $str;
}

function Pulsation($value){
  $str = '';
  if($value >= 71){
    $str = 'หัวใจเต้นอยู่ในระดับสูง';
  }else if($value >= 60){
    $str = 'หัวใจเต้นอยู่ในระดับปกติ';
  }else{
    $str = 'หัวใจเต้นอยู่ในระดับดี';
  }
  return $str;
}

function VoToMax($age,$wiegth,$value,$gender){
  //echo ">>>".$value;
  if(!empty($value)){
    $value = explode("|",$value);
    $sql = "SELECT * FROM pfit_t_max_oxygen WHERE pulserate = '$value[0]' AND load_kpm = '$value[1]' AND gender = '$gender'";
    //echo $sql;
    $query = DbQuery($sql,null);
    $json  = json_decode($query, true);
    $row   = $json['data'];
    $num   = $json['dataCount'];
    if($num > 0){
      $valOx = $row[0]['value'];

      $sql = "SELECT * FROM pfit_t_agefactor WHERE age = '$age'";
      $query = DbQuery($sql,null);
      $json  = json_decode($query, true);
      $row   = $json['data'];
      $num   = $json['dataCount'];
      if($num > 0){
        $valage = $row[0]['value'];
        $ans = ($valage*$valOx*1000)/$wiegth;
        return number_format($ans,1);
      }else{
        return "";
      }
    }else{
      return "";
    }
  }else{
    return "";
  }



}

?>
