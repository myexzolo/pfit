<!DOCTYPE html>
<html>

<?php
include("inc/head.php");
$typeConnect = 1;
$display = "none";
if($typeConnect == 2){
  $display = "block";
}

function getRealIpAddr()
{
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    }
    elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
    }
    else
    {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}




//echo "IP :".$_SERVER["REMOTE_ADDR"];

?>
<body class="bgDEP">

  <div class="row">
      <div class="col-md-4 col-md-offset-4">
        <div class="login" style="margin-top: 20%;">
          <div align="center"><img src="images/dep_logo.png"  class="logo-login"></div>
          <h4 style="color:#FFFFFF;text-align:center;font-size:20px;">ระบบจัดเก็บและรายงานผลการทดสอบสมรรถภาพทางกาย</h3>
          <form id="form1" smk-icon="glyphicon-remove" novalidate autocomplete="off" >
              <div class="form-group">
                <input name="user" id="user" type="text" class="form-control" placeholder="Username" smk-text="กรุณากรอก Username" required>
              </div>
              <div class="form-group">
                <input name="pass" id="pass" type="password" autocomplete="new-password" class="form-control" placeholder="Password"  smk-text="กรุณากรอก Password" required>
              </div>
              <div class="form-group checkbox" style="display:none;">
                <label style="color:#fff">
                  <!-- typeConnect = 1 online  2 = offline-->
                  <input type="checkbox" data-toggle="toggle" id="toggleOffline" value="">
                  <input type="hidden" name="typeConnect" id="typeConnect" value="<?= $typeConnect ?>">
                  Offline IP Address
                </label>
              </div>
              <div class="form-group" id="offline" style="display:none;">
                <input name="ip" id="ip" type="text" pattern="((^|\.)((25[0-5])|(2[0-4]\d)|(1\d\d)|([1-9]?\d))){4}$" class="form-control" placeholder="IP Address">
              </div>
              <button type="submit" class="btn bg-navy btn-block non-radius">LOGIN</button>
              <table width="100%" style="margin-top:10px;">
                <tr>
                  <td style="text-align:left;" width="50%">
                    <div style="display:<?= $display ?>;color:#eee;cursor: pointer;" onclick="upload()">นำเข้า User<div/>
                  </td>
                  <td style="text-align: right;">
                    <a href="download/userManual.pdf" target="_blank" style="color:#eee">คู่มือการใช้งาน<a/>
                  </td>
                </tr>
              </table>
              <div class="form-group" >
              </div>
          </form>
        </div>
      </div>
    </div>

    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">นำเข้า User</h4>
          </div>
          <form id="formData" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" >
            <div class="modal-body" id="formShow">
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
              <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">นำเข้า</button>
            </div>
          </form>
        </div>
      </div>
    </div>

<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>
<script type="text/javascript">

$(document).ready(function() {
    $("#user").focus();
    $('#form1').on("submit",function(e) {

        if ($("#form1").smkValidate()) {
          $.ajax({
                url: 'ajax/login/checkLogin.php',
                type: 'POST',
                data: new FormData( this ),
                processData: false,
                contentType: false,
                dataType: 'json'
            }).done(function( data ) {
                if (data.status == "danger") {
                    $.smkAlert({text: data.message , type: data.status});
                    //$("#form1").smkClear();
                    $("#pass").smkClear();
                    $("#user").focus();
                } else {
                    $.smkProgressBar({
                      element:'body',
                      status:'start',
                      bgColor: '#000',
                      barColor: '#fff',
                      content: 'Loading...'
                    });
                    setTimeout(function(){
                      $.smkProgressBar({
                        status:'end'
                      });
                      window.location='index.php';
                    }, 1000);
                  }
            });
            e.preventDefault();
        }
       e.preventDefault();
    });
});

function GetComputerName() {
    try {
        var network = new ActiveXObject('WScript.Network');
        // Show a pop up if it works
        alert(network.computerName);
    }
    catch (e) {alert(e) }
}

function upload(){
  $('#myModal').modal({backdrop:'static'});
  $.get("ajax/login/formUpload.php")
  .done(function( data ) {
      $("#formShow").html(data);
  });
}

$(function() {
  $('#toggleOffline').change(function() {
    if($('#toggleOffline').prop('checked')){
      $('#offline').show();
      $("#ip").prop('required',true);
      //$('#typeConnect').val(2);

    }else{
      $('#offline').hide();
      $('#ip').val("");
      $("#ip").prop('required',false);
      //$('#typeConnect').val("1");
    }
  })
})


$('#formData').on('submit', function(event) {
  event.preventDefault();
  if ($('#formDataUpload').smkValidate()) {
    $.ajax({
        url: 'ajax/login/manageUpload.php',
        type: 'POST',
        data: new FormData( this ),
        processData: false,
        contentType: false,
        dataType: 'json'
    }).done(function( data ) {
      $.smkProgressBar();
      setTimeout(function(){
        $.smkProgressBar({status:'end'});
        $('#formData').smkClear();
        $.smkAlert({text: data.message ,type: data.status});
        $('#myModal').modal('toggle');
      }, 1000);
    }).fail(function (jqXHR, exception) {
        //alert(jqXHR);
        $('#myModal').modal('toggle');
    });;
  }
});

</script>
</body>
</html>
