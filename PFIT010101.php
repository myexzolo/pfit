<!DOCTYPE html>
<html>

<?php
  include("inc/head.php");

  $project_code   = isset($_POST['project_code'])?$_POST['project_code']:"";
  $project_name   = isset($_POST['project_name'])?$_POST['project_name']:"";
  $project_type   = isset($_POST['project_type'])?$_POST['project_type']:"";
  $m   = isset($_POST['m'])?$_POST['m']:"";
  $y   = isset($_POST['y'])?$_POST['y']:"";

  $urlback = "PFIT0101.php?m=$m&y=$y";

  if($project_code == ""){
    exit("<script>window.location='PFIT0101.php';</script>");
  }
?>

<style>
  th {
    text-align: center !important;
  }
</style>
<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        จัดเก็บรายชื่อผู้ทดสอบ
        <small>PFIT010101</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="PFIT0101.php"><i class="fa fa-clipboard"></i>จัดเก็บผลการทดสอบ</a></li>
        <li class="active">รายชื่อผู้ทดสอบ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">รายชื่อผู้ทดสอบ : <?= $project_name?> </h3>
                  <div class="box-tools pull-right">
                    <?php
                      $pram = "?project_name=$project_name&project_code=$project_code";
                      $url = 'print_all_qr.php'.$pram;
                    ?>
                    <button class="btn bg-navy" onclick="postURL_blank('<?=$url?>')"><i class="fa  fa-print"></i> พิมพ์ผู้ทดสอบทั้งหมด</button>
                    <button class="btn bg-navy" onclick="exportExcel('<?=$project_type?>')" ><i class="fa fa-file-excel-o"></i> Excel Template</button>
                    <button class="btn btn-primary" onclick="openModal('<?=$project_code?>','<?=$project_name?>','<?=$project_type?>')" style="width:80px;">เพิ่ม</button>
                    <button class="btn btn-success" onclick="upload('<?=$project_code?>','<?=$project_name?>','<?=$project_type?>')" style="width:80px;">นำเข้า</button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="table" class="table table-bordered table-striped"
                         data-toggle="table"
                         data-url="ajax/PFIT010101/show.php?code=<?= $project_code?>"
                         data-pagination="true"
                         data-page-size = 20
                         data-page-list= "[20, 50, 100, ALL]"
                         data-search="true"
                         data-flat="true"
                         data-show-refresh="true"
                         >
                      <thead>
                          <tr>
                              <th data-sortable="true" data-field="person_number" data-align="left">เลขประจำตัว</th>
                              <th data-sortable="true" data-field="person_name" data-align="left">ชื่อ</th>
                              <th data-sortable="true" data-field="person_lname" data-align="left">นามสกุล</th>
                              <th data-field"operate" data-formatter="getDateToTH" data-align="center">วันเดือนปีเกิด</th>
                              <th data-sortable="true" data-field="wiegth" data-align="right">น้ำหนัก</th>
                              <th data-sortable="true" data-field="height" data-align="right">ส่วนสูง</th>
                              <th data-field"operate" data-formatter="getGender" data-align="center">เพศ</th>
                              <th data-field="operate" data-formatter="operateEdit" data-align="center">แก้ไข</th>
                              <th data-field="operate" data-formatter="operateExport" data-align="center">พิมพ์</th>
                              <th data-field="operate" data-formatter="operateDEl" data-align="center">ลบ</th>
                          </tr>
                      </thead>
                  </table>
                </div>
                <div class="box-footer with-border">
                  <div class="box-tools pull-right">
                    <button class="btn btn-success" onclick="gotoPage('<?= $urlback ?>')" style="width:80px;">ย้อนกลับ</button>
                  </div>
                </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">รายชื่อผู้ทดสอบ</h4>
            </div>
            <form id="formData" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data">
              <div class="modal-body" id="formShow">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
                <button type="submit" class="btn btn-primary" id="submitFormData" style="width:100px;">บันทึก</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel2">นำเข้าข้อมูลผู้ทดสอบ</h4>
            </div>
            <form id="formDataUpload" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" >
              <div class="modal-body" id="formShow2">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade bs-example-modal" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Excel template</h4>
            </div>
              <div class="modal-body">
                <table id="tableExcel" class="table table-bordered table-striped">
                <thead>
                  <tr>
                      <th data-align="center">ลำดับ</th>
                      <th data-align="center">รายการ Excel template</th>
                      <th data-align="center">DownLoad</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                      <td style="text-align: center;">1</td>
                      <td>นักเรียน (7 - 18 ปี)</td>
                      <td style="text-align: center;">
                        <a onclick="loadExcel('template/person_template.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                  <tr>
                      <td align="center">2</td>
                      <td>ประชาชนทั่วไป ช่วงอายุ (7 - 18 ปี)</td>
                      <td align="center">
                          <a onclick="loadExcel('template/person_7_18.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                  <tr>
                      <td align="center">3</td>
                      <td>ประชาชนทั่วไป ช่วงอายุ (19 - 59 ปี)</td>
                      <td align="center">
                          <a onclick="loadExcel('template/person_19_59.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                  <tr>
                      <td align="center">4</td>
                      <td>ประชาชนทั่วไป ช่วงอายุ (60 ปีขึ้นไป)</td>
                      <td align="center">
                        <a onclick="loadExcel('template/person_60.xlsx')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a>
                      </td>
                  </tr>
                </tbody>
                </table>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
              </div>
          </div>
        </div>
      </div>
      <!-- <textarea name="content_data" id="content_data" ></textarea> -->

  <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <div id="loading" class="none">
    <img src="images/loading2.gif" alt="">
  </div>
  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>
 <script src="ckeditor/ckeditor.js"></script>
 <script src="js/PFIT010101.js"></script>
</div>
<!-- ./wrapper -->

</body>
</html>
