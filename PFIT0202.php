<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        เกณฑ์การทดสอบ
        <small>PFIT0202</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-list-ul"></i> ข้อมูลหลัก</a></li>
        <li class="active">เกณฑ์การทดสอบ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">รายการเกณฑ์การทดสอบ</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-success" onclick="upload()" style="width:80px;">นำเข้า</button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table id="table" class="table table-bordered table-striped"
                         data-toggle="table"
                         data-toolbar="#toolbar"
                         data-url="ajax/PFIT0201/show.php"
                         data-pagination="true"
                         data-page-size = 20
                         data-page-list= "[20, 50, 100, ALL]"
                         data-search="true"
                         data-flat="true"
                         data-show-refresh="true"
                         >
                      <thead>
                          <tr>
                              <th data-sortable="true" data-formatter="runNo" data-align="center">ลำดับ</th>
                              <th data-sortable="true" data-field="test_code" data-align="center">รหัสฐาน</th>
                              <th data-sortable="true" data-field="test_name" data-align="left">ชื่อฐานการทดสอบ</th>
                              <th data-sortable="true" data-formatter="getObjectStr" data-align="left">องค์ประกอบที่ต้องการวัด</th>
                              <th data-field="operate" data-formatter="operateGoto" data-align="center">ข้อมูล</th>
                          </tr>
                      </thead>
                  </table>
                </div>
          </div>
        </div>
      </div>


      <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel2">นำเข้าข้อมูลเกณฑ์การทดสอบ</h4>
            </div>
            <form id="formDataUpload" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" >
              <div class="modal-body" id="formShow2">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal -->
      <!-- <textarea name="content_data" id="content_data" ></textarea> -->

  <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>

 <script src="js/PFIT0202.js?v=2"></script>
</div>
<!-- ./wrapper -->

</body>
</html>
