<!DOCTYPE html>
<html>

<?php include("inc/head.php"); ?>

<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php");
    $MN = isset($_GET['MN'])?base64_decode($_GET['MN']):"หน้าหลัก";
    $PN = isset($_GET['PN'])?base64_decode($_GET['PN']):"";
    $IC = isset($_GET['IC'])?base64_decode($_GET['IC']):"fa fa-home";

    $month  = date("m");
    $year   = date("Y");
    $year2  = date('Y', strtotime('-1 year'));
    $year3  = date('Y', strtotime('-2 year'));
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        รายงานสรุปผลการทดสอบสมรรถภาพแต่ละฐาน
        <small><?= $PN ?></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-clipboard"></i> รายงาน</a></li>
        <li class="active">รายงานสรุปผลการทดสอบสมรรถภาพแต่ละฐาน</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">

      <!-- Main row -->
      <div class="col-md-12">
        <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title">ค้นหารายชื่อกิจกรรม</h3>
          </div>
          <div class="box-body">
            <div class="row">
              <div class="col-md-6" >
                <div class="form-group">
                  <label class="col-md-2 control-label text-right" style="line-height: 34px;">ปี</label>
                  <div class="col-md-4">
                    <select name="type_test" class="form-control" id="s_year"  <?=$disabled ?> required>
                      <option value="<?= $year ?>"><?= $year ?></option>
                      <option value="<?= $year2 ?>"><?= $year2 ?></option>
                      <option value="<?= $year3 ?>"><?= $year3 ?></option>
                    </select>
                  </div>
                  <label class="col-md-2 control-label text-right" style="line-height: 34px;" >เดือน</label>
                  <div class="col-md-4">
                    <select name="type_test" class="form-control" id="s_month" <?=$disabled ?> required>
                      <option value="01" <?= ($month == '01' ? 'selected="selected"':'') ?>>มกราคม</option>
                      <option value="02" <?= ($month == '02' ? 'selected="selected"':'') ?>>กุมภาพันธ์</option>
                      <option value="03" <?= ($month == '03' ? 'selected="selected"':'') ?>>มีนาคม</option>
                      <option value="04" <?= ($month == '04' ? 'selected="selected"':'') ?>>เมษายน</option>
                      <option value="05" <?= ($month == '05' ? 'selected="selected"':'') ?>>พฤษภาคม</option>
                      <option value="06" <?= ($month == '06' ? 'selected="selected"':'') ?>>มิถุนายน</option>
                      <option value="07" <?= ($month == '07' ? 'selected="selected"':'') ?>>กรกฎาคม</option>
                      <option value="08" <?= ($month == '08' ? 'selected="selected"':'') ?>>สิงหาคม</option>
                      <option value="09" <?= ($month == '09' ? 'selected="selected"':'') ?>>กันยายน</option>
                      <option value="10" <?= ($month == '10' ? 'selected="selected"':'') ?>>ตุลาคม</option>
                      <option value="11" <?= ($month == '11' ? 'selected="selected"':'') ?>>พฤศจิกายน</option>
                      <option value="12" <?= ($month == '12' ? 'selected="selected"':'') ?>>ธันวาคม</option>
                    </select>
                  </div>
                </div>
              </div>
              <button class="btn btn-primary" onclick="searchProject()" style="width:80px;">ค้นหา</button>
            </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div id="show-form"></div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

<?php include("inc/foot.php"); ?>

</div>
<!-- ./wrapper -->

<?php include("inc/footer.php"); ?>

<script src="js/report03.js"></script>


</body>
</html>
