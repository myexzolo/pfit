<!DOCTYPE html>
<html>

<?php
include('inc/utils.php');
include("inc/head.php");
?>
<style>
  th {
    text-align: center !important;
  }
</style>

<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php");
      $code = isset($_POST['code'])?$_POST['code']:"";
  ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        กิจกรรม
        <small>PFIT0100</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-soccer-ball-o"></i>กิจกรรม</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <div class="col-md-12">
          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">รายชื่อกิจกรรม</h3>
                  <div class="box-tools pull-right">
                    <button class="btn btn-primary" onclick="searchData('','','')" style="width:80px;">เพิ่ม</button>
                    <button class="btn btn-success" onclick="upload()" style="width:80px;">นำเข้า</button>
                  </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <input type="hidden" id="connectInternet" value="<?php echo is_connected(); ?>">
                  <input type="hidden" id="serverName" value="<?php echo getServerName(); ?>">
                  <table id="table" class="table table-bordered table-striped"
                         data-toggle="table"
                         data-toolbar="#toolbar"
                         data-url="ajax/PFIT0100/show.php?code=<?= $code ?>"
                         data-pagination="true"
                         data-page-size = 20
                         data-page-list= "[20, 50, 100, ALL]"
                         data-search="true"
                         data-flat="true"
                         data-show-refresh="true"
                         >
                      <thead>
                          <tr>
                              <th data-sortable="true" data-formatter="runNo" data-align="center" class="number">ลำดับ</th>
                              <th data-sortable="true" data-field="project_code" data-align="left" class="number">รหัส</th>
                              <th data-sortable="true" data-field="project_name" data-align="left">ชื่อกิจกรรม</th>
                              <th data-sortable="true" data-formatter="getDateProject" data-align="center" class="number">ระยะเวลา</th>
                              <th data-sortable="true" data-formatter="getStatus" data-align="center">สถานะ</th>
                              <?php
                              if($_SESSION['TYPE_CONN'] != '1')
                              {
                              ?>
                                <th data-field="operate" data-formatter="operateSync" data-align="center">Sync ข้อมูล</th>
                              <?php
                              }
                              ?>
                              <th data-field="operate" data-formatter="operateExport" data-align="center">ส่งออก</th>
                              <th data-field="operate" data-formatter="operateOpen" data-align="center">ดู</th>
                              <th data-field="operate" data-formatter="operateEdit" data-align="center">แก้ไข</th>
                              <th data-field="operate" data-formatter="operateDelete" data-align="center">ลบ</th>
                          </tr>
                      </thead>
                  </table>
                </div>
          </div>
        </div>
      </div>

      <!-- <textarea name="content_data" id="content_data" ></textarea> -->

  <!-- /.row -->
  </section>
    <!-- /.content -->
  </div>
  <!-- Modal -->
  <div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel">รายชื่อกิจกรรม</h4>
        </div>
        <form id="formData" data-smk-icon="glyphicon-remove-sign" >
          <div class="modal-body" id="formShow">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px;">ปิด</button>
            <button type="submit" class="btn btn-primary" style="width:100px;">บันทึก</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade bs-example-modal-lg" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel2">นำเข้าข้อมูลกิจกรรม</h4>
        </div>
        <form id="formDataUpload" data-smk-icon="glyphicon-remove-sign" novalidate enctype="multipart/form-data" >
          <div class="modal-body" id="formShow2">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="closeModel()" style="width:100px;">ปิด</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div id="loading" class="loading none">
    <img src="images/loading2.gif" alt="">
  </div>
  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>
 <script src="ckeditor/ckeditor.js"></script>
 <script src="js/PFIT0100.js"></script>
</div>
<!-- ./wrapper -->

</body>
</html>
