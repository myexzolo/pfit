<?php

include 'mpdf/mpdf.php';
ob_start();

$person_number = $_POST['person_number'];
$projectCode = $_POST['project_code'];

$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'qrcode/temp'.DIRECTORY_SEPARATOR;
//html PNG location prefix
$PNG_WEB_DIR = 'qrcode/temp/';
include "qrcode/qrlib.php";

if (!file_exists($PNG_TEMP_DIR)){
  mkdir($PNG_TEMP_DIR);
}

$filename = $PNG_TEMP_DIR.$person_number.'.png';
$CorrectionLevel = 'L';

$textQr = "$person_number";
$matrixPointSize = 8;
QRcode::png($textQr, $filename, $CorrectionLevel, $matrixPointSize, 2);


?>
<!DOCTYPE html>
<html>

<body>


<h3 class="text-center"><strong>QR CODE</strong></h3>
<table style="width:100%">
  <tr>
    <td class="text-center">
      <?php echo '<img src="'.$PNG_WEB_DIR.basename($filename).'" />';  ?>
    </td>
  </tr>
</table>

</body>
</html>

<?php
    $html = ob_get_contents();
    ob_end_clean();

    $mpdf=new mPDF('utf-8');
    $mpdf->autoScriptToLang = false;
    $mpdf->margin_header = 2;
    // $mpdf->SetHeader('รายงานโดย codingthailand | รายงานลูกค้าทั้งหมด | ออกรายงานเมื่อ: '.date('d/m/Y H:i:s'));
    $mpdf->margin_footer = 9;
    // $mpdf->SetFooter('หน้าที่ {PAGENO}');
    // Define a Landscape page size/format by name
    //$mpdf=new mPDF('utf-8', 'A4-L');
    // Define a page size/format by array - page will be 190mm wide x 236mm height
    //$mpdf=new mPDF('utf-8', array(190,236));
    $stylesheet = file_get_contents('bootstrap/css/printpdf.css');
    //$mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($stylesheet,1);
    $mpdf->WriteHTML($html,2);
    //$mpdf->Output();
    $mpdf->Output(time(),'I');


    exit;
?>
