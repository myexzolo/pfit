<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
</head>
<style>

  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:16pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:16pt;
    font-weight: bold;
    padding: 5px;
  }

  .vertical-centers{
    vertical-align : middle !important;
  }
  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16px;

  }
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
      html, body {
        width: 195mm;
        height: auto;
        margin-left: 10px;
      }
      .break{
        page-break-after: always;
      }
  }
</style>
<html>
<body>
  <?php
    $date = '2018-10';
  ?>

  <?php
    echo $sql = "SELECT DISTINCT(pt.test_code),ts.test_name,ts.test_opjective FROM pfit_t_project p , pfit_t_project_test pt, pfit_t_test ts
            WHERE p.project_code = pt.project_code AND ts.test_code = pt.test_code AND p.end_date LIKE '$date%' ORDER BY pt.test_code ASC";
    $query = DbQuery($sql,null);
    $row  = json_decode($query, true);
    $num  = $row['dataCount'];
    $data = $row['data'];

    foreach ($data as $key => $value) {
      $test_name = $value['test_name'];
      $test_code = $value['test_code'];
      $test_opjective = getTestOpjective($value['test_opjective']);

      $sqlp = "SELECT DISTINCT(p.date_of_birth),pt.end_date,p.person_number FROM pfit_t_person p, pfit_t_project pt
               WHERE p.project_code = pt.project_code AND pt.end_date LIKE '$date%'";
      $queryp = DbQuery($sqlp,null);
      $rowp  = json_decode($queryp, true);
      $nump  = $rowp['dataCount'];
      $datap = $rowp['data'];

      // age
      $arr_age = array();
      foreach ($datap as $valuep) {
        $person_number = $valuep['person_number'];
        $date_of_birth = $valuep['date_of_birth'];
        $end_date = $valuep['end_date'];
        $age = yearBirth2($date_of_birth,$end_date);
          if(!array_key_exists($age,$arr_age)){
            $arr_age[$age] = $age;
          }
      }
      $count_age = sizeof($arr_age);
      $genders = array('ชาย','หญิง','รวม');
      $sqlcat = "SELECT pcd.category_criteria_detail_name FROM pfit_t_test pt, pfit_t_cat_criteria_detail pcd
                 WHERE pt.category_criteria_code = pcd.category_criteria_code AND pt.test_code = '$test_code'";
      $querycat = DbQuery($sqlcat,null);
      $rowcat   = json_decode($querycat, true);
      $numcat   = $rowcat['dataCount'];
      $datacat  = $rowcat['data'];

  ?>
  <table class="table table-bordered">
    <thead>
      <tr class="text-center">
        <td class="vertical-centers" rowspan="2">สมรรถภาพทางร่างกาย</td>
        <td class="vertical-centers" rowspan="2">test</td>
        <td class="vertical-centers" rowspan="2">ช่วงอายุ</td>
        <?php foreach ($genders as $value_gen) {?>
        <td colspan="<?=$numcat ?>"><?=$value_gen ?></td>
        <?php } ?>
      </tr>
      <tr class="text-center">
        <?php
          foreach ($genders as $value_gen) {
            foreach ($datacat as $valuecat) {
              $category_criteria_detail_name = $valuecat['category_criteria_detail_name'];
        ?>
        <td><?=$category_criteria_detail_name ?></td>
        <?php
          }
            }
        ?>
      </tr>
    </thead>
    <tbody>
      <?php
        $count = 1;
        foreach ($arr_age as $value_age) {

      ?>
      <tr class="text-center">
        <?php if($count == 1){ ?>
        <td class="vertical-centers" rowspan="<?=$count_age ?>"><?=$test_opjective; ?></td>
        <td class="vertical-centers" rowspan="<?=$count_age ?>"><?=$test_name; ?></td>
        <?php } ?>
        <td><?=$value_age.' ปี' ?></td>


        <?php
          foreach ($genders as $value_gen) {
            foreach ($datacat as $valuecat) {
              $category_criteria_detail_name = $valuecat['category_criteria_detail_name'];
        ?>
        <td><?=123 ?></td>
        <?php
          }
            }
        ?>
      </tr>
      <?php $count++; } ?>
    </tbody>
  </table>

  <br><br>

  <?php } ?>

</body>
</html>
<?php
include("inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      // window.print();
      // window.close();
    }, 100);
  });
</script>
