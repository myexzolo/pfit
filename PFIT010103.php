<!DOCTYPE html>
<html>

<?php
include("inc/head.php");
include('inc/utils.php');

  $project_code   = isset($_POST['project_code'])?$_POST['project_code']:"";
  $project_name   = isset($_POST['project_name'])?$_POST['project_name']:"";
  $m   = isset($_POST['m'])?$_POST['m']:"";
  $y   = isset($_POST['y'])?$_POST['y']:"";

  $urlback = "PFIT0101.php?m=$m&y=$y";
  if($project_code == ""){
    exit("<script>window.location='PFIT0101.php';</script>");
  }
?>
<body class="hold-transition skin-blue sidebar-mini" >
<div class="wrapper" >

<?php include("inc/header.php"); ?>

  <!-- Left side column. contains the logo and sidebar -->
  <?php include("inc/sidebar.php"); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        รายงานผลการทดสอบ
        <small>PFIT010103</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="PFIT0101.php"><i class="fa fa-clipboard"></i>จัดเก็บผลการทดสอบ</a></li>
        <li class="active">รายงานผลการทดสอบ</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

      <div class="row">
        <div class="col-md-12">

          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">ข้อมูลผู้ทดสอบ</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                  <div class="row">
                    <div class="col-md-10 col-md-offset-1">

                      <form class="form-horizontal" id="formSearch" data-smk-icon="glyphicon-remove-sign"  enctype="multipart/form-data">
                        <div class="row">
                          <div class="col-md-12" align="center">
                            <table>
                            <tr>
                              <td style="padding:5px;" align="right">ชื่อผู้ทดสอบ : </td>
                              <td style="padding:5px;" align="left">
                                <input type="text" id="personName" class="form-control" placeholder="ชื่อ" >
                              </td>
                              <td style="padding:5px;width:150px;" align="right">นามสกุลผู้ทดสอบ :</td>
                              <td style="padding:5px;" align="left">
                                <input type="text" id="personLname" class="form-control" placeholder="นามสกุล" >
                              </td>
                            </tr>
                            <tr>
                              <td style="padding:5px;" align="right">เลขประจำตัว : </td>
                              <td style="padding:5px;" align="left">
                                <input type="text" id="personId" class="form-control" placeholder="เลขประจำตัว" >
                              </td>
                              <td style="padding:5px;width:150px;" align="right"></td>
                              <td style="padding:5px;" align="left"></td>
                            </tr>
                            <tr>
                              <td style="padding:5px;padding-top:20px;" colspan="4" align="center">
                                  <button type="submit" id="btnSave" class="btn btn-success btn-flat" style="width:100px">ค้นหา</button>
                                  <button type="reset" class="btn btn-flat" style="width:100px">ล้างค่า</button>
                              </td>
                            </tr>
                          </table>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>

                </div>
          </div>

          <div class="box boxBlack">
                <div class="box-header with-border">
                  <h3 class="box-title">แสดงรายการ</h3>
                  <div class="box-tools pull-right" style="width:550px;" align="right">
                      <label>ประเภทการทดสอบ : </label>
                      <select style="width:100px;">
                        <option>บุคคลทั่วไป</option>
                        <option>นักกีฬา</option>
                      </select>
                      <?php
                        $url = "report_result.php?code=".$_POST['project_code'];
                        $urlExcel = "testExportExcel.php?code=".$_POST['project_code'];
                      ?>
                    <button type="button" class="btn btn-primary btnReport btn-flat" onclick="postURL_blank('<?php echo $url; ?>')" style="width:140px;">ออกรายงานทั้งหมด</button>
                    <button type="button" class="btn btn-success btnReport btn-flat" onclick="postURL('<?php echo $urlExcel; ?>')" style="width:80px;">Excel</button>
                    <input type="hidden" id="projectCode" value="<?= $project_code ?>">
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body" id="formShow"></div>
                <div class="box-footer with-border">
                  <div class="box-tools pull-right">
                    <button class="btn btn-success" onclick="gotoPage('<?= $urlback ?>')" style="width:80px;">ย้อนกลับ</button>
                  </div>
                </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
      <!-- <textarea name="content_data" id="content_data" ></textarea> -->
  <!-- /.row -->
  </section>

  <div id="printableArea">

  </div>
    <!-- /.content -->
  </div>

  <div id="loading" class="none">
    <img src="images/loading2.gif" alt="">
  </div>

  <?php include("inc/foot.php"); ?>
  <!-- /.content-wrapper -->
 <?php include("inc/footer.php"); ?>


 <script src="js/PFIT010103.js"></script>
 <script type="text/javascript">
   $(document).ready(function() {
      formShowPerson();
   });
 </script>
</div>
<!-- ./wrapper -->

</body>

<script type="text/javascript">
  function printDiv(divName){
    $.get("ajax/PFIT010103/print.php")
    .done(function( data ) {
        $("#printableArea").html(data);

        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
        $("#printableArea").html('');
    });


  }
</script>

</html>
