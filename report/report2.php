<?php
session_start();
include('../conf/connect.php');
include('../inc/utils.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:12pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
    text-align: center;
  }

  .bolder{
    font-weight: bolder;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:12pt;
    font-weight: bold;
    padding: 5px;
  }

  .mar{
    margin: 20px 0px;
  }
   .row{
     margin-right: 0px;
     margin-left: 0px;
   }

  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16pt;

  }
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
      html, body {
        width: 297mm;
        height: 210mm;
        margin-left: 10px;
      }
      .break{
        page-break-after: always;
      }
  }
</style>
<html>
  <body>

    <div class="container">


    <h2 style="text-align:center">
      <img src="../images/dep_logo.png" style="height:70px;width:75px;">
      <strong>สมรรถภาพทางกายเชิงสุขภาพ</strong>
    </h2>

      <table align="center" border="1" bordercolor="#ccc" cellpadding="5" cellspacing="0" style="border-collapse:collapse; width:100%">
      	<tbody>
      		<tr>
      			<td class="content" style="text-align:center"><strong>รายการตรวจวัดสุขภาพ</strong></td>
      			<td class="content" style="text-align:center"><strong>ผลการตรวจวัด</strong></td>
      			<td class="content" style="text-align:center"><strong>ค่ามาตราฐาน</strong></td>
      			<td class="content" style="text-align:center"><strong>ปกติ</strong></td>
      			<td class="content" style="text-align:center"><strong>ดี</strong></td>
      			<td class="content" style="text-align:center"><strong>สูง</strong></td>
      			<td class="content" style="text-align:center"><strong>ต่ำ</strong></td>
      		</tr>
      		<tr>
      			<td class="content"><strong>FAT (%)</strong> ไขมันสะสมใต้ผิวหนัง</td>
      			<td class="content">&nbsp;</td>
      			<td class="content" style="text-align:center">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      		</tr>
      		<tr>
      			<td class="content"><strong>TBW (%)</strong> ระดับน้ำในร่างกาย</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">
      			<p style="text-align:center"><strong>ชาย</strong>/ค่าปกติ 50-65%<br />
      			<strong>หญิง</strong>/ค่าปกติ 45-60%</p>
      			</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      		</tr>
      		<tr>
      			<td class="content"><strong>Bone Mass (Kg)</strong> น้ำหนักของกระดูก</td>
      			<td class="content">&nbsp;</td>
      			<td class="content" style="text-align:center">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      		</tr>
      		<tr>
      			<td class="content"><strong>Visceral Fat</strong> ระดับไขมันในช่องท้อง</td>
      			<td class="content">&nbsp;</td>
      			<td class="content" style="text-align:center"><strong>ชาย</strong> ไม่เกินระดับ 9<br />
      			<strong>หญิง</strong> ไม่เกินระดับ 5</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      		</tr>
      		<tr>
      			<td class="content"><strong>BMR</strong> การเผาผลาญพนังงานของร่างกาย</td>
      			<td class="content">&nbsp;</td>
      			<td class="content" style="text-align:center"><strong>ค่าแต่ละบุคคล</strong> (kcal)</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      		</tr>
      		<tr>
      			<td class="content"><strong>Metabolic Age</strong> เปรียบเทียบอายุการเผาผลาญ</td>
      			<td class="content">&nbsp;</td>
      			<td class="content" style="text-align:center"><strong>ควรต่ำกว่าอายุจริง</strong> (ปี)</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      			<td class="content">&nbsp;</td>
      		</tr>
      	</tbody>
      </table>
      <p><strong>Fat Mass</strong> คือ ปริมาณไขมันที่สะสมในร่างกายมีหน่วยวัดเป็นกิโลกรัม และวิเคราะห์เป็นค่าร้อยละ</p>
      <p><strong>TBW (Total Body Water)</strong> คือ เปอร์เซ็นต์ของน้ำในร่างกาย เป็นสัดส่วนของน้ำในร่างกายเมื่อเทียบกับน้ำหนักตัว</p>
      <p><strong>Bone Mass</strong> คือ นำหนักของกระดูกทั้งหมดในร่างกาย มีหน่วยวัดเป็นกิโลกรัม</p>
      <p><strong>Visceral Fat Rating</strong> คือ ระดับไขมันที่เกาะเลือดหรือสะสมคามอวัยวะภายในช่องท้อง</p>
      <p><strong>BMR (Basal Metabolic Rate)</strong> คือ อัตราการเผาผลาญพลังงานพื้นฐานเป็นค่าการใช้พลังงานของร่างกาย ของแต่ละคน</p>
      <ol>
      	<li>สำหรับผู้ชาย BMR = 66 + (13.7 x น้ำหนักตัวเป็น กก. ) + ( 5 x ส่วนสูงเป็น ซม. ) - ( 6.8 x อายุ )</li>
      	<li>สำหรับผู้หญิง&nbsp;BMR = 665&nbsp;+ (9.6 x น้ำหนักตัวเป็น กก. ) + ( 1.8&nbsp;x ส่วนสูงเป็น ซม. ) - ( 4.7&nbsp;x อายุ )</li>
      </ol>
      <p><strong>Metabolie Age</strong> คือ อายุเปรียบเทียบกับอัตราเผาผลาญพลังงาน เป็นค่าที่แสดงอัตราเผาผลาญพลังงานในปัจจุบันและมวลกล้ามเนื้อที่วัดได้เทียบกับอายุ</p>
      <p><strong>Muscle Mass</strong> หมายถึง น้ำหนักของกล้ามเนื้อในร่างกาย หน่วยวัดเป็นกิโลกรัม</p>
      <p><strong>BMI (Body Mass Index)</strong> หมายถึงดัชนีมวลกาย ซึ่งคำนวณจาก น้ำหนักตัว(กิโลกรัม) / ส่วนสูง<sup>2</sup>&nbsp;(เมตร)</p>
      <p><strong>Ideal Body Weight</strong> หมายถึง น้ำหนักตัวที่เหมาะสม เป็นค่าที่ได้จากการคำนวณระดับองค์ประกอบของร่างกาย</p>
      <p>&nbsp;</p>
      <p><em><strong>กลุ่มพัฒนาสมรรถภาพทางกาย&nbsp; สำนักวิทยาศาสตร์การกีฬา&nbsp; กรมพลศึกษา&nbsp; กระทรวงการท่องเที่ยวและกีฬา</strong></em></p>

    </div>


  </body>
</html>
<?php
include("../inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      // window.print();
      // window.close();
    }, 100);
  });
</script>
