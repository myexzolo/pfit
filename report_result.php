<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
?>
<!DOCTYPE html>
<head>
    <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" type="image/png" href="images/fav.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
</head>
<style>
  .tbroder {
     padding:3px 5px 3px 5px;
     border:1px solid #333;
  }
  .info{
    font-size:16pt;
    text-align:left;
  }
  .content{
    padding: 5px;
    font-size:16pt;
  }

  .thStyle {
    text-align: center;
    background-color:#e7e6e6;
    font-size:16pt;
    font-weight: bold;
    padding: 5px;
  }
  @font-face {
    font-family: "THSarabun";
    src: url("fonts/THSarabunNew/THSarabunNew.ttf") ;
  }

  body, html{
      font-family: "THSarabun" !important;
      font-size:16px;

  }

  td{
    vertical-align: middle !important;
  }
  @page {
    size: A4;
    margin: 0;
  }
  @media print {
      html, body {
        margin-right: 10px;
        margin-left: 10px;
      }
      .break{
        page-break-after: always;
      }
  }
</style>
<html>
<body>
  <?php
      $projectCode = $_POST['code'];


      $sql    = "SELECT * FROM pfit_t_project WHERE project_code = '$projectCode' ";
      $query  = DbQuery($sql,null);
      $json  = json_decode($query, true);
      $row   = $json['data'];
      $location = $row[0]['location'];

      $str = "WHERE pfit_t_project_test.project_code = '$projectCode' ";
      if(isset($_POST['id'])){
        $str .= "AND pfit_t_person.person_number = '".$_POST['id']."'";
      }

      $sqlp = "SELECT pfit_t_person.person_number,pfit_t_person.person_name,pfit_t_person.person_lname,
               pfit_t_person.person_gender,".getQueryDate('date_of_birth').",pfit_t_person.wiegth,pfit_t_person.height
               FROM pfit_t_project_test
               INNER JOIN pfit_t_person ON pfit_t_project_test.project_code = pfit_t_person.project_code
               $str
               GROUP BY pfit_t_person.person_number,pfit_t_person.person_name,pfit_t_person.person_lname,
               pfit_t_person.person_gender,pfit_t_person.date_of_birth,pfit_t_person.wiegth,pfit_t_person.height";

      //echo $sqlp;
      $queryp = DbQuery($sqlp,null);
      $rowp  = json_decode($queryp, true);
      foreach ($rowp['data'] as $key => $value) {
        $person_numberp = $value['person_number'];
        $person_name    = $value['person_name'];
        $person_lname   = $value['person_lname'];
        $gender         = $value['person_gender'];
        $person_gender  = $value['person_gender']=='M'?"ชาย":"หญิง";
        $date_of_birth  = $value['date_of_birth'];
        $wiegth         = $value['wiegth'];
        $height         = $value['height'];
        $birth = yearBirth($date_of_birth);
        // $num = 0;
        $date_create  = getQueryDate('date_create');
        $sql_date = "SELECT $date_create FROM pfit_t_result WHERE person_number = '$person_numberp'";
        $query_date = DbQuery($sql_date,null);
        $row_date  = json_decode($query_date, true);
        $num = $row_date['dataCount'];
        $date_create = '...../...../..........';
        if($num > 0){
          $date_create = convDatetoThai($row_date['data'][0]['date_create']);
        }

        $BMI = array("","");
        if($wiegth != "" && $height != ""){
          //$bmi = number_format($wiegth/(pow($height/100,2)),2);


         $sqlb = "SELECT * FROM pfit_t_result
                  LEFT JOIN pfit_t_test_criteria ON pfit_t_result.test_criteria_code = pfit_t_test_criteria.test_criteria_code
                  LEFT JOIN pfit_t_cat_criteria_detail ON pfit_t_test_criteria.category_criteria_detail_code = pfit_t_cat_criteria_detail.category_criteria_detail_code
                  WHERE pfit_t_result.project_code = '$projectCode'
                  AND pfit_t_result.person_number = '$person_numberp'
                  AND pfit_t_result.test_code = 'BMI'";

         //echo $sqlb;
         $queryb = DbQuery($sqlb,null);
         $rowb  = json_decode($queryb, true);

         $BMI[0] = isset($rowb['data'][0]['result_cal']) && !empty($rowb['data'][0]['result_cal'])?$rowb['data'][0]['result_cal']:"-";
         $BMI[1] = isset($rowb['data'][0]['category_criteria_detail_name']) && !empty($rowb['data'][0]['category_criteria_detail_name'])?$rowb['data'][0]['category_criteria_detail_name']:"-";

        }
  ?>
  <br>
  <table style="width: 100%;" border="0" >
    <tr>
      <td align="left" style="width:200px"><img src="images/dep_logo.png" style="height:100px;width:100px;"></td>
      <td colspan="3" style="font-size:20pt;font-weight:500;padding-top:20px;" align="center"><b>ผลการทดสอบสมรรถภาพทางกาย</b></td>
      <td align="right" style="width:200px;font-size:14pt">สถานที่ : <?= $location ?><br> วันที่ : <?= $date_create; ?></td>
    </tr>


    <tr style="padding-top:20px;">
      <td colspan="5" style="padding-top:20px;" class="info">
        <b>ชื่อ :</b> <?php echo $person_name,' ',$person_lname; ?> &emsp;&emsp;
        <b>เพศ :</b> <?php echo $person_gender; ?> &emsp;&emsp;
        <b>อายุ :</b> <?= $birth; ?> ปี
      </td>
    </tr>
    <tr>
      <td colspan="5" style="width:250px"class="info">
        <b>น้ำหนัก :</b> <?php echo $wiegth; ?> กิโลกรัม &emsp;
        <b>ส่วนสูง :</b> <?php echo $height; ?> เซนติเมตร &emsp;
        <b>ดัชนีมวลกาย :</b> <?php echo  $BMI[0] ?> &emsp;
        <b>อยู่ในเกณฑ์ :</b> <?php echo $BMI[1]; ?>
      </td>
    </tr>

  </table>
  <table border="1" cellspacing="0" style="border-collapse:collapse; border:solid #333 1px; width:100%" >
      <thead>
        <tr>
    		<td colspan="6" style="height:30px;border-top-style:hidden;border-right-style:hidden;border-left-style:hidden;"></td>
      </tr>
  		<tr>
  			<td class="thStyle" style="width:120px;">รายการทดสอบ</td>
  			<td colspan="2" class="thStyle">ผลการทดสอบ</td>
        <td class="thStyle" style="width:105px;">เกณฑ์มาตรฐาน</td>
  			<td class="thStyle" style="width:80px;">ผลการประเมิน</td>
  			<td class="thStyle">คำแนะนำ</td>
  		</tr>
    </thead>
    <tbody>
        <?php
        // echo $sqlp = "SELECT * FROM pfit_t_result
        //          INNER JOIN pfit_t_test ON pfit_t_result.test_code = pfit_t_test.test_code
        //          INNER JOIN pfit_t_project_test ON pfit_t_result.test_code = pfit_t_project_test.test_code and pfit_t_result.project_code = pfit_t_project_test.project_code
        //          LEFT JOIN pfit_t_test_criteria ON pfit_t_result.test_criteria_code = pfit_t_test_criteria.test_criteria_code
        //          LEFT JOIN pfit_t_cat_criteria_detail ON pfit_t_test_criteria.category_criteria_detail_code = pfit_t_cat_criteria_detail.category_criteria_detail_code
        //          WHERE pfit_t_result.project_code = '$projectCode' AND pfit_t_result.person_number = '$person_numberp'
        //          ORDER BY pfit_t_project_test.test_seq ASC";
          $sqlp = "SELECT * FROM pfit_t_person
                   INNER JOIN pfit_t_project_test ON pfit_t_person.project_code = pfit_t_project_test.project_code
                   INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
                   WHERE pfit_t_person.project_code = '$projectCode'
                   AND pfit_t_test.test_age_min <= $birth and pfit_t_test.test_age_max >= $birth
                   AND pfit_t_person.person_number = '$person_numberp'
                   order by pfit_t_project_test.test_seq ASC";
          //echo     $sqlp;
          $queryp = DbQuery($sqlp,null);
          $rowp  = json_decode($queryp, true);
          $num = $rowp['dataCount'];

          if($num>0){

            foreach ($rowp['data'] as $key => $value) {

              $test_code = $value['test_code'];
              $test_name = $value['test_name'];
              if($test_code == "BMI"){
                continue;
              }
              $sqlp = "SELECT * FROM pfit_t_result
                       INNER JOIN pfit_t_test ON pfit_t_result.test_code = pfit_t_test.test_code
                       LEFT JOIN pfit_t_test_criteria ON pfit_t_result.test_criteria_code = pfit_t_test_criteria.test_criteria_code
                       LEFT JOIN pfit_t_cat_criteria_detail ON pfit_t_test_criteria.category_criteria_detail_code = pfit_t_cat_criteria_detail.category_criteria_detail_code
                       WHERE pfit_t_result.project_code = '$projectCode'
                       AND pfit_t_result.person_number = '$person_numberp'
                       AND pfit_t_result.test_code = '$test_code' AND pfit_t_test.test_type != 1";
              //echo     $sqlp;
              $queryp = DbQuery($sqlp,null);
              $rowp  = json_decode($queryp, true);
              $num = $rowp['dataCount'];
              $result_cal = '0';
              $test_suggest = '-';
              $category_criteria_detail_name = '-';
              if($num>0){
                $test_criteria_code = $rowp['data'][0]['test_criteria_code'];
                $test_unit = $rowp['data'][0]['test_unit'];
                $result_cal = $rowp['data'][0]['result_cal']!=''?$rowp['data'][0]['result_cal']:"-";
                $resultTest = $rowp['data'][0]['result']!=''?$rowp['data'][0]['result']:"";

                $test_suggest = $rowp['data'][0]['test_suggest']!=''?$rowp['data'][0]['test_suggest']:"-";
                $category_criteria_detail_name = $rowp['data'][0]['category_criteria_detail_name']!=''?$rowp['data'][0]['category_criteria_detail_name']:"-";
                // echo $birth;
                // $test_code = $rowp['data'][0]['test_c'];


              }

              if($test_code == "ABP"){
                $criteriaS = "120/80";
                if($resultTest != ""){
                  $result_cal = str_replace("|", "/", $resultTest);
                }
              }else{
                $sql_m = "SELECT * FROM pfit_t_test_criteria WHERE test_code = '$test_code' AND age = '$birth' AND gender = '$gender' order by category_criteria_detail_code";
                $query_m = DbQuery($sql_m,null);
                $row_m  = json_decode($query_m, true);
                $num_m = $row_m['dataCount'];
                $postion =  ceil($num_m/2)-1;

                $min = "";
                $max = "";
                if(isset($row_m['data'][$postion]['min'])){
                  $min = $row_m['data'][$postion]['min']!=''?$row_m['data'][$postion]['min']:"";
                }
                if(isset($row_m['data'][$postion]['max'])){
                  $max = $row_m['data'][$postion]['max']!=''?$row_m['data'][$postion]['max']:"";
                }

                if(substr($min,0,1) == "."){
                  $min = "0".$min;
                }

                if(substr($max,0,1) == "."){
                  $max = "0".$max;
                }
                $criteriaS = $min.' - '.$max;
              }


          ?>
          <tr>
      			<td class="content"><?=$test_name ?></td>
            <td class="content" align="right" style="width:50px;border-right:0px;"><?= $result_cal; ?></td>
            <td class="content" align="left" style="width:70px;border-left:0px;"><?= $value['test_unit'] ?></td>
      			<td class="content" align="center"><?= $criteriaS; ?></td>
      			<td class="content" align="center"><?= strip_tags($category_criteria_detail_name); ?></td>
      			<td class="content" style="text-align: justify;">
      			<?= strip_tags($test_suggest); ?>
      			</td>
      		</tr>
          <?php }
          }else{
            // $sqlp = "SELECT * FROM pfit_t_result
            //          INNER JOIN pfit_t_test ON pfit_t_result.test_code = pfit_t_test.test_code
            //          LEFT JOIN pfit_t_test_criteria ON pfit_t_result.test_criteria_code = pfit_t_test_criteria.test_criteria_code
            //          LEFT JOIN pfit_t_cat_criteria_detail ON pfit_t_test_criteria.category_criteria_detail_code = pfit_t_cat_criteria_detail.category_criteria_detail_code
            //          WHERE pfit_t_result.project_code = '61070001'
            //          GROUP BY pfit_t_test.test_code";
            // $queryp = DbQuery($sqlp,null);
            // $rowp  = json_decode($queryp, true);
            //
            // if($rowp['dataCount'] > 0){
            //   foreach ($rowp['data'] as $key => $value) {
            //   $test_unit = $value['test_unit'];
            //   $result = "-";
            //   $min = "";
            //   $max = "";
            //   $total = $min.' - '.$max;
            //   $test_suggest = "-";
            //   $category_criteria_detail_name = "-";

              for($i=0; $i<5; $i++){
              ?>
              <tr>
                <td class="content" align="center"> &nbsp;</td>
                <td class="content" align="center">	&nbsp;</td>
                <td class="content" align="center">	&nbsp;</td>
                <td class="content" align="center">	&nbsp;</td>
                <td class="content" align="center">	&nbsp;</td>
                <td class="content" align="center">	&nbsp;</td>
              </tr>

              <?php
                }
                } ?>

  	</tbody>
  </table>
  <div align="center" style="font-size:14pt;padding-top:10px;">
    * หมายเหตุ ค่าเกณฑ์สมรรถภาพทางกายสำหรับแต่ละช่วงวัยดูได้ที่ http://sportscience.dpe.go.th/
  </div>
  <div align="center" style="font-size:14pt;font-weight: bold;">
    กรมพลศึกษา กระทรวงการท่องเที่ยวและกีฬา โทร. 0 2214 2577
  </div>
  <div class="break"></div>
  <?php } ?>

</body>
</html>
<?php
include("inc/footer.php");
?>

<script type="text/javascript">
  $(document).ready(function(){
    setTimeout(function(){
      window.print();
      window.close();
    }, 100);
  });
</script>
