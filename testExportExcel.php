<?php
session_start();
include('conf/connect.php');
include('inc/utils.php');
$projectCode = $_POST['code'];
//$name = $projectCode.'_'.date('Ymd');

error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once 'Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("pfit")
							 ->setLastModifiedBy("pfit")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

$r 	 = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG");

$sql = "SELECT project_code ,TO_CHAR( end_date, 'YYYY-MM-DD') as end_date  FROM pfit_t_project WHERE project_code = '$projectCode'";
$query = DbQuery($sql,null);
$row  = json_decode($query, true);

$num   = $row['dataCount'];
$nameSTmp = "";
if($num>0){
    $tab = 0;
    $setHead  = true;
    $number   = 1;
    $rperson  = 5;
    foreach ($row['data'] as $value) {
      $project_codep = $value['project_code'];
			$end_date 		 = $value['end_date'];
      $sqlp = "SELECT project_code, person_number, person_name, person_lname , person_gender , TO_CHAR( date_of_birth, 'YYYY-MM-DD') as date_of_birth, wiegth, height,stage, grade
			FROM pfit_t_person WHERE project_code = '$project_codep' order by stage, grade, date_create";
      $queryp = DbQuery($sqlp,null);
      $rowp  = json_decode($queryp, true);
      $stage = '';
      //print_r($rowp);
      $nump   = $rowp['dataCount'];
      if($nump>0){

      if($rowp['data'][0]['stage'] == 1){
        $stage = 'นักเรียนชั้นประถมศึกษาปีที่';
      }elseif($rowp['data'][0]['stage'] == 2){
        $stage = 'นักเรียนชั้นมันธยมศึกษาปีที่';
      }
      $grade = $rowp['data'][0]['grade'];

      $nameS =  $stage.' '.$grade;


      $sqlpp = "SELECT * FROM pfit_t_project_test
                INNER JOIN pfit_t_test ON pfit_t_project_test.test_code = pfit_t_test.test_code
                WHERE pfit_t_project_test.project_code = '$project_codep' ORDER BY pfit_t_project_test.test_seq ASC";
      $querypp = DbQuery($sqlpp,null);
      $rowpp  = json_decode($querypp, true);
      $numc   = $rowpp['dataCount'];
      $total_max_value = 0;

      $re   =  ($numc + 8);
      $name = "ผลการทดสอบสมรรถภาพทางกาย";
      if($nameSTmp  != "" && $nameSTmp != $nameS){
        $tab++;
        $nameSTmp = $nameS;
        $setHead = true;
      }else{
        if($tab != 0){
          $setHead = false;
        }
      }

      if($setHead){

        if($tab == 0){
          $nameSTmp = $nameS;
      		$objPHPExcel->getActiveSheet()->setTitle($nameS);
      	}else{
      		$objPHPExcel->createSheet()->setTitle($nameS);
      	}

        $objPHPExcel->setActiveSheetIndex($tab);
        $objPHPExcel->getActiveSheet($tab)->mergeCells('A1:'.$r[$re].'1')->setCellValue('A1',$name)->getStyle('A1:'.$r[$re].'2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet($tab)->mergeCells('A2:'.$r[$re].'2')->setCellValue('A2',$nameS);

        $objPHPExcel->getActiveSheet($tab)->mergeCells('A3:A4')->setCellValue('A3','เลขที่')->getStyle('A3:E3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $objPHPExcel->getActiveSheet($tab)->mergeCells('B3:B4')->setCellValue('B3','เลขประจำตัว');
        $objPHPExcel->getActiveSheet($tab)->mergeCells('C3:C4')->setCellValue('C3','เพศ');
        $objPHPExcel->getActiveSheet($tab)->mergeCells('D3:D4')->setCellValue('D3','ชื่อ');
        $objPHPExcel->getActiveSheet($tab)->mergeCells('E3:E4')->setCellValue('E3','นามสกุล');
				$objPHPExcel->getActiveSheet($tab)->mergeCells('F3:F4')->setCellValue('F3','วันเดือนปีเกิด');
				$objPHPExcel->getActiveSheet($tab)->mergeCells('G3:G4')->setCellValue('G3','อายุ');
        $objPHPExcel->getActiveSheet($tab)->setCellValue('H3', 'น้ำหนัก')->getStyle('H3:'.$r[$re].'4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $objPHPExcel->getActiveSheet($tab)->setCellValue('H4', '(กก.)');
        $objPHPExcel->getActiveSheet($tab)->setCellValue('I3', 'ส่วนสูง');
        $objPHPExcel->getActiveSheet($tab)->setCellValue('I4', '(ซม.)');
        $objPHPExcel->getActiveSheet($tab)->setCellValue('J3', 'ดัชนีมวลกาย');
        $objPHPExcel->getActiveSheet($tab)->setCellValue('J4', '(BMI)');

        $colTest = 10;
        foreach ($rowpp['data'] as $valuepp) {
          $category_criteria_code = $valuepp['category_criteria_code'];

          $sql_value = "SELECT MAX(category_criteria_detail_value) AS max_value FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code'";
          $query_value = DbQuery($sql_value,null);
          $row_value  = json_decode($query_value, true);
          $num_value  = $row_value['dataCount'];
          $data_value = $row_value['data'];

          $total_max_value += $data_value[0]['max_value'];

          $test_unit = $valuepp['test_unit'];
          $test_name = $valuepp['test_name'];

          $objPHPExcel->getActiveSheet($tab)->setCellValue($r[$colTest].'3', $test_name);
          $objPHPExcel->getActiveSheet($tab)->setCellValue($r[$colTest].'4', '('.$test_unit.')');

          $colTest++;
        }
				$objPHPExcel->getActiveSheet($tab)->setCellValue($r[$colTest].'3', 'คะแนน');
				$objPHPExcel->getActiveSheet($tab)->setCellValue($r[$colTest].'4', '(เต็ม '.$total_max_value.')');
      }

			foreach ($rowp['data'] as $valuep) {
				$person_number 	= $valuep['person_number'];
	      $person_name 		= $valuep['person_name'];
	      $person_lname 	= $valuep['person_lname'];
	      $date_of_birth 	= $valuep['date_of_birth'];
	      $wiegth 				= $valuep['wiegth'];
	      $height 				= $valuep['height'];
	      $person_gender 	= $valuep['person_gender']=='M'?'ชาย':'หญิง';
	      $bmi 		= bmi($wiegth,$height)[0];
				$bmiVal = bmi($wiegth,$height)[1];

	      $objPHPExcel->getActiveSheet($tab)->setCellValue('A'.$rperson, $number);
	      $objPHPExcel->getActiveSheet($tab)->setCellValueExplicit('B'.$rperson, $person_number,PHPExcel_Cell_DataType::TYPE_STRING);
	      $objPHPExcel->getActiveSheet($tab)->setCellValue('C'.$rperson, $person_gender);
	      $objPHPExcel->getActiveSheet($tab)->setCellValue('D'.$rperson, $person_name);
	      $objPHPExcel->getActiveSheet($tab)->setCellValue('E'.$rperson, $person_lname);
				$objPHPExcel->getActiveSheet($tab)->setCellValue('F'.$rperson, DateThai($date_of_birth));
				$objPHPExcel->getActiveSheet($tab)->setCellValue('G'.$rperson, yearBirth2($date_of_birth,$end_date));
	      $objPHPExcel->getActiveSheet($tab)->setCellValue('H'.$rperson, $wiegth);
	      $objPHPExcel->getActiveSheet($tab)->setCellValue('I'.$rperson, $height);
	      $objPHPExcel->getActiveSheet($tab)->setCellValue('J'.$rperson, $bmi);

				$total_max_value = 0;
				$colTest = 10;
				foreach ($rowpp['data'] as $valuepp) {
					$test_code = $valuepp['test_code'];
					$sqlres = "SELECT result,category_criteria_detail_name,category_criteria_detail_value FROM pfit_t_result pts, pfit_t_test_criteria ptc , pfit_t_cat_criteria_detail ptccd
										 WHERE ptc.test_criteria_code = pts.test_criteria_code
										 AND ptccd.category_criteria_detail_code = ptc.category_criteria_detail_code
										 AND pts.test_code = '$test_code' AND pts.person_number = '$person_number'";
					$queryres = DbQuery($sqlres,null);
					$rowres  = json_decode($queryres, true);

					$numres   		= $rowres['dataCount'];
					$result 			= '';
					$resultDetail = '';
					if($numres>0){
						$total_max_value += $rowres['data'][0]['category_criteria_detail_value'];
						$resultDetail 		= $rowres['data'][0]['category_criteria_detail_name'];
						$result 					= $rowres['data'][0]['result'];
					}
					$objPHPExcel->getActiveSheet($tab)->setCellValue($r[$colTest].$rperson, $result);

					$objPHPExcel->getActiveSheet($tab)->setCellValue('A'.($rperson + 1), '');
		      $objPHPExcel->getActiveSheet($tab)->setCellValueExplicit('B'.($rperson + 1), '');
		      $objPHPExcel->getActiveSheet($tab)->setCellValue('C'.($rperson + 1), '');
		      $objPHPExcel->getActiveSheet($tab)->setCellValue('D'.($rperson + 1), '');
		      $objPHPExcel->getActiveSheet($tab)->setCellValue('E'.($rperson + 1), '');
		      $objPHPExcel->getActiveSheet($tab)->setCellValue('F'.($rperson + 1), '');
		      $objPHPExcel->getActiveSheet($tab)->setCellValue('G'.($rperson + 1), '');
		      $objPHPExcel->getActiveSheet($tab)->setCellValue('H'.($rperson + 1), $bmiVal)->getStyle('H'.($rperson + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

					$objPHPExcel->getActiveSheet($tab)->setCellValue($r[$colTest].($rperson + 1), $resultDetail)->getStyle($r[$colTest].($rperson + 1))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);


					$colTest++;
	      }
				$objPHPExcel->getActiveSheet($tab)->setCellValue($r[$colTest].$rperson, $total_max_value);

				$rperson = ($rperson + 2);
				$number++;
			}


      foreach(range('A','Z') as $columnID) {
    	    $objPHPExcel->getActiveSheet($tab)->getColumnDimension($columnID)
    	        ->setAutoSize(true);
    	}
   }
 }
}

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>
