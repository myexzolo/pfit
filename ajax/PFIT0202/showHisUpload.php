<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');

$type_file = !empty($_GET['type_file'])?"{$_GET['type_file']}":"pfit_t_test_criteria";

if($_SESSION['TYPE_CONN'] == '1'){
   $date_upload =  getQueryDateV2('date_upload','YYYY-MM-DD HH24:MI:SS');
}else{
   $date_upload = "date_upload";
}


$sql  = "SELECT history_upload_id,file_name,status_upload,total_record,success_record,fail_record,path_file,type_file,project_code,$date_upload
FROM pfit_t_history_upload WHERE project_code = 'PFIT0202' and type_file = '$type_file' order by history_upload_id desc";

$query = DbQuery($sql,null);
$json  = json_decode($query, true);

if($json['dataCount'] == 0){
  $row   = "";
}else{
  $row   = json_encode($json['data']);
}

header('Content-Type: application/json');
exit($row);

?>
