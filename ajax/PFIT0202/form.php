<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$testCode             = $_GET['testCode'];
$testCriteriaCode     = $_GET['testCriteriaCode'];
$categoryCriteriaCode = $_GET['categoryCriteriaCode'];
$statusData           = $_GET['statusData'];
$disabled = '';
$visibility = '';

$test_criteria_id   = "";
$test_criteria_code = "";
$test_code          = "";
$age                = "";
$gender             = "";
$min                = "";
$max                = "";
$category_criteria_detail_code  ="";
$type_test          = "";
$test_suggest       = "";
$status             = "";
$test_suggest_en    = "";


$optionCat= "<option></option>";

$sqlCat = "SELECT * FROM pfit_t_cat_criteria_detail where category_criteria_code = '$categoryCriteriaCode'";

$queryCat = DbQuery($sqlCat,null);

$jsonCat = json_decode($queryCat, true);
$numCat  = $jsonCat['dataCount'];
$rowCat  = $jsonCat['data'];

if(!empty($testCriteriaCode) && $statusData != "COPY" ){
  $type = "EDIT";

  $sql = "SELECT * FROM pfit_t_test_criteria where test_criteria_code  = '$testCriteriaCode'";

  $query = DbQuery($sql,null);

  $json = json_decode($query, true);
  $num  = $json['dataCount'];
  $row  = $json['data'];

  if($num > 0){
    $test_criteria_code = $row[0]['test_criteria_code'];
    $age                = $row[0]['age'];
    $gender             = $row[0]['gender'];
    $min                = $row[0]['min'];
    $max                = $row[0]['max'];
    $category_criteria_detail_code  = $row[0]['category_criteria_detail_code'];
    $type_test          = $row[0]['type_test'];
    $test_suggest       = $row[0]['test_suggest'];
    $test_suggest_en    = $row[0]['test_suggest_en'];
    $status             = $row[0]['status'];
  }

  if($numCat > 0){
    for ($i=0; $i < $numCat ; $i++) {
      $selected = "";
      $categoryCriteriaDetailName = $rowCat[$i]['category_criteria_detail_name'];
      $categoryCriteriaDetailCode = $rowCat[$i]['category_criteria_detail_code'];
      if($category_criteria_detail_code == $categoryCriteriaDetailCode){
        $selected = 'selected="selected"';
      }
      $optionCat .= "<option value='$categoryCriteriaDetailCode' $selected >$categoryCriteriaDetailName</option>";
    }
  }
  if($statusData == 'VIEW'){
    $disabled = 'disabled';
    $display = '';
    $visibility = 'visibility-non';

    //echo $statusType;
  }
}else{
  $type = "ADD";

  if($statusData == "COPY"){
    $sql = "SELECT * FROM pfit_t_test_criteria where test_criteria_code  = '$testCriteriaCode'";

    $query = DbQuery($sql,null);

    $json = json_decode($query, true);
    $num  = $json['dataCount'];
    $row  = $json['data'];

    if($num > 0){
      $age                = $row[0]['age'];
      $gender             = $row[0]['gender'];
      $min                = $row[0]['min'];
      $max                = $row[0]['max'];
      $category_criteria_detail_code  = $row[0]['category_criteria_detail_code'];
      $type_test          = $row[0]['type_test'];
      $test_suggest       = $row[0]['test_suggest'];
      $test_suggest_en    = $row[0]['test_suggest_en'];
      $status             = $row[0]['status'];
    }
  }

  $sql = "SELECT test_criteria_code FROM pfit_t_test_criteria order by test_criteria_code desc";

  $query = DbQuery($sql,null);

  $json = json_decode($query, true);
  $num  = $json['dataCount'];
  $row  = $json['data'];

  if($num > 0){
    $test_criteria_code = $row[0]['test_criteria_code'];
  }

  if(!empty($test_criteria_code)){
    $lastNum = substr($test_criteria_code,strlen($testCode));
    $lastNum = $lastNum + 1;
    $test_criteria_code = $testCode.sprintf("%03d", $lastNum);
  }else{
    $test_criteria_code = $testCode.sprintf("%03d", 1);
  }

  if($numCat > 0){
    for ($i=0; $i < $numCat ; $i++) {
      $selected = "";
      $categoryCriteriaDetailName = $rowCat[$i]['category_criteria_detail_name'];
      $categoryCriteriaDetailCode = $rowCat[$i]['category_criteria_detail_code'];
      if($statusData == "COPY"){
        if($category_criteria_detail_code == $categoryCriteriaDetailCode){
          $selected = 'selected="selected"';
        }
      }
      $optionCat .= "<option value='$categoryCriteriaDetailCode' $selected >$categoryCriteriaDetailName</option>";
    }
  }
}

?>

<div class="row">
  <div class="col-md-3">
    <div class="form-group">
      <label>อายุ</label>
      <input type="hidden" value="<?= $type ?>" name="type" required>
      <input type="hidden" name="test_criteria_code" value="<?= $test_criteria_code; ?>" required>
      <input type="hidden" name="test_code" value="<?= $testCode; ?>" required>
      <input type="number" name="age" value="<?= $age ?>" class="form-control" placeholder="อายุ" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>เพศ</label>
      <div class="form-control" style="border-style: hidden;">
        <input <?= $disabled?> type="radio" name="gender" id="gender_m" class="minimal" value="M" <?= $gender== 'M'? 'checked':''?> required><label for="gender_m">&nbsp;ชาย</label>&nbsp;&nbsp;
        <input <?= $disabled?> type="radio" name="gender" id="gender_f" class="minimal" value="F" <?= $gender == 'F'? 'checked':''?> required><label for="gender_f">&nbsp;หญิง</label>
      </div>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Min</label>
      <input type="text" name="min" value="<?= $min ?>" class="form-control" placeholder="ค่าต่ำสุดที่ใช้วัดเกณฑ์" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-3">
    <div class="form-group">
      <label>Max</label>
      <input type="text" data-smk-type="decimal" name="max" value="<?= $max ?>" class="form-control" placeholder="ค่าสูงสุดที่ใช้วัดเกณฑ์" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>รายการผลการทดสอบ</label>
      <select name="category_criteria_detail_code" class="form-control" <?=$disabled ?> required>
        <?= $optionCat ?>
      </select>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>ประเภทผู้ทดสอบ</label>
      <select name="type_test" class="form-control" <?=$disabled ?> required>
        <option></option>
        <option value="1" <?= ($type_test == '1' ? 'selected="selected"':'') ?>>บุคคลทั่วไป</option>
        <option value="2" <?= ($type_test == '2' ? 'selected="selected"':'') ?>>นักกีฬา</option>
      </select>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>สถานะ</label>
      <select name="status" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="Y" <?= ($status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>คำแนะนำภาษาไทย</label>
      <textarea class="form-control" name="test_suggest" id="content_data" <?=$disabled ?>><?= $test_suggest; ?></textarea>
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>คำแนะนำภาษาอังกฤษ</label>
      <textarea class="form-control" name="test_suggest_en" id="content_data2" <?=$disabled ?>><?= $test_suggest_en; ?></textarea>
    </div>
  </div>

</div>
