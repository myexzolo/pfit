<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$id = $_POST['value'];

$sql = "DELETE FROM t_module WHERE module_id = ?";

$ParamData  = array($id);
$query      = DbQuery($sql,$ParamData);
$row        = json_decode($query, true);
$status     = $row['status'];

if($status == "success"){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}
?>
