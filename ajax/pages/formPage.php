<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php

$sql = "SELECT * FROM t_module WHERE is_active = 'Y'";

$queryModule = DbQuery($sql,null);

$rowModule  = json_decode($queryModule, true);
$numModule  = $rowModule['dataCount'];
$dataModule = $rowModule['data'];

$page_id = "";
$page_code = "";
$page_name = "";
$page_seq = "";
$is_active = "";
$page_path = "";
$module_id = "";
$option    = "";

if(!empty($_POST['value'])){
  $sql = "SELECT * FROM t_page WHERE page_id = ?";

  $ParamData = array($_POST['value']);
  $query = DbQuery($sql,$ParamData);

  $row  = json_decode($query, true);
  $data = $row['data'];

  $page_id    = $data[0]['page_id'];
  $page_code  = $data[0]['page_code'];
  $page_name  = $data[0]['page_name'];
  $page_seq   = $data[0]['page_seq'];
  $is_active  = $data[0]['is_active'];
  $page_path  = $data[0]['page_path'];
  $module_id  = $data[0]['module_id'];

  for ($i=0; $i < $numModule ; $i++) {

    $module_id_mt   = $dataModule[$i]['module_id'];
    $module_code    = $dataModule[$i]['module_code'];
    $module_name    = $dataModule[$i]['module_name'];

    $selected =  ' ';
    if($module_id_mt == $module_id){
      $selected =  'selected="selected"';
    }
    $option .= '<option value="'.$module_id_mt.'" '.$selected.'>'.$module_code.' : '.$module_name.'</option>';
  }
}else{
  for ($i=0; $i < $numModule ; $i++) {
    $module_id_mt   = $dataModule[$i]['module_id'];
    $module_code    = $dataModule[$i]['module_code'];
    $module_name    = $dataModule[$i]['module_name'];

    $option .= '<option value="'.$module_id_mt.'" >'.$module_code.':'.$module_name.'</option>';
  }
}
?>
<div class="row">
  <div class="col-md-4">
    <div class="form-group">
      <label>Page Code</label>
      <input value="<?= $page_code ?>" name="page_code" type="text" maxlength="6" class="form-control" placeholder="Code" required>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>Page Name</label>
      <input value="<?= $page_name ?>" name="page_name" type="text" class="form-control" placeholder="Name" required>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Page Sequence</label>
      <input value="<?= $page_seq ?>" name="page_seq" type="number" maxlength="3" class="form-control" placeholder="Sequence" required>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>Module</label>
      <select name="module_id" class="form-control select2" style="width: 100%;" required>
        <?=$option ?>
      </select>
    </div>
  </div>
  <div class="col-md-4">
    <div class="form-group">
      <label>Status</label>
      <select name="is_active" class="form-control select2" style="width: 100%;" required>
        <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
  <div class="col-md-8">
    <div class="form-group">
      <label>Page Path</label>
      <input value="<?= $page_path ?>" name="page_path" type="text" class="form-control" placeholder="Path" >
    </div>
  </div>
</div>
<input type="hidden" name="page_id" value="<?= $page_id ?>">
