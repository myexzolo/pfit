<?php

session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../../Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

$dateStart  	= $_POST['dateStart'];
$projectCode 	= isset($_POST['project_code'])?$_POST['project_code']:"";
// $dateStart  = '2018/10/01';
$month = monthThaiFull(date("n", strtotime($dateStart)));
$year  = date("Y", strtotime($dateStart))+543;


$r 	 = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG");


$dateEnd    = date("Y-m", strtotime($dateStart));
$con = "";
if($projectCode != ""){
	$con = " and project_code = '$projectCode' ";
}
$end_date    = getQueryDate('end_date');
$end_date2   = getQueryDateWere('end_date');
$sql = "SELECT $end_date,project_name,location,project_code
FROM pfit_t_project where $end_date2 LIKE '$dateEnd%' and status <> 'C' $con
ORDER BY end_date ASC";

$query = DbQuery($sql,null);
$row  = json_decode($query, true);
$num  = $row['dataCount'];
$data = $row['data'];

$project_name = $data[0]['project_name'];
$location     = $data[0]['location'];
$end_date     = convDatetoThaiMonth($data[0]['end_date']);
$project_code = $data[0]['project_code'];
$end_date2 		= $data[0]['end_date'];

$name = "รายงานสรุปผลการทดสอบสมรรถภาพแต่ละฐานของ ".$project_name;
$sqlm = "SELECT person_gender,".getQueryDate('date_of_birth')." FROM pfit_t_person WHERE project_code = '$project_code'";
$querym = DbQuery($sqlm,null);
$rowm  = json_decode($querym, true);
$numm  = $rowm['dataCount'];


$sql = "SELECT stage,grade FROM pfit_t_person
			 WHERE project_code= '$project_code'
			 GROUP BY stage,grade";
//echo $sql;
$query = DbQuery($sql,null);
$row   = json_decode($query, true);
$numStage 	= $row['dataCount'];
$dataStage  = $row['data'];


$con = "";
if($projectCode != ""){
	$con = " and p.project_code = '$projectCode' ";
}
$end_date   = getQueryDateWere('p.end_date');

$sql = "SELECT DISTINCT(pt.test_code) as test_code,ts.test_name,ts.test_opjective,ts.category_criteria_code
				FROM pfit_t_project p , pfit_t_project_test pt, pfit_t_test ts WHERE p.project_code = pt.project_code $con
				AND ts.test_code = pt.test_code AND $end_date LIKE '$dateEnd%' ORDER BY pt.test_code ASC";

$queryT = DbQuery($sql,null);
$rowT  = json_decode($queryT, true);
$numT  = $rowT['dataCount'];
$dataT = $rowT['data'];
$tab = 0;

$CCC = array();

for($x=0;$x <$numT;$x++){
	$test_code 							= $dataT[$x]['test_code'];
	$test_name 							= $dataT[$x]['test_name'];
	$test_opjective 				= $dataT[$x]['test_opjective'];
	$category_criteria_code = $dataT[$x]['category_criteria_code'];
	$test_opjective = getTestOpjective($test_opjective);
	$test_name = str_replace("/"," ",$test_name);
  $test_name = str_replace('\\'," ",$test_name);

	if($tab == 0){
		$objPHPExcel->getActiveSheet()->setTitle($test_code);
	}else{
		$objPHPExcel->createSheet()->setTitle($test_code);
	}
	$objPHPExcel->setActiveSheetIndex($tab);
	$objPHPExcel->getActiveSheet($tab)->mergeCells('A2:A3')->setCellValue('A2','ระดับชั้น')->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


	$sql = "SELECT * FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code' ORDER BY category_criteria_detail_code";
	$queryDet 	= DbQuery($sql,null);
	$rowDet  		= json_decode($queryDet, true);
	$numDet  		= $rowDet['dataCount'];
	$dataDet 		= $rowDet['data'];

	$rs      = 1;
	$re      = $numDet;
	$start   = $r[$rs].'2';
	$end     = $r[$re].'2';
	$cellMerge = $start.":".$end;
	$objPHPExcel->getActiveSheet($tab)->mergeCells($cellMerge)->setCellValue($start,'ชาย (คน)')->getStyle($cellMerge)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	for($j=0;$j <$numDet;$j++){
		$detail_name 	= $dataDet[$j]['category_criteria_detail_name'];
		$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

		$col = $r[($rs+$j)];
		$resDetail['M'][$detail_code] =  array();
		$resDetail['M'][$detail_code]['COL'] = $col;
		$objPHPExcel->getActiveSheet()->setCellValue($col.'3', $detail_name)->getStyle($col.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
	$rs      = $re + 1;
	$re      = ($re + $numDet);
	$start   = $r[$rs].'2';
	$end     = $r[$re].'2';
	$cellMerge = $start.":".$end;

	$objPHPExcel->getActiveSheet($tab)->mergeCells($cellMerge)->setCellValue($start,'หญิง (คน)')->getStyle($cellMerge)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	for($j=0;$j <$numDet;$j++){
		$detail_name 	= $dataDet[$j]['category_criteria_detail_name'];
		$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

		$col = $r[($rs+$j)];
		$resDetail['F'][$detail_code] = array();
		$resDetail['F'][$detail_code]['COL'] = $col;
		//$resDetail['F'][$detail_code]['R'.$j] = $col;
		$objPHPExcel->getActiveSheet()->setCellValue($col.'3', $detail_name)->getStyle($col.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}

	$endTotal = $re;
	$rs      = $re + 1;
	$re      = ($re + $numDet);
	$start   = $r[$rs].'2';
	$end     = $r[$re].'2';
	$cellMerge = $start.":".$end;

	$objPHPExcel->getActiveSheet($tab)->mergeCells($cellMerge)->setCellValue($start,'รวม (คน)')->getStyle($cellMerge)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	for($j=0;$j <$numDet;$j++){
		$detail_name 	= $dataDet[$j]['category_criteria_detail_name'];
		$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

		$col = $r[($rs+$j)];
		$resDetail['T'][$detail_code] = array();
		$resDetail['T'][$detail_code]['COL'] = $col;
		$resDetail['T'][$detail_code]['TOTAL'] = 0;
		$objPHPExcel->getActiveSheet()->setCellValue($col.'3', $detail_name)->getStyle($col.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
	$objPHPExcel->getActiveSheet($tab)->mergeCells('A1:'.$r[$re].'1')->setCellValue('A1',"รายงานสรุปผลการทดสอบสมรรถภาพของฐาน  $test_name")->getStyle('A1:'.$r[$rs].'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$rowStage = 3;
	for($a= 0;$a < $numStage ; $a++){
		$rowStage++;
		$stage    = $dataStage[$a]['stage'];
		$grade    = $dataStage[$a]['grade'];

		$stageStr		= "";
		if($stage == 2){
			$stageStr = "ประถมศึกษาปีที่ ".$grade;
		}else if($stage == 1){
			$stageStr = "มัธยมศึกษาปีที่ ".$grade;
		}
		$sg = $stage.$grade;
		for($j=0;$j <$numDet;$j++){
			$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

			//echo $detail_code."<br>";
			$resDetail['M'][$detail_code][$sg] = 0;
			$resDetail['F'][$detail_code][$sg] = 0;
			$resDetail['T'][$detail_code][$sg] = 0;
		}
		//echo $stageStr." ,".$sg." ,".$rowStage."<br>";
		$objPHPExcel->getActiveSheet($tab)->setCellValue('A'.$rowStage, $stageStr);
	}


	//$sql = "SELECT t.*, p.stage,p.grade FROM pfit_t_result r , pfit_t_test_criteria t, pfit_t_person p
	//				WHERE r.test_code='$test_code' and r.test_criteria_code = t.test_criteria_code
	//				and r.project_code = '$projectCode' and r.person_number = p.person_number and p.project_code = '$projectCode' ";

	//$sql = "SELECT  r.*,p.stage,p.grade  FROM pfit_t_result r
	//				LEFT JOIN pfit_t_person p ON r.person_number = p.person_number
//					WHERE r.project_code = '$projectCode' AND  p.project_code = '$projectCode'  AND r.test_code = '$test_code'
//					ORDER BY r.test_criteria_code,p.stage,p.grade";

	$sql = "SELECT r.*,p.stage,p.grade,c.age,c.gender,c.category_criteria_detail_code
					FROM pfit_t_result r, pfit_t_person p, pfit_t_test_criteria c
					WHERE r.test_code='$test_code'
					and r.project_code = '$projectCode'
					and  r.person_number = p.person_number and p.project_code = '$projectCode'
					and r.test_criteria_code = c.test_criteria_code
					order by  r.test_criteria_code,p.stage,p.grade";

	$queryRes 	= DbQuery($sql,null);
	$rowRes  		= json_decode($queryRes, true);
	$numRes  		= $rowRes['dataCount'];
	$dataRes 		= $rowRes['data'];
	//echo $sql."<br>";
	for($e=0;$e < $numRes; $e++)
	{
			$testCriteriaCode	= $dataRes[$e]['test_criteria_code'];
			$stage    				= $dataRes[$e]['stage'];
			$grade    				= $dataRes[$e]['grade'];
			$detailCode 			= $dataRes[$e]['category_criteria_detail_code'];
			$age 							= $dataRes[$e]['age'];
			$gender 					= $dataRes[$e]['gender'];

			$sg = $stage.$grade;
			//echo $sg."<br>";
			if(isset($resDetail[$gender][$detailCode][$sg]))
			{
				$resDetail[$gender][$detailCode][$sg]++;
			}
			//print_r ($resDetail);
			$resDetail['T'][$detailCode]['TOTAL']++;
			$resDetail['T'][$detailCode][$sg]++;
	}

	$rowStage = 3;
	$total = 0;
	for($a= 0;$a < $numStage; $a++){
		$rowStage++;
		$stage    = $dataStage[$a]['stage'];
		$grade    = $dataStage[$a]['grade'];
		$sg = $stage.$grade;

		for($j=0;$j <$numDet;$j++){
			$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];
			//echo $detail_code."<br>";
			$resM = $resDetail['M'][$detail_code][$sg];
			$resF = $resDetail['F'][$detail_code][$sg];
			$resT = $resDetail['T'][$detail_code][$sg];

			$colM = $resDetail['M'][$detail_code]['COL'];
			$colF = $resDetail['F'][$detail_code]['COL'];
			$colT = $resDetail['T'][$detail_code]['COL'];

			$objPHPExcel->getActiveSheet($tab)->setCellValue($colM.$rowStage, $resM);
			$objPHPExcel->getActiveSheet($tab)->setCellValue($colF.$rowStage, $resF);
			$objPHPExcel->getActiveSheet($tab)->setCellValue($colT.$rowStage, $resT);
		}
	}







	//
	//
	// $objPHPExcel->getActiveSheet($tab)->mergeCells('A4:A58')->setCellValue('A4', $test_opjective);
	// $objPHPExcel->getActiveSheet($tab)->mergeCells('B4:B58')->setCellValue('B4', $test_name);
  // $objPHPExcel->getActiveSheet($tab)->getStyle('A4:B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	// $objPHPExcel->getActiveSheet($tab)->setCellValue('C4', 'น้อยกว่า 7');
	// $objPHPExcel->getActiveSheet($tab)->setCellValue('C58', '60 ขึ้นไป');
	// $objPHPExcel->getActiveSheet($tab)->mergeCells('A59:'.$r[$endTotal].'59')->setCellValue('A59', 'รวม')->getStyle('A59')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
	//
	// for($j=0;$j <$numDet;$j++){
	// 	$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];
	// 	//echo $detail_code."<br>";
	// 	$total  = $resDetail['T'][$detail_code]['TOTAL'];
	// 	$colT 	= $resDetail['T'][$detail_code]['COL'];
	//
	// 	$objPHPExcel->getActiveSheet($tab)->setCellValue($colT.'59', $total);
	// }

	foreach(range('A','U') as $columnID) {
	    $objPHPExcel->getActiveSheet($tab)->getColumnDimension($columnID)
	        ->setAutoSize(true);
	}
	$tab++;
}



$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');


?>
