<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}
th {
  text-align: center !important;
}
</style>
<?php
  $dateStart  = $_POST['dateStart'];
  // echo $dateStart  = '2018/10/01';
  $month = monthThaiFull(date("n", strtotime($dateStart)));
  $year  = date("Y", strtotime($dateStart))+543;
?>
<div class="col-md-12">
  <div class="box boxBlack">
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:16px;"><b>รายงานสรุปผลการทดสอบสมรรถภาพแต่ละฐาน เดือน <?=$month.' '.$year; ?></b></p>
        <table class="table table-bordered table-striped">
          <thead>
        		<tr>
        			<th>ลำดับ</th>
              <th>รหัส</th>
              <th>ชื่อกิจกรรม</th>
              <th>สถานที่</th>
              <th>ระยะเวลา</th>
        			<th>Excel</th>
        		</tr>
          </thead>
        <tbody class="text-center">
            <?php
              $dateStart  = $_POST['dateStart'];
              $dateEnd    = date("Y-m", strtotime($dateStart));
              $end_date   = getQueryDate('end_date');
              $start_date = getQueryDate('start_date');
              $end_date2   = getQueryDateWere('end_date');
              $con = "";
              if(!strstr($_SESSION['ROLECODE'], 'ADM')){
                  $con .= " and user_login = '".$_SESSION['USER_NAME']."'";
              }

              $sql = "SELECT $end_date,$start_date,project_name,location,project_code
              FROM pfit_t_project where $end_date2 LIKE '$dateEnd%' and status <> 'C' and project_type in (1,2)  $con
              ORDER BY end_date ASC";

              $query = DbQuery($sql,null);
              $row  = json_decode($query, true);
              $num  = $row['dataCount'];
              $data = $row['data'];

              if($num>0){
                foreach ($data as $key => $value) {
                  $project_name = $value['project_name'];
                  $location     = $value['location'];
                  $start_date   = convDatetoThaiMonth($value['start_date']);
                  $end_date     = convDatetoThaiMonth($value['end_date']);
                  $project_code = $value['project_code'];
                  $end_date2    = $value['end_date'];

                  $urlProject = "ajax/report03/excelexport.php?dateStart=$dateStart&project_code=$project_code";
            ?>
            <tr>
              <td><?=$key+1; ?></td>
              <td class="text-left"><?=$project_code; ?></td>
              <td class="text-left"><?=$project_name; ?></td>
              <td class="text-left" ><?=$location; ?></td>
              <td ><?=$start_date." - ".$end_date; ?></td>
              <td><a onclick="postURL_blank('<?=$urlProject ?>')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
            </tr>
            <?php
                }
               }else{ ?>
              <tr>
          			<td colspan="13">ไม่พบข้อมูล</td>
              </tr>
            <?php } ?>
        	</tbody>
        </table>

      </div>
    </div>
    </div>
  </div>
</div>
</div>
