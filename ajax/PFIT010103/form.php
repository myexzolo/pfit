<?php
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code        = $_GET['code'];
$projectCode = $_GET['projectCode'];

$statusType = $_GET['status'];
$disabled = '';
$readonly = '';
$visibility = '';

$project_code   = "";
$person_number  = "";
$person_gender  = "";
$person_name    = "";
$person_lname   = "";
$date_of_birth  = "";
$wiegth         = "";
$heght          = "";
$person_type    = "";
$status         = "";

//
if(!empty($code)){
  $type = "EDIT";

  // $sql = "SELECT * FROM pfit_t_person where project_code = '$projectCode' and person_number = '$code'";
  $sql = "SELECT * FROM pfit_t_result
          INNER JOIN pfit_t_person ON pfit_t_result.person_number = pfit_t_person.person_number
          INNER JOIN pfit_t_test ON pfit_t_result.test_code = pfit_t_test.test_code
          where pfit_t_person.project_code = '$projectCode' and pfit_t_person.person_number = '$code'";
  $query = DbQuery($sql,null);
  $row = json_decode($query, true);
  $num = count($row);

  if($num > 0){
    $person_number  = $row[0]['person_number'];
    $person_gender  = $row[0]['person_gender'];
    $person_name    = $row[0]['person_name'];
    $person_lname   = $row[0]['person_lname'];
    $date_of_birth  = $row[0]['date_of_birth'];
    $wiegth         = $row[0]['wiegth'];
    $heght          = $row[0]['heght'];
    $person_type    = $row[0]['person_type'];
  }

  $readonly = 'readonly';
  if($statusType == 'VIEW'){
    $disabled = 'disabled';
    $display = '';
    $visibility = 'visibility-non';

    echo $statusType;
  }
}


$sql = "SELECT * FROM pfit_t_test";
$query = DbQuery($sql,null);
$row = json_decode($query, true);
$num = count($row);
$table = '';
if($num > 0){
  $test_name  = $row[0]['test_name'];
  $test_unit  = $row[0]['test_unit'];
  foreach ($row as $key => $value) {
      $key= $key+1;
      $table .= "<tr class='text-center'>
                    <td>$key</td>
                    <td>$test_name</td>
                    <td>$test_unit</td>
                    <td><input type='text'/></td>
                    <td></td>
                  </tr>";

  }

}



?>

<div class="row">
  <div class="col-md-4">
    <p>เลขประจำตัว : <?= $type ?></p>
  </div>
  <div class="col-md-4">
    <p>ชื่อ : <?= $person_name ?></p>
  </div>
  <div class="col-md-4">
    <p>สกุล : <?= $person_lname ?></p>
  </div>
  <div class="col-md-4">
    <p>วันเดือนปี เกิด : <?= $date_of_birth; ?></p>
  </div>
  <div class="col-md-4">
    <p>เพศ : <?= $person_gender=='M'?"ชาย":"หญิง"; ?> น้ำหนัก : <?= $wiegth; ?></p>
  </div>
  <div class="col-md-4">
    <p>ส่วนสูง : <?= $heght; ?></p>
  </div>

  <div class="col-md-12">
    <table class="table table-bordered">
      <thead>
        <tr class="text-center">
          <td>ลำดับ</td>
          <td>รายการ</td>
          <td>หน่วย</td>
          <td>ผลการวัด</td>
          <td>เกณฑ์การวัด</td>
        </tr>
      </thead>
      <tbody>
        <?= $table ?>
      </tbody>
    </table>
  </div>


</div>
