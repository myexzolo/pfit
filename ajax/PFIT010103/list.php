<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);


$project_code = $_POST['projectCode'];

$personId     = isset($_POST['personId'])?$_POST['personId']:"";
$personName   = isset($_POST['personName'])?$_POST['personName']:"";
$personLname  = isset($_POST['personLname'])?$_POST['personLname']:"";




$sql   = "SELECT TO_CHAR( end_date, 'YYYY-MM-DD') as end_date FROM pfit_t_project WHERE project_code = '$project_code'";
$query = DbQuery($sql,null);
$json  = json_decode($query, true);
$row   = $json['data'];

$end_date = $row[0]['end_date'];


$con = "";

if($personId != "")
{
  $con .= " and person_number like  '%$personId%'";
}

if($personName != "")
{
  $con .= " and person_name like '$personName%'";
}

if($personLname != "")
{
  $con .= " and person_lname like '$personLname%'";
}

//$project_code = "61100004_1539515202";

$sql_test = "SELECT t.* FROM pfit_t_test t,pfit_t_project_test pt where t.test_code = pt.test_code and pt.project_code = '$project_code' order by pt.test_seq";
$query_test = DbQuery($sql_test,null);
$jsonTest   = json_decode($query_test, true);
$rowTest    = $jsonTest['data'];
$numTest    = $jsonTest['dataCount'];


if($_SESSION['TYPE_CONN'] == '2'){
  $sql  = "SELECT * FROM pfit_t_person WHERE project_code = '$project_code' $con ";
}else{
  $sql  = "SELECT project_code, person_number, person_name, person_lname , person_gender , TO_CHAR( date_of_birth, 'YYYY-MM-DD') as date_of_birth, wiegth, height
           FROM pfit_t_person WHERE project_code = '$project_code' $con  and rownum < 21 order by date_create desc";
}

$query = DbQuery($sql,null);
$json  = json_decode($query, true);
$row   = $json['data'];
$num   = $json['dataCount'];

?>
<style>
  th {
    text-align: center !important;
  }
</style>

<div class="table-responsive">
<table id="tableDisplay" class="table table-bordered table-striped">
    <thead>
      <tr>
          <th></th>
          <th data-align="center" style="line-height: 40px;">QR<input type="hidden" id="numPerson" value="<?= $num ?>"></th>
          <th data-align="center" style="line-height: 40px;">เลขประจำตัว</th>
          <th data-align="center" style="line-height: 40px;">ชื่อ</th>
          <th data-align="center" style="line-height: 40px;">สกุล</th>
          <th data-align="center" style="line-height: 40px;">เพศ</th>
          <th data-align="center" style="line-height: 40px;">วันเดือนปีเกิด</th>
          <th data-align="center" style="line-height: 40px;">อายุ</th>
          <th data-align="center" style="line-height: 40px;">น้ำหนัก</th>
          <th data-align="center" style="line-height: 40px;">ส่วนสูง</th>
          <?php
              for ($x = 0; $x < $numTest; $x++)
              {
                $test_code = $rowTest[$x]["test_code"];
                $test_name = $rowTest[$x]["test_name"];
                $test_unit = $rowTest[$x]["test_unit"];
                echo "<th data-align='right'>$test_name<br>($test_unit)</th>";
              }
          ?>
      </tr>
    </thead>
    <tbody>
      <?php
      for ($i = 0; $i < $num; $i++)
      {
        $project_code  = $row[$i]['project_code'];
        $person_number = $row[$i]['person_number'];
        $parm = "?code=".$row[$i]['project_code']."&id=".$row[$i]['person_number'];


        $sql_re = "SELECT result,test_code FROM pfit_t_result where project_code = '$project_code' and person_number = '$person_number'";
        $query_re = DbQuery($sql_re,null);
        $jsonRe   = json_decode($query_re, true);
        $nums    = $jsonRe['dataCount'];

        $resultArr = array();
        if($nums > 0){
          $rowRe    = $jsonRe['data'];
          for ($j = 0; $j < $nums; $j++)
          {
            $testCode  = $rowRe[$j]["test_code"];
            $Result    = str_replace("|", ",", $rowRe[$j]['result']);
            $resultArr[$testCode] = $Result;
          }
        }
        $date_of_birth = $row[$i]['date_of_birth'];

      ?>
      <tr>
      <td class="text-center"><a onclick="postURLV2('report_result.php<?= $parm ?>')"><span class="glyphicon glyphicon-print"></a></span></td>
      <td class="text-center"><a onclick="postURLV2('qrcodePerson.php<?= $parm ?>')"><span class="fa fa-qrcode"></a></span></td>
      <td><?= $row[$i]['person_number']; ?></td>
      <td><?= $row[$i]['person_name']; ?></td>
      <td><?= $row[$i]['person_lname']; ?></td>
      <td><?= getGender($row[$i]['person_gender']); ?></td>
      <td><?= DateThai($date_of_birth); ?></td>
      <td align="center"><?= yearBirth2($date_of_birth,$end_date); ?></td>
      <td><?= $row[$i]['wiegth']; ?></td>
      <td><?= $row[$i]['height']; ?></td>
      <?php
          for ($x = 0; $x < $numTest; $x++)
          {
            $test_code = $rowTest[$x]["test_code"];
            //$test_name = $rowTest[$x]["test_name"];

            // $sql_re = "SELECT result FROM pfit_t_result where project_code = '$project_code' and person_number = '$person_number' and test_code = '$test_code'";
            // $query_re = DbQuery($sql_re,null);
            // $jsonRe   = json_decode($query_re, true);
            // $nums    = $jsonRe['dataCount'];
            // $nums = 0;
            // if($nums >0){
            //   $rowRe    = $jsonRe['data'];
            //   $esult = str_replace("|", ",", $rowRe[0]['result']);
            //   echo "<td class='text-right'>$esult</td>";
            // }else{
            //   echo "<td class='text-right'></td>";
            // }

            if(isset($resultArr[$test_code]) && !empty($resultArr[$test_code])){
              echo "<td class='text-right'>".$resultArr[$test_code]."</td>";
            }else{
              echo "<td class='text-right'></td>";
            }

          }
        }
      ?>
      </tr>
    </tbody>
</table>
</div>
<script>
  $(function () {
    $('#tableDisplay').DataTable({
      'paging'      : true,
      'lengthChange': true,
      'searching'   : true,
      'ordering'    : false,
      'info'        : true,
      'autoWidth'   : false
    })
  })

</script>
