<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$test_code        = $_POST['test_code'];
$test_name        = $_POST['test_name'];
$test_opjective   = $_POST['test_opjective'];
$test_detail      = $_POST['test_detail'];
$category_criteria_code = $_POST['category_criteria_code'];
$test_unit        = $_POST['test_unit'];
$test_calculator  = $_POST['test_calculator'];
$status           = $_POST['status'];
$test_min         = $_POST['test_min'];
$test_max         = $_POST['test_max'];
$test_type        = $_POST['test_type'];
$test_age_min     = $_POST['test_age_min'];
$test_age_max     = $_POST['test_age_max'];
//$test_display     = $_POST['test_display'];
$test_display     = implode(",",@$_POST['test_display']);

$type   = $_POST['type'];

if($type == "EDIT"){
  $sql = "UPDATE pfit_t_test SET
          test_name       = '$test_name',
          test_opjective  = '$test_opjective',
          test_detail     = '$test_detail',
          category_criteria_code    = '$category_criteria_code',
          test_unit       = '$test_unit',
          test_calculator = '$test_calculator',
          test_min        = '$test_min',
          test_max        = '$test_max',
          test_type       = '$test_type',
          test_age_min    = '$test_age_min',
          test_age_max    = '$test_age_max',
          test_display    = '$test_display',
          status          = '$status'
          WHERE test_code   = '$test_code'";
}else{

  //$sql = "SELECT test_code FROM pfit_t_test order by test_code desc";
  $sql = "SELECT test_code FROM pfit_t_test where test_code like 'TST%' order by test_code desc";
  $query = DbQuery($sql,null);
  $json = json_decode($query, true);

  $num  = $json['dataCount'];
  $row  = $json['data'];

  if($num > 0){
    $test_code = $row[0]['test_code'];
  }

  if(!empty($test_code)){
    $lastNum = substr($test_code,3);
    $lastNum = $lastNum + 1;
    $test_code = "TST".sprintf("%02d", $lastNum);
  }else{
    $test_code = "TST".sprintf("%02d", 1);
  }

  $sql = "INSERT INTO pfit_t_test
         (test_code,test_name,test_opjective,test_detail,category_criteria_code,test_unit,test_calculator,status,test_min,test_max,test_type,test_age_min,test_age_max,test_display)
         VALUES('$test_code','$test_name','$test_opjective','$test_detail','$category_criteria_code','$test_unit','$test_calculator','$status','$test_min','$test_max','$test_type','$test_age_min','$test_age_max','$test_display')";
}

$query  = DbQuery($sql,null);
$row    = json_decode($query, true);
$status = $row['status'];

if($status == "success")
{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :')));
}
?>
