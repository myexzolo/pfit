<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$category_criteria_code = $_POST['category_criteria_code'];
$category_criteria_name = $_POST['category_criteria_name'];
$detail = $_POST['detail'];
$status = $_POST['status'];

$type   = $_POST['type'];

$category_criteria_detail_code   = $_POST['category_criteria_detail_code'];
$category_criteria_detail_name   = $_POST['category_criteria_detail_name'];
$category_criteria_detail_value  = $_POST['category_criteria_detail_value'];
$statusCD                        = $_POST['statusCD'];


if($type == "EDIT"){
  $sql = "UPDATE pfit_t_category_criteria SET
          category_criteria_name  = '$category_criteria_name',
          detail    = '$detail',
          status    = '$status'
          WHERE category_criteria_code   = '$category_criteria_code'";
}else{
  $sql = "INSERT INTO pfit_t_category_criteria
         (category_criteria_code,category_criteria_name,detail,status)
         VALUES('$category_criteria_code','$category_criteria_name','$detail','$status')";
}

$query  = DbQuery($sql,null);
$row    = json_decode($query, true);
$status = $row['status'];

if($status == "success"){
  $sql = "DELETE FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = ?";
  $ParamData = array($category_criteria_code);
  DbQuery($sql,$ParamData);

  $num = count($category_criteria_detail_code);

  for($x=0 ; $x < $num ; $x++){
    $categoryCriteriaDetailCode = $category_criteria_detail_code[$x];
    $categoryCriteriaDetailName = $category_criteria_detail_name[$x];
    $categoryCriteriaDetailValue = $category_criteria_detail_value[$x];
    $status = $statusCD[$x];

    $sql = "INSERT INTO pfit_t_cat_criteria_detail
           (category_criteria_detail_code,category_criteria_code,category_criteria_detail_name,category_criteria_detail_value,status)
           VALUES('$categoryCriteriaDetailCode','$category_criteria_code','$categoryCriteriaDetailName','$categoryCriteriaDetailValue','$status')";
    DbQuery($sql,null);
  }

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));

}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail :')));
}
?>
