<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$disabled = '';
$readonly = '';
$visibility = '';

?>

<div class="row">
  <div class="col-md-12">
    <div class="box boxBlack">
          <div class="box-header with-border">
            <h3 class="box-title"></h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-md-8 col-md-offset-2">
                  <div class="form-group col-md-12">
                    <label class="col-sm-5 control-label">นำเข้า ไฟล์ .TXT</label>
                    <div class="col-sm-7">
                      <input type="file" name="filepath" accept=".txt" required>
                    </div>
                  </div>
              </div>
            </div>
          </div>
    </div>
  </div>
</div>
