<?php
include('../../conf/connect.php');
include('../../inc/utils.php');
session_start();

$target_file    = $_FILES["filepath"]["name"];
$inputFileName  = $target_file;

if (file_exists($target_file)) {
  unlink($target_file);
}

$success = 0;
$fail    = 0;
$total   = 0;
$message = "นำเข้าไม่สำเร็จ";
$status   = 'danger';

if (move_uploaded_file($_FILES["filepath"]["tmp_name"], $target_file)) {
  // $a = readfile($inputFileName);
  $data = file_get_contents($inputFileName);
  if($data != ""){
    $data   = base64_decode($data);
    //echo $data;
    $arrTable = json_decode($data,true);
    if($arrTable['dataCount'] > 0){
      //print_r($arrTable['data']);
       $_SESSION['TYPE_CONN'] = 2;
       foreach ($arrTable['data'] as $key => $value) {
         $total++;
         $user_login        = $value['user_login'];
         $user_password	    = $value['user_password'];
         $user_name         = $value['user_name'];
         $is_active         = $value['is_active'];
         $role_list         = $value['role_list'];
         $user_img          = $value['user_img'];
         $user_id_update    = $value['user_id_update'];
         $note1	            = $value['note1'];
         $note2	            = $value['note2'];

         $str   = "SELECT * FROM t_user WHERE user_login = '$user_login'";
         $query = DbQuery($str,null);
         $json  = json_decode($query, true);
         $num   = $json['dataCount'];

         if($num > 0){
             $sql = "UPDATE t_user SET
                       user_password	   = '$user_password',
                       user_name        = '$user_name',
                       is_active        = '$is_active',
                       role_list        = '$role_list',
                       user_img	       = '$user_img',
                       user_id_update	 = '$user_id_update',
                       note1	           = '$note1',
                       note2	           = '$note2'
                    WHERE user_login  = '$user_login'";


            $js      = DbQuery($sql,null);
            $row     = json_decode($js, true);
            $status  = $row['status'];

            $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
            if($errorInfo > 0){
              $fail++;
              $message = "นำเข้าไม่สำเร็จ";
              $status   = 'danger';
            }else{
              $success++;
              $message = "นำเข้าสำเร็จ";
              $status   = 'success';
            }
          }
         else
         {
             $sql = "INSERT INTO t_user
                    (user_login,user_name,user_password,is_active,
                      role_list,user_img,user_id_update,note1,note2)
                    VALUES('$user_login','$user_name','$user_password','$is_active',
                      '$role_list','$user_img','$user_id_update','$note1','$note2')";

            $js      = DbQuery($sql,null);
            $row     = json_decode($js, true);
            $status  = $row['status'];

             $errorInfo  = isset($row['errorInfo'][0])?$row['errorInfo'][0]:1;
             if($errorInfo > 0){
               $fail++;
               $message = "นำเข้าไม่สำเร็จ";
               $status   = 'danger';
             }else{
               $success++;
               $message = "นำเข้าสำเร็จ";
               $status   = 'success';
             }
           }
       }
    }
  }
}
//echo 'success :'.$success.' fail :'.$fail.' total :'.$total;
header('Content-Type: application/json');
exit(json_encode(array('status' => $status, 'message' => $message, 'success'=>$success, 'fail'=>$fail, 'total'=>$total)));


?>
