<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$code = $_GET['code'];
$statusType = $_GET['status'];
$disabled = '';
$visibility = '';

$PROJECT_ID       = "";
$PROJECT_CODE     = "";
$PROJECT_NAME     = "";
$START_DATE       = date('Y/m/d');
$END_DATE         = date('Y/m/d');
$LOCATION         = "";
$PROJECT_DETAIL   = "";
$STATUS           = "";
$TEST_ID          = "";
$category_criteria_code = "";
$status           = "";

$option = '';
$TEST_IDs = array();

$optionCat= "<option></option>";

$sqlCat = "SELECT * FROM pfit_t_category_criteria where status = 'Y'";

$queryCat = DbQuery($sqlCat,null);
$rowCat   = json_decode($queryCat, true);
$numCat   = count($rowCat);

if(!empty($code)){
  $type = "EDIT";

  $sql = "SELECT * FROM PFIT_T_PROJECT where PROJECT_CODE  = '$code'";
  $query = DbQuery($sql,null);
  $row = json_decode($query, true);
  $num = count($row);



  if($num > 0){
    $PROJECT_ID       = $row[0]['PROJECT_ID'];
    $PROJECT_CODE     = $row[0]['PROJECT_CODE'];
    $PROJECT_NAME     = $row[0]['PROJECT_NAME'];
    $START_DATE       = $row[0]['START_DATE'];
    $END_DATE         = $row[0]['END_DATE'];
    $LOCATION         = $row[0]['LOCATION'];
    $PROJECT_DETAIL   = $row[0]['PROJECT_DETAIL'];
    $STATUS           = $row[0]['STATUS'];
    $TEST_ID          = $row[0]['TEST_ID'];
    $TEST_IDs = explode("," , $TEST_ID);
  }

  if($numCat > 0){
    for ($i=0; $i < $numCat ; $i++) {
      $selected = "";
      $category_criteria_name = $rowCat[$i]['category_criteria_name'];
      $categoryCriteriaCode = $rowCat[$i]['category_criteria_code'];
      if($category_criteria_code == $categoryCriteriaCode){
        $selected = 'selected="selected"';
      }
      $optionCat .= "<option value='$categoryCriteriaCode' $selected >$category_criteria_name</option>";
    }
  }
  if($statusType == 'VIEW'){
    $disabled = 'disabled';
    $display = '';
    $visibility = 'visibility-non';

    echo $statusType;
  }
}else{
  $type = "ADD";

  $sql = "SELECT PROJECT_CODE FROM PFIT_T_PROJECT order by PROJECT_CODE desc";

  $query = DbQuery($sql,null);
  $row = json_decode($query, true);

  $num = count($row);

  if($num > 0){
    $PROJECT_CODE = $row[0]['PROJECT_CODE'];
  }

  if(!empty($PROJECT_CODE)){
    $lastNum = substr($PROJECT_CODE,3);
    $lastNum = $lastNum + 1;
    $date = date("Y")+543;
    $dates = date("m");
    $PROJECT_CODE = substr($date, 2, 4).$dates.sprintf("%04d", $lastNum);
  }else{
    $PROJECT_CODE = substr($date, 2, 4).$dates.sprintf("%04d", 1);
  }

  if($numCat > 0){
    for ($x=0; $x < $numCat; $x++) {
      $selected = "";
      $category_criteria_name = $rowCat[$x]['category_criteria_name'];
      $category_criteria_code = $rowCat[$x]['category_criteria_code'];
      $optionCat .= "<option value='$category_criteria_code' $selected >$category_criteria_name</option>";
    }
  }
}

$sql_test = "SELECT * FROM pfit_t_test";
$query_test = DbQuery($sql_test,null);
$row_test = json_decode($query_test, true);

foreach ($row_test as $key => $value) {
  $check = '';
  if (in_array($value['test_code'],$TEST_IDs)){
       $check = 'checked';
  }else{
       $check = '';
  }

    $option .= "<tr class='text-center'>
                  <td><input type='checkbox' name='TEST_IDs[".$value['test_code']."]' value='1' $check></td>
                  <td>".$value['test_name']."</td>
                  <td>".$value['test_detail']."</td>
                </tr>";

}

?>

<div class="row">
  <div class="col-md-6">
    <div class="form-group">
      <label>ชื่อฐานการทดสอบ</label>
      <input type="hidden" value="<?= $PROJECT_ID ?>" name="type" required>
      <input type="text" name="test_name" value="<?= $PROJECT_NAME ?>" class="form-control" placeholder="ชื่อฐานการทดสอบ" <?=$disabled ?> required>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>รหัส</label>
      <input type="text" name="test_code" value="<?= $PROJECT_CODE; ?>" class="form-control" placeholder="รหัสฐาน" <?=$disabled ?> readonly >
    </div>
  </div>
  <div class="col-md-12">
    <div class="form-group">
      <label>ชื่อกิจกรรม</label>
      <input type="text" name="test_opjective" value="<?= $PROJECT_NAME; ?>" class="form-control" placeholder="ชื่อกิจกรรม"  <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>วันที่เริ่ม</label>
      <input type="date" name="test_opjective" value="<?= $START_DATE; ?>" class="form-control" placeholder="วันที่เริ่ม"  <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>วันที่สิ้นสุด</label>
      <input type="date" name="test_opjective" value="<?= $END_DATE; ?>" class="form-control" placeholder="วันที่สิ้นสุด"  <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>รายละเอียด</label>
      <input type="text" class="form-control" value="<?= $PROJECT_DETAIL; ?>" name="test_detail" placeholder="รายละเอียด" <?=$disabled ?>>
    </div>
  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label>สถานะ</label>
      <select name="status" class="form-control select2" style="width: 100%;" <?=$disabled; ?> required>
        <option value="Y" <?= ($status == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($status == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>

    <div class="col-md-12">
      <table class="table table-bordered">
        <thead>
          <tr class="text-center">
            <td></td>
            <td>ชื่อฐานการทดสอบ</td>
            <td>รายละเอียด</td>
          </tr>
        </thead>
        <tbody>
          <?php echo $option; ?>
        </tbody>
      </table>
    </div>
</div>
