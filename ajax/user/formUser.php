<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>

<?php
//echo $_SESSION['ROLECODE'];
$index = strpos($_SESSION['ROLECODE'],"SADM");
$con = "";

if($index <= 0 || empty($index) ){
  $con = " and role_code not in ('SADM') ";
}

$sql = "SELECT * FROM t_role WHERE is_active = 'Y' ".$con." ORDER BY role_id";

$queryRole = DbQuery($sql,null);

$rowRole = json_decode($queryRole, true);
$numRole  = $rowRole['dataCount'];
$dataRole = $rowRole['data'];


$user_id = "";
$user_login = "";
$user_password = "";
$user_name = "";
$user_img  = "";
$is_active = "";
$option    = "";

$note1     = "";
$note2     = "";

$display    = "block";
$display2   = "block";
$required    = "required";
$required2   = "required";
$type       = "";

$labPW = "Password";
if(isset($_POST['typeEdit'])){
  $type = $_POST['typeEdit'];

  if($type == "edit"){
    $display    = "none";
    $required    = "";
  }
  if($type == "resetPw"){
    $display2     = "none";
    $required2    = "";
    $labPW = "New Password";
  }
}


if(!empty($_POST['value'])){
  $sql = "SELECT * FROM t_user WHERE user_id = ?";

  $ParamData = array($_POST['value']);
  $query = DbQuery($sql,$ParamData);

  $row = json_decode($query, true);

  $num  = $row['dataCount'];
  $data = $row['data'];


  $user_id        = $data[0]['user_id'];
  $user_login     = $data[0]['user_login'];
  $user_password  = $data[0]['user_password'];
  $user_name      = $data[0]['user_name'];
  $is_active      = $data[0]['is_active'];
  $role_list      = $data[0]['role_list'];
  $user_img       = $data[0]['user_img'];
  $note1          = isset($data[0]['note1'])?$data[0]['note1']:"";
  $note2          = isset($data[0]['note2'])?$data[0]['note2']:"";

  if($type == "resetPw"){
    $user_password = "";
  }

  $mapRole = explode(",", $role_list);

  for ($i=0; $i < $numRole ; $i++) {

    $role_id   = $dataRole[$i]['role_id'];
    $role_name = $dataRole[$i]['role_name'];

    $checked = ' ';
    if(array_search($role_id,$mapRole) != ""){
      $checked =  "checked";
    }
    $option .= '<div class="form-group col-md-6"><input class="minimal" name="role_list[]" id="'.$role_id.'" type="checkbox" value="'.$role_id.'" '.$checked.'> <label for="'.$role_id.'">'.$role_name.'</lable></div>';
  }
}else{
  for ($i=0; $i < $numRole ; $i++) {
    $role_id   = $dataRole[$i]['role_id'];
    $role_name = $dataRole[$i]['role_name'];

    $option .= '<div class="form-group col-md-6"><input class="minimal" name="role_list[]" id="'.$role_id.'" type="checkbox" value="'.$role_id.'" > <label for="'.$role_id.'">'.$role_name.'</lable></div>';
  }
}
?>
<div class="row">
  <div class="col-md-5" style="display: <?= $display2 ?>" >
    <div class="form-group">
        <label>User Login</label>
        <input value="<?= $user_login ?>" autocomplete="off" name="user_login" type="text" class="form-control" placeholder="UserLogin" <?= $required2 ?>>
    </div>
  </div>
  <div class="col-md-7" style="display: <?= $display2 ?>" >
    <div class="form-group">
        <label>Name</label>
        <input value="<?= $user_name ?>" name="user_name" type="text" class="form-control" placeholder="Name" <?= $required2 ?>>
    </div>
  </div>
  <div class="col-md-6" style="display: <?= $display ?>" >
      <div class="form-group">
        <label><?= $labPW ?></label>
        <input value="<?= $user_password ?>" id="user_password" name="user_password" type="password" class="form-control" placeholder="Password" <?= $required ?>>
      </div>
  </div>
  <div class="col-md-6" style="display: <?= $display ?>" >
    <div class="form-group">
      <label>Confirm Password</label>
      <input value="<?= $user_password ?>" id="user_password_comfirm" name="user_password_comfirm" type="password" class="form-control" placeholder="Password" <?= $required ?>>
    </div>
  </div>
  <div class="col-md-12" style="display: <?= $display2 ?>" >
    <div class="form-group">
        <label>รายละเอียด</label>
        <input value="<?= $note1 ?>" name="$note1" type="text" class="form-control" placeholder="Name" >
    </div>
  </div>
  <div class="col-md-4" style="display: <?= $display2 ?>">
    <div class="form-group">
      <label>Status</label>
      <select name="is_active" class="form-control select2" style="width: 100%;" <?= $required2 ?>>
        <option value="Y" <?= ($is_active == 'Y' ? 'selected="selected"':'') ?> >ใช้งาน</option>
        <option value="N" <?= ($is_active == 'N' ? 'selected="selected"':'') ?> >ไม่ใช้งาน</option>
      </select>
    </div>
  </div>
  <div class="col-md-6" style="display:none">
    <div class="form-group">
      <label>Image</label>
      <input name="user_img" type="file" class="form-control" >
    </div>
  </div>
  <div class="col-md-2" style="display:none">
    <div class="form-group">
      <label>
        <img src="<?= $user_img ?>" onerror="this.onerror='';this.src='images/man.png'" style="height: 60px;">
      </label>
    </div>
  </div>
  <div class="col-md-12" style="display: <?= $display2 ?>" >
    <div class="form-group">
      <label>Select Role</label>
        <div class="box-body">
          <?=$option ?>
        </div>
    </div>
  </div>
</div>
<input type="hidden" name="user_id" value="<?= $user_id ?>">
<input type="hidden" name="type" value="<?= $type ?>">
