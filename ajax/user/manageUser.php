<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$user_id        = $_POST['user_id'];
$user_login     = isset($_POST['user_login']) ? $_POST['user_login'] : '';
$user_password  = isset($_POST['user_password']) ? md5($_POST['user_password']) : '';
$user_name      = isset($_POST['user_name']) ? $_POST['user_name'] : '';
$is_active      = isset($_POST['is_active']) ? $_POST['is_active'] : '';
$type           = isset($_POST['type']) ? $_POST['type'] : '';
$note1          = isset($_POST['note1']) ? $_POST['note1'] : '';
$note2          = isset($_POST['note2']) ? $_POST['note2'] : '';
$role_list      = "";
$user_id_update = $_SESSION['user_id'];

$user_img   = "";
$updateImg  = "";

$path = "image/";
$user_img = resizeImageToBase64($_FILES["user_img"],'128','128','100',$user_id_update,$path);
if(!empty($user_img)){
    $updateImg    =  "user_img = '$user_img',";
}


if(!empty($_POST['role_list'])){
  foreach( $_POST['role_list'] as $v ) {
      $role_list .= ",".$v;
  }
}

if(!empty($user_id)){
  $sql = "";
  if($type == 'edit'){
    $sql = "UPDATE t_user SET
            user_login      = '$user_login',
            user_name       = '$user_name',
            is_active       = '$is_active',
            ".$updateImg."
            role_list       = '$role_list',
            note1           = '$note1',
            note2           = '$note2',
            user_id_update  = '$user_id_update'
            WHERE user_id   = $user_id";
  }else if($type == 'resetPw'){
    $sql = "UPDATE t_user SET
            user_password   = '$user_password',
            user_id_update  = '$user_id_update'
            WHERE user_id   = $user_id";
  }

  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $status     = $json['status'];

  if($status == "success"){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else{
  if($_SESSION['TYPE_CONN'] == '1'){
    $sql = "INSERT INTO t_user
           (user_id,user_login,user_name,user_password,is_active,
             role_list,user_img,user_id_update,note1,note2)
           VALUES(T_USER_SEQ.NEXTVAL,'$user_login','$user_name','$user_password','$is_active',
             '$role_list','$user_img','$user_id_update','$note1','$note2')";
  }else{
    $sql = "INSERT INTO t_user
           (user_login,user_name,user_password,is_active,
             role_list,user_img,user_id_update,note1,note2)
           VALUES('$user_login','$user_name','$user_password','$is_active',
             '$role_list','$user_img','$user_id_update','$note1','$note2')";
  }


  $query      = DbQuery($sql,null);
  $json       = json_decode($query, true);
  $status     = $json['status'];

  if($status == "success"){
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'success','message' => 'Success')));
  }else{
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}
?>
