<?php
session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
.btnList{
  cursor: pointer;
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}

</style>
<?php
  $role = "Y";
  $user_name = $_SESSION['USER_NAME'];
  $pos = strrpos($_SESSION['ROLECODE'], "ADM");
  if ($pos === false) { // note: three equal signs
      $role = "N";
  }
  //echo $user_name;
  $dateStart  = $_POST['dateStart'];
  $dateEnd    = $_POST['dateEnd'];
  // echo $dateStart  = '2018/10/01';
  $dateS = $dateStart;
  $dateE = $dateEnd;
  $month = monthThaiFull(date("n", strtotime($dateStart)));
  $year  = date("Y", strtotime($dateStart))+543;

  $url = "ajax/report01/excelexport.php?dateStart=$dateStart"."&dateEnd=".$dateEnd;
?>
<div class="col-md-12">
  <div class="box boxBlack">
  <div class="box-header with-border">
    <h3 class="box-title"></h3>
    <div class="box-tools pull-right">
      <button onclick="postURL_blank('<?=$url?>')" class="btn btn-success" style="width:150px;">ออกรายงาน Excel</button>
    </div>
  </div>
  <div class="box-body">
    <div class="row">
      <div class="col-md-12">
        <p class="text-center" style="font-size:16px;"><b>รายงานการทดสอบสมรรถภาพทางกาย ตั้งแต่วันที่ <?=DateThai($dateStart) ?> ถึง <?= DateThai($dateEnd)?></b></p>
        <table class="table table-bordered table-striped">
          <thead>
        		<tr>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">ลำดับ</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">สถานที่</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">วัน/เดือน/ปี</th>
        			<th colspan="1" rowspan="2" style="vertical-align:middle">จำนวนรวม</th>
        			<th colspan="4" rowspan="1">ชาย (คน)</th>
        			<th colspan="4" rowspan="1">หญิง (คน)</th>
        			<th rowspan="2" style="vertical-align:middle">Excel</th>
        		</tr>
        		<tr>
        			<th>ต่ำกว่า 14 ปี</th>
        			<th>14-24 ปี</th>
        			<th>25-59 ปี</th>
        			<th>60 ปีขึ้นไป</th>
        			<th>ต่ำกว่า 14 ปี</th>
        			<th>14-24 ปี</th>
        			<th>25-59 ปี</th>
        			<th>60 ปีขึ้นไป</th>
        		</tr>
          </thead>
        <tbody class="text-center">
            <?php
              //$dateStart  = $_POST['dateStart'];
              //$dateStart    = date("Y-m", strtotime($dateStart));
              $end_date   = getQueryDate('end_date');
              $end_date2   = getQueryDateWere('end_date');

              $dateStart = queryDateOracle($dateStart);
              $dateEnd  = queryDateOracle($dateEnd);

              $con = "";
              if($role == "N"){
                $con = " and user_login = '$user_name'";
              }

              $sql = "SELECT $end_date,project_name,location,project_code
              FROM pfit_t_project where end_date between $dateStart and $dateEnd and status <> 'C' $con
              ORDER BY end_date ASC";

              //echo $sql;

              $query = DbQuery($sql,null);
              $row  = json_decode($query, true);
              $num  = $row['dataCount'];
              $data = $row['data'];

              $totalc = 0;

              $total_m_14 = 0;
              $total_m_25 = 0;
              $total_m_60 = 0;
              $total_m_61 = 0;

              $total_f_14 = 0;
              $total_f_25 = 0;
              $total_f_60 = 0;
              $total_f_61 = 0;


              if($num>0){
                foreach ($data as $key => $value) {
                  $project_name = @$value['project_name'];
                  $location     = @$value['location'];
                  $end_date     = convDatetoThaiMonth(@$value['end_date']);
                  $project_code = @$value['project_code'];
                  $end_date2    = @$value['end_date'];

                  $urlProject = "ajax/report01/excelexport.php?dateStart=$dateS&dateEnd=$dateE&project_code=$project_code";

                  $sqlc = "SELECT COUNT(person_number) as person_c FROM pfit_t_person WHERE project_code = '$project_code'";
                  $queryc = DbQuery($sqlc,null);
                  $rowc  = json_decode($queryc, true);

                  $person_c  = $rowc['data'][0]['person_c']==0?"0":$rowc['data'][0]['person_c'];


                  $sqlm = "SELECT person_gender,".getQueryDate('date_of_birth')." FROM pfit_t_person WHERE project_code = '$project_code'";
                  $querym = DbQuery($sqlm,null);
                  $rowm  = json_decode($querym, true);
                  $numm  = $rowm['dataCount'];

                  $person['M'][14] = 0;
                  $person['M'][25] = 0;
                  $person['M'][60] = 0;
                  $person['M'][61] = 0;

                  $person['F'][14] = 0;
                  $person['F'][25] = 0;
                  $person['F'][60] = 0;
                  $person['F'][61] = 0;

                  if($numm > 0){

                  foreach ($rowm['data'] as $keym => $valuem) {
                    $person_genderm = $valuem['person_gender'];
                    $sex = $person_genderm == 'M'?'M':'F';


                    // $age = yearBirth($valuem['date_of_birth']);
                    // echo $end_date2;
                    $age = yearBirth2($valuem['date_of_birth'],$end_date2);
                    if($age<14){
                      $person[$sex][14]++;
                    }elseif($age<=25){
                      $person[$sex][25]++;
                    }elseif($age<=59){
                      $person[$sex][60]++;
                    }else{
                      $person[$sex][61]++;
                    }

                  }

                  $totalc = $totalc+$person_c;

                  $total_m_14 += $person['M'][14];
                  $total_m_25 += $person['M'][25];
                  $total_m_60 += $person['M'][60];
                  $total_m_61 += $person['M'][61];

                  $total_f_14 += $person['F'][14];
                  $total_f_25 += $person['F'][25];
                  $total_f_60 += $person['F'][60];
                  $total_f_61 += $person['F'][61];

                }

            ?>
            <tr>
              <td><?=$key+1; ?></td>
              <td class="text-left"><?=$project_name; ?></td>
              <td align="right"><?=$end_date; ?></td>
              <td align="right"><?=$person_c; ?></td>
              <td align="right"><?=number_format($person['M'][14]); ?></td>
              <td align="right"><?=number_format($person['M'][25]); ?></td>
              <td align="right"><?=number_format($person['M'][60]); ?></td>
              <td align="right"><?=number_format($person['M'][61]); ?></td>
              <td align="right"><?=number_format($person['F'][14]); ?></td>
              <td align="right"><?=number_format($person['F'][25]); ?></td>
              <td align="right"><?=number_format($person['F'][60]); ?></td>
              <td align="right"><?=number_format($person['F'][61]); ?></td>
              <td><a onclick="postURL_blank('<?=$urlProject ?>')"><i class="fa fa-file-excel-o" aria-hidden="true"></i></a></td>
            </tr>
            <?php } ?>
            <tfoot>
              <tr>
          			<td colspan='3' align='right'><strong>ผลรวม</strong></td>
          			<td align="right"><strong><?=number_format($totalc); ?></strong></td>
          			<td align="right"><strong><?=number_format($total_m_14); ?></strong></td>
          			<td align="right"><strong><?=number_format($total_m_25); ?></strong></td>
          			<td align="right"><strong><?=number_format($total_m_60); ?></strong></td>
          			<td align="right"><strong><?=number_format($total_m_61); ?></strong></td>
                <td align="right"><strong><?=number_format($total_f_14); ?></strong></td>
          			<td align="right"><strong><?=number_format($total_f_25); ?></strong></td>
          			<td align="right"><strong><?=number_format($total_f_60); ?></strong></td>
          			<td align="right"><strong><?=number_format($total_f_61); ?></strong></td>
          			<td>&nbsp;</td>
          		</tr>
            </tfoot>
            <?php }else{ ?>
              <tr>
          			<td colspan="13">ไม่พบข้อมูล</td>
              </tr>
            <?php } ?>
        	</tbody>
        </table>

      </div>
    </div>
    </div>
  </div>
</div>
</div>
