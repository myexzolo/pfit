<?php

session_start();
include('../../conf/connect.php');
include('../../inc/utils.php');
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2011 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2011 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.6, 2011-02-27
 */

/** Error reporting */
error_reporting(E_ALL);

date_default_timezone_set('Europe/London');

/** PHPExcel */
require_once '../../Classes/PHPExcel.php';

$objPHPExcel = new PHPExcel();

$objPHPExcel->getProperties()->setCreator("pfit")
							 ->setLastModifiedBy("pfit")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

//$dateStart  	= $_POST['dateStart'];
$dateStart  = $_POST['dateStart'];
$dateEnd    = $_POST['dateEnd'];

$projectCode 	= isset($_POST['project_code'])?$_POST['project_code']:"";
// $dateStart  = '2018/10/01';
$month = monthThaiFull(date("n", strtotime($dateStart)));
$year  = date("Y", strtotime($dateStart))+543;


$r 	 = array("A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","AA","AB","AC","AD","AE","AF","AG");

// $sheet = $objPHPExcel->getActiveSheet();
//
// $sheet->getStyle('A1')->applyFromArray(array(
//     'borders' => array(
//         'left' => array(
//             'style' => PHPExcel_Style_Border::BORDER_THICK,
//             'color' => array(
//                 'rgb' => 'FF0000',
//             ),
//         ),
//     ),
// ));
$name = "รายงานการทดสอบสมรรถภาพทางกาย ตั้งแต่วันที่ ".DateThai($dateStart)." ถึง ".DateThai($dateEnd);

$objPHPExcel->getActiveSheet()->setTitle("$month $year");
$objPHPExcel->getActiveSheet()->mergeCells('A1:L1')->setCellValue('A1',$name)->getStyle('A1:L1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('A2:A3')->setCellValue('A2','ลำดับ')->getStyle('A2:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('B2:B3')->setCellValue('B2','สถานที่')->getStyle('B2:B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('C2:C3')->setCellValue('C2','วัน/เดือน/ปี')->getStyle('C2:C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('D2:D3')->setCellValue('D2','จำนวน')->getStyle('D2:D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('E2:H2')->setCellValue('E2','ชาย (คน)')->getStyle('E2:H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->mergeCells('I2:L2')->setCellValue('I2','หญิง (คน)')->getStyle('I2:L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()->setCellValue('E3', 'ต่ำกว่า 14 ปี');
$objPHPExcel->getActiveSheet()->setCellValue('F3', '14-24 ปี');
$objPHPExcel->getActiveSheet()->setCellValue('G3', '25-59 ปี');
$objPHPExcel->getActiveSheet()->setCellValue('H3', '60 ปีขึ้นไป');
$objPHPExcel->getActiveSheet()->setCellValue('I3', 'ต่ำกว่า 14 ปี');
$objPHPExcel->getActiveSheet()->setCellValue('J3', '14-24 ปี');
$objPHPExcel->getActiveSheet()->setCellValue('K3', '25-59 ปี');
$objPHPExcel->getActiveSheet()->setCellValue('L3', '60 ปีขึ้นไป');
$objPHPExcel->getActiveSheet()->getStyle('E3:L3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

//$dateEnd    = date("Y-m", strtotime($dateStart));
$con = "";
if($projectCode != ""){
	$con = " and project_code = '$projectCode' ";
}
//$sql = "SELECT * FROM pfit_t_project where end_date LIKE '$dateEnd%' $con ORDER BY end_date ASC";

$end_date    = getQueryDate('end_date');
$end_date2   = getQueryDateWere('end_date');

$dateStart = queryDateOracle($dateStart);
$dateEnd  = queryDateOracle($dateEnd);

$sql = "SELECT $end_date,project_name,location,project_code
FROM pfit_t_project where end_date between $dateStart and $dateEnd and status <> 'C' $con
ORDER BY end_date ASC";
//echo $sql;
$query = DbQuery($sql,null);
$row  = json_decode($query, true);
$num  = $row['dataCount'];
$data = $row['data'];


$projectCodeList = "";

$totalc = 0;

$total_m_14 = 0;
$total_m_25 = 0;
$total_m_60 = 0;
$total_m_61 = 0;

$total_f_14 = 0;
$total_f_25 = 0;
$total_f_60 = 0;
$total_f_61 = 0;

if($num>0){
  $i = 4;
  foreach ($data as $key => $value) {

    $project_name = @$value['project_name'];
    $location     = @$value['location'];
    $end_date     = convDatetoThaiMonth(@$value['end_date']);
    $project_code = @$value['project_code'];
    $end_date2 		= @$value['end_date'];

		if($projectCodeList == ""){
			$projectCodeList .= "'".$project_code."'";
		}else{
			$projectCodeList .= ",'".$project_code."'";
		}

    $sqlc = "SELECT COUNT(DISTINCT person_number) AS person_c
		FROM pfit_t_person WHERE project_code = '$project_code'";
    $queryc = DbQuery($sqlc,null);
    $rowc  = json_decode($queryc, true);

    $person_c  = $rowc['data'][0]['person_c']==0?"0":$rowc['data'][0]['person_c'];

    //$person = array();
    //$sqlm = "SELECT *  FROM pfit_t_person WHERE project_code = '$project_code'";
		$sqlm = "SELECT person_gender,".getQueryDate('date_of_birth')." FROM pfit_t_person WHERE project_code = '$project_code'";
    $querym = DbQuery($sqlm,null);
    $rowm  = json_decode($querym, true);
    $numm  = $rowm['dataCount'];

		$person['M'][14] = 0;
		$person['M'][25] = 0;
		$person['M'][60] = 0;
		$person['M'][61] = 0;

		$person['F'][14] = 0;
		$person['F'][25] = 0;
		$person['F'][60] = 0;
		$person['F'][61] = 0;
    if($numm > 0){

    foreach ($rowm['data'] as $keym => $valuem) {
      $person_genderm = $valuem['person_gender'];
      $sex = $person_genderm == 'M'?'M':'F';

      $age = yearBirth2($valuem['date_of_birth'],$end_date2);
      if($age<14){
        $person[$sex][14]++;
      }elseif($age<=25){
        $person[$sex][25]++;
      }elseif($age<=59){
        $person[$sex][60]++;
      }else{
        $person[$sex][61]++;
      }
    }

    $totalc = $totalc+$person_c;

    $total_m_14 += $person['M'][14];
    $total_m_25 += $person['M'][25];
    $total_m_60 += $person['M'][60];
    $total_m_61 += $person['M'][61];

    $total_f_14 += $person['F'][14];
    $total_f_25 += $person['F'][25];
    $total_f_60 += $person['F'][60];
    $total_f_61 += $person['F'][61];

    }

    $objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $key+1);
    $objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $project_name);
    $objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $end_date);
    $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $person_c);
    $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, @$person['M'][14]);
    $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, @$person['M'][25]);
    $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, @$person['M'][60]);
    $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, @$person['M'][61]);
    $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, @$person['F'][14]);
    $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, @$person['F'][25]);
    $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, @$person['F'][60]);
    $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, @$person['F'][61]);

    $i++;
  }
  $objPHPExcel->getActiveSheet()->mergeCells('A'.$i.':C'.$i)->setCellValue('A'.$i,'รวม')->getStyle('A'.$i.':C'.$i)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
  $objPHPExcel->getActiveSheet()->setCellValue('D'.$i, @$totalc);
  $objPHPExcel->getActiveSheet()->setCellValue('E'.$i, @$total_m_14);
  $objPHPExcel->getActiveSheet()->setCellValue('F'.$i, @$total_m_25);
  $objPHPExcel->getActiveSheet()->setCellValue('G'.$i, @$total_m_60);
  $objPHPExcel->getActiveSheet()->setCellValue('H'.$i, @$total_m_61);
  $objPHPExcel->getActiveSheet()->setCellValue('I'.$i, @$total_f_14);
  $objPHPExcel->getActiveSheet()->setCellValue('J'.$i, @$total_f_25);
  $objPHPExcel->getActiveSheet()->setCellValue('K'.$i, @$total_f_60);
  $objPHPExcel->getActiveSheet()->setCellValue('L'.$i, @$total_f_61);

	foreach(range('A','U') as $columnID) {
	    $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
	        ->setAutoSize(true);
	}
}else{


}

$con = "";
if($projectCode != ""){
	$con = " and p.project_code = '$projectCode' ";
}
$end_date   = getQueryDateWere('p.end_date');

$sql = "SELECT DISTINCT(pt.test_code) as test_code,ts.test_name,ts.test_opjective,ts.category_criteria_code
				FROM pfit_t_project p , pfit_t_project_test pt, pfit_t_test ts WHERE p.project_code = pt.project_code $con
				AND p.status <> 'C' AND ts.test_code = pt.test_code AND p.end_date between $dateStart and $dateEnd ORDER BY pt.test_code ASC";
//echo $sql;
$queryT = DbQuery($sql,null);
$rowT  = json_decode($queryT, true);
$numT  = $rowT['dataCount'];
$dataT = $rowT['data'];
$tab = 0;

for($x=0;$x <$numT;$x++){
	$tab++;
	$test_code 							= $dataT[$x]['test_code'];
	$test_name 							= $dataT[$x]['test_name'];
	$test_opjective 				= $dataT[$x]['test_opjective'];
	$category_criteria_code = $dataT[$x]['category_criteria_code'];
	$test_opjective = getTestOpjective($test_opjective);
	$test_name = str_replace("/"," ",$test_name);
  $test_name = str_replace('\\'," ",$test_name);

	$objPHPExcel->createSheet()->setTitle($test_code);
	$objPHPExcel->setActiveSheetIndex($tab);
	$objPHPExcel->getActiveSheet($tab)->mergeCells('A2:A3')->setCellValue('A2','สมรรถภาพทางกาย')->getStyle('A2:A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet($tab)->mergeCells('B2:B3')->setCellValue('B2','รายการ')->getStyle('B2:B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet($tab)->mergeCells('C2:C3')->setCellValue('C2','อายุ (ปี)')->getStyle('C2:C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);


	$sql = "SELECT * FROM pfit_t_cat_criteria_detail WHERE category_criteria_code = '$category_criteria_code' ORDER BY category_criteria_detail_code";
	$queryDet 	= DbQuery($sql,null);
	$rowDet  		= json_decode($queryDet, true);
	$numDet  		= $rowDet['dataCount'];
	$dataDet 		= $rowDet['data'];

	$rs      = 3;
	$re      = (2 + $numDet);
	$start   = $r[$rs].'2';
	$end     = $r[$re].'2';
	$cellMerge = $start.":".$end;
	$objPHPExcel->getActiveSheet($tab)->mergeCells($cellMerge)->setCellValue($start,'ชาย (คน)')->getStyle($cellMerge)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	for($j=0;$j <$numDet;$j++){
		$detail_name 	= $dataDet[$j]['category_criteria_detail_name'];
		$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

		$col = $r[($rs+$j)];
		$resDetail['M'][$detail_code] =  array();
		$resDetail['M'][$detail_code]['COL'] = $col;
		$resDetail['M'][$detail_code][6] = 0;
		$resDetail['M'][$detail_code][60] = 0;
		$objPHPExcel->getActiveSheet()->setCellValue($col.'3', $detail_name)->getStyle($col.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
	$rs      = $re + 1;
	$re      = ($re + $numDet);
	$start   = $r[$rs].'2';
	$end     = $r[$re].'2';
	$cellMerge = $start.":".$end;

	$objPHPExcel->getActiveSheet($tab)->mergeCells($cellMerge)->setCellValue($start,'หญิง (คน)')->getStyle($cellMerge)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	for($j=0;$j <$numDet;$j++){
		$detail_name 	= $dataDet[$j]['category_criteria_detail_name'];
		$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

		$col = $r[($rs+$j)];
		$resDetail['F'][$detail_code] = array();
		$resDetail['F'][$detail_code]['COL'] = $col;
		$resDetail['F'][$detail_code][6] = 0;
		$resDetail['F'][$detail_code][60] = 0;
		//$resDetail['F'][$detail_code]['R'.$j] = $col;
		$objPHPExcel->getActiveSheet()->setCellValue($col.'3', $detail_name)->getStyle($col.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}

	$endTotal = $re;
	$rs      = $re + 1;
	$re      = ($re + $numDet);
	$start   = $r[$rs].'2';
	$end     = $r[$re].'2';
	$cellMerge = $start.":".$end;

	$objPHPExcel->getActiveSheet($tab)->mergeCells($cellMerge)->setCellValue($start,'รวม (คน)')->getStyle($cellMerge)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	for($j=0;$j <$numDet;$j++){
		$detail_name 	= $dataDet[$j]['category_criteria_detail_name'];
		$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

		$col = $r[($rs+$j)];
		$resDetail['T'][$detail_code] = array();
		$resDetail['T'][$detail_code]['COL'] = $col;
		$resDetail['T'][$detail_code]['TOTAL'] = 0;
		$resDetail['T'][$detail_code][6] = 0;
		$resDetail['T'][$detail_code][60] = 0;
		$objPHPExcel->getActiveSheet()->setCellValue($col.'3', $detail_name)->getStyle($col.'3')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	}
	$objPHPExcel->getActiveSheet($tab)->mergeCells('A1:'.$r[$re].'1')->setCellValue('A1',"รายงานสรุปผลทั้งหมดของรายการ  $test_name ประจำเดือน $month $year")->getStyle('A1:'.$r[$rs].'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	$rowAge = 4;
	for($a= 7;$a < 60; $a++){
		$rowAge++;
		for($j=0;$j <$numDet;$j++){
			$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];
			//echo $detail_code."<br>";
			$resDetail['M'][$detail_code][$a] = 0;
			$resDetail['F'][$detail_code][$a] = 0;
			$resDetail['T'][$detail_code][$a] = 0;
		}


		$objPHPExcel->getActiveSheet($tab)->setCellValue('C'.$rowAge, $a);
	}


  // //print_r ($resDetail);
	//
	$con = "";
	$con2 = "";
	$date_create   = getQueryDateWere('r.date_create');
	if($projectCode != ""){
		$con = " and r.project_code = '$projectCode' ";
	}

	if ($projectCodeList != ""){
		$con2 = " r.project_code in ($projectCodeList) ";
	}else{
		$con2  = $date_create." LIKE '".$dateEnd."%' ";
	}
	//$date_create   = getQueryDateWere('r.date_create');

	$sql = "SELECT t.* FROM pfit_t_result r , pfit_t_test_criteria t
					WHERE $con2 and r.test_code='$test_code' and r.test_criteria_code = t.test_criteria_code $con";
	$queryRes 	= DbQuery($sql,null);
	$rowRes  		= json_decode($queryRes, true);
	$numRes  		= $rowRes['dataCount'];
	$dataRes 		= $rowRes['data'];
	//echo $sql."<br>";
	for($e=0;$e < $numRes; $e++){
		$age 					= $dataRes[$e]['age'];
		$gender 			= $dataRes[$e]['gender'];
		$detailCode 	= $dataRes[$e]['category_criteria_detail_code'];

		//echo $age.",";
		if($age<7){
					//$resDetail[$gender][$detailCode][6]++;
					if(isset($resDetail[$gender][$detailCode][6])){
						$resDetail[$gender][$detailCode][6]++;
						$resDetail['T'][$detailCode][6]++;
					}else{
						$resDetail[$gender][$detailCode][6] = 1;
						$resDetail['T'][$detailCode][6] = 1;
					}
		}else if($age > 60){
					//$resDetail[$gender][$detailCode][61]++;
					if(isset($resDetail[$gender][$detailCode][60])){
						$resDetail[$gender][$detailCode][60]++;
						$resDetail['T'][$detailCode][60]++;
					}else{
						$resDetail[$gender][$detailCode][60] = 1;
						$resDetail['T'][$detailCode][60] = 1;
					}
		}else{
				if(isset($resDetail[$gender][$detailCode][$age])){
					$resDetail[$gender][$detailCode][$age]++;
					$resDetail['T'][$detailCode][$age]++;
					//echo $detailCode.",".$gender.",".$age.",".$test_code."<br>";
				}else{
					$resDetail[$gender][$detailCode][$age] = 1;
					$resDetail['T'][$detailCode][$age] = 1;
				}
		}
		//$resDetail['T'][$detailCode]['TOTAL']++;
		if(isset($resDetail['T'][$detailCode]['TOTAL'])){
			$resDetail['T'][$detailCode]['TOTAL']++;
		}else{
			$resDetail['T'][$detailCode]['TOTAL'] = 1;
		}
	}


	$rowAge = 3;
	$total = 0;
	for($a= 6;$a <= 60; $a++){
		$rowAge++;
		for($j=0;$j <$numDet;$j++){
			$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];

			$resM = $resDetail['M'][$detail_code][$a];
			$resF = $resDetail['F'][$detail_code][$a];
			$resT = $resDetail['T'][$detail_code][$a];

			// echo "resDetail['M'][$detail_code][$a] :".$resDetail['M'][$detail_code][$a];
			// echo "<br>";
			// echo "resDetail['F'][$detail_code][$a] :".$resDetail['F'][$detail_code][$a];
			// echo "<br>";

			$colM = $resDetail['M'][$detail_code]['COL'];
			$colF = $resDetail['F'][$detail_code]['COL'];
			$colT = $resDetail['T'][$detail_code]['COL'];

			$objPHPExcel->getActiveSheet($tab)->setCellValue($colM.$rowAge, $resM);
			$objPHPExcel->getActiveSheet($tab)->setCellValue($colF.$rowAge, $resF);
			$objPHPExcel->getActiveSheet($tab)->setCellValue($colT.$rowAge, $resT);
		}
	}


	$objPHPExcel->getActiveSheet($tab)->mergeCells('A4:A58')->setCellValue('A4', $test_opjective);
	$objPHPExcel->getActiveSheet($tab)->mergeCells('B4:B58')->setCellValue('B4', $test_name);
  $objPHPExcel->getActiveSheet($tab)->getStyle('A4:B4')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
	$objPHPExcel->getActiveSheet($tab)->setCellValue('C4', 'น้อยกว่า 7');
	$objPHPExcel->getActiveSheet($tab)->setCellValue('C58', '60 ขึ้นไป');
	$objPHPExcel->getActiveSheet($tab)->mergeCells('A59:'.$r[$endTotal].'59')->setCellValue('A59', 'รวม')->getStyle('A59')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

	for($j=0;$j <$numDet;$j++){
		$detail_code 	= $dataDet[$j]['category_criteria_detail_code'];
		//echo $detail_code."<br>";
		$total  = $resDetail['T'][$detail_code]['TOTAL'];
		$colT 	= $resDetail['T'][$detail_code]['COL'];

		$objPHPExcel->getActiveSheet($tab)->setCellValue($colT.'59', $total);
	}

	foreach(range('A','U') as $columnID) {
	    $objPHPExcel->getActiveSheet($tab)->getColumnDimension($columnID)
	        ->setAutoSize(true);
	}
}

$objPHPExcel->setActiveSheetIndex(0);

header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');


?>
