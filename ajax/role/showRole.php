<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<table class="table table-bordered table-striped" id="tableDisplay">
  <thead>
    <tr class="text-center">
      <td>Num</td>
      <td>Code</td>
      <td>Name</td>
      <td>รายละเอียด</td>
      <td>สถานะ</td>
      <td>Edit/Del</td>
    </tr>
  </thead>
  <tbody>
<?php
  $sql = "SELECT * FROM t_role ORDER BY role_id DESC";

  $query = DbQuery($sql,null);
  $row = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];


  for ($i=0; $i < $num ; $i++) {
?>
    <tr class="text-center">
      <td><?= $i+1 ?></td>
      <td><?= $data[$i]['role_code']; ?></td>
      <td><?= $data[$i]['role_name']; ?></td>
      <td><?= $data[$i]['role_desc']; ?></td>
      <td><?= ($data[$i]['is_active'] == 'Y' ? 'ใช้งาน' : 'ไม่ใช้งาน') ; ?></td>
      <td>
        <button type="button" class="btn btn-warning btn-sm" onclick="showForm(<?= $data[$i]['role_id']; ?>)">Edit</button>
        <button type="button" class="btn btn-danger btn-sm" onclick="removeRole(<?= $data[$i]['role_id']; ?>)">Del</button>
      </td>
    </tr>
<?php } ?>
</tbody>
</table>
<script>
  $(function () {
    $('#tableDisplay').DataTable();
  })
</script>
