<?php
session_start();
include('../../conf/connect.php');
include('../../conf/utils.php');

$code = $_POST['code'];
$ParamData = array($code);

$sql  = "UPDATE PFIT_T_PROJECT SET STATUS = 'C' WHERE project_code = ?";

//$sql  = "DELETE FROM PFIT_T_PROJECT WHERE PROJECT_ID = ?";

$query      = DbQuery($sql,$ParamData);
$row        = json_decode($query, true);
$status     = $row['status'];

if($status == "success"){
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}
?>
